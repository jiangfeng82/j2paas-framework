/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.impl;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.vos.AbstractPageVo;
import cn.easyplatform.messages.vos.VisibleVo;
import cn.easyplatform.spi.service.TaskService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.web.dialog.MessageBox;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.utils.PageUtils;
import cn.easyplatform.web.utils.WebUtils;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Idspace;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Panelchildren;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PanelMainTaskBuilder extends AbstractMainTaskBuilder<Idspace> {

    private Panel panel;

    /**
     * @param container
     * @param taskId
     * @param pv
     */
    public PanelMainTaskBuilder(Component container, String taskId, AbstractPageVo pv) {
        super(container, taskId, pv);
    }

    @Override
    public void build() {
        panel = new Panel();
        panel.setBorder("normal");
        panel.setTitle(apv.getTitile());
        panel.setClosable(true);
        panel.setCollapsible(true);
        panel.setMaximizable(true);
        panel.setFloatable(true);
        panel.setMovable(true);
        panel.setSizable(true);
        super.build();
        Panelchildren pc = new Panelchildren();
        pc.appendChild(idSpace);
        panel.appendChild(pc);
        container.appendChild(panel);
        panel.setEvent("close()");
        PageUtils.addEventListener(this, Events.ON_CLOSE, panel);
        setSize();
    }

    @Override
    protected void doPage() {
        super.doPage();
        setSize();
    }

    private void setSize() {
        if (apv instanceof VisibleVo) {
            VisibleVo vv = (VisibleVo) apv;
            if (vv.getHeight() > 0)
                panel.setHeight(vv.getHeight() + "px");
            if (vv.getWidth() > 0)
                panel.setWidth(vv.getWidth() + "px");
            if (vv.getHeight() <= 0 || vv.getWidth() <= 0) {
                panel.setMaximized(true);
            }
        } else {
            if (Strings.isBlank(idSpace.getHeight()))
                panel.setVflex("1");
            if (Strings.isBlank(idSpace.getWidth()))
                panel.setHflex("1");
        }
    }

    @Override
    public void close(boolean normal) {
        if (!normal) {
            IResponseMessage<?> resp = ServiceLocator.lookup(
                    TaskService.class).close(
                    new SimpleRequestMessage(getId(), null));
            if (!resp.isSuccess()) {
                MessageBox.showMessage(resp);
                return;
            }
        }
        WebUtils.removeTask(getId());
        clear();
        idSpace.detach();
        idSpace = null;
        panel.detach();
        panel = null;
    }
}
