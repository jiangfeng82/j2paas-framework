/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.simple;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.GetListRequestMessage;
import cn.easyplatform.messages.response.GetListResponseMessage;
import cn.easyplatform.messages.vos.GetListVo;
import cn.easyplatform.messages.vos.datalist.ListGetListVo;
import cn.easyplatform.spi.service.ComponentService;
import cn.easyplatform.type.FieldVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.type.ListRowVo;
import cn.easyplatform.web.ext.ComponentBuilder;
import cn.easyplatform.web.ext.Widget;
import cn.easyplatform.web.ext.zul.ComboboxExt;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.task.zkex.ListSupport;
import cn.easyplatform.web.task.OperableHandler;
import cn.easyplatform.web.task.zkex.list.PanelSupport;
import cn.easyplatform.web.utils.PageUtils;
import cn.easyplatform.web.utils.WebUtils;
import org.zkoss.image.AImage;
import org.zkoss.image.Image;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Comboitem;

import java.io.IOException;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ComboboxBuilder extends AbstractQueryBuilder<ComboboxExt> implements Widget {

    public ComboboxBuilder(OperableHandler mainTaskHandler, ComboboxExt comp) {
        super(mainTaskHandler, comp);
    }

    public ComboboxBuilder(ListSupport support, ComboboxExt comp, Component anchor) {
        super(support, comp, anchor);
    }

    @Override
    public Component build() {
        me.setAttribute("$proxy", this);
        PageUtils.checkAccess(main.getAccess(), me);
        if (me.isImmediate())
            load();
        PageUtils.processEventHandler(main, me, anchor);
        return me;
    }

    protected void createModel(List<?> data) {
        if (me.getEmptyLabel() != null
                && !me.getEmptyLabel().equals("")) {
            Comboitem cbi = new Comboitem();
            cbi.setValue(me.getEmptyValue());
            cbi.setLabel(me.getEmptyLabel());
            me.appendChild(cbi);
        }
        for (Object fv : data) {
            Object[] fvs = (Object[]) fv;
            boolean isRaw = !(fvs[0] instanceof FieldVo);
            Comboitem cbi = new Comboitem();
            cbi.setValue(isRaw ? fvs[0] : ((FieldVo) fvs[0]).getValue());
            if (fvs.length == 1) {
                Object val = isRaw ? fvs[0] : ((FieldVo) fvs[0]).getValue();
                cbi.setLabel(val == null ? "" : val.toString());
                if (me.getMaxlength() == 0 && !isRaw)
                    me.setMaxlength(((FieldVo) fvs[0]).getLength());
            } else if (fvs.length == 2) {
                if (me.isShowValue()) {
                    Object val = isRaw ? fvs[0] : ((FieldVo) fvs[0]).getValue();
                    cbi.setLabel(val == null ? "" : val.toString());
                    val = isRaw ? fvs[1] : ((FieldVo) fvs[1]).getValue();
                    cbi.setDescription(val == null ? ""
                            : val.toString());
                } else {
                    Object val = isRaw ? fvs[1] : ((FieldVo) fvs[1]).getValue();
                    cbi.setLabel(val == null ? "" : val.toString());
                }
                if (me.getMaxlength() == 0 && !isRaw)
                    me.setMaxlength(((FieldVo) fvs[1]).getLength());
            } else if (fvs.length == 3) {
                Object val = isRaw ? fvs[1] : ((FieldVo) fvs[1]).getValue();
                cbi.setLabel(val == null ? "" : val.toString());
                val = isRaw ? fvs[2] : ((FieldVo) fvs[2]).getValue();
                if (val != null) {
                    if (val instanceof byte[]) {
                        try {
                            Image image = new AImage("",
                                    (byte[]) val);
                            cbi.setImageContent(image);
                        } catch (IOException e) {
                        }
                    } else if (val instanceof String) {
                        String str = (String) val;
                        if (str.startsWith("z-icon")
                                || str.startsWith("fa fa-"))
                            cbi.setIconSclass(str);
                        else
                            cbi.setDescription(str);
                    }
                }
            } else if (fvs.length == 4) {
                Object val = isRaw ? fvs[1] : ((FieldVo) fvs[1]).getValue();
                cbi.setLabel(val == null ? "" : val.toString());
                val = isRaw ? fvs[2] : ((FieldVo) fvs[2]).getValue();
                cbi.setDescription(val == null ? ""
                        : val.toString());
                val = isRaw ? fvs[3] : ((FieldVo) fvs[3]).getValue();
                if (val != null) {
                    if (val instanceof String) {
                        String str = (String) val;
                        if (str.startsWith("z-icon")
                                || str.startsWith("fa fa-"))
                            cbi.setIconSclass(str);
                        else
                            cbi.setImage(str);
                    } else if (val instanceof byte[]) {
                        try {
                            Image image = new AImage("",
                                    (byte[]) val);
                            cbi.setImageContent(image);
                        } catch (IOException e) {
                        }
                    }
                }
            }// if
            me.appendChild(cbi);
        }// for
    }

    @Override
    public void reload(Component widget) {
        widget.getChildren().clear();
        load();
    }

}
