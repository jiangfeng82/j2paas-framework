/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.support.scripting;

import cn.easyplatform.type.FieldVo;
import cn.easyplatform.web.task.EventSupport;
import cn.easyplatform.web.utils.WebUtils;

import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SimpleCmd extends UtilsCmd {

    private FieldVo[] source;

    private FieldVo[] target;

    public SimpleCmd(EventSupport handler, Map<String, Object> scope) {
        super(handler, scope);
    }

    void setSource(FieldVo[] source) {
        this.source = source;
    }

    void setTarget(FieldVo[] target) {
        this.target = target;
    }

    @Override
    public void setValue(Object name, Object value, String... type) {
        if (type.length == 0) {
            setVal((String) name, value);
        } else {
            if ("$".equals(type[0])) {
                for (FieldVo field : target) {
                    if (field.getName().equals(name)) {
                        field.setValue(value);
                        return;
                    }
                }
            } else {
                if (source == null)
                    setVal((String) name, value);
                else {
                    for (FieldVo field : source) {
                        if (field.getName().equals(name)) {
                            field.setValue(value);
                            return;
                        }
                    }
                }
            }
            setVal((String) name, value);
        }
    }

    @Override
    public Object getValue(Object name, String... type) {
        if (type.length == 0) {
            return getVal((String) name);
        } else {
            if ("$".equals(type[0])) {
                for (FieldVo field : target) {
                    if (field.getName().equalsIgnoreCase((String) name))
                        return field.getValue();
                }
            } else {
                if (source == null)
                    return getVal((String) name);
                for (FieldVo field : source) {
                    if (field.getName().equalsIgnoreCase((String) name))
                        return field.getValue();
                }
            }
            return getVal((String) name);
        }
    }

    private void setVal(String name, Object value) {
        WebUtils.setFieldValue(handler, name, value);
    }

    private Object getVal(String name) {
        return WebUtils.getFieldValue(handler, name);
    }
}
