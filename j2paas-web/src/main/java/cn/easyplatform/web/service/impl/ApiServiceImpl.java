/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.service.impl;

import cn.easyplatform.messages.request.ApiInitRequestMessage;
import cn.easyplatform.messages.request.ApiRequestMessage;
import cn.easyplatform.messages.request.BeginRequestMessage;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.request.im.PagingRequestMessage;
import cn.easyplatform.spi.engine.EngineFactory;
import cn.easyplatform.spi.service.ApiService;
import cn.easyplatform.type.IResponseMessage;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/10/29 10:56
 * @Modified By:
 */
public class ApiServiceImpl implements ApiService {

    private ApiService broker = EngineFactory.me().getEngineService(
            ApiService.class);

    @Override
    public IResponseMessage<?> auth(ApiInitRequestMessage req) {
        return broker.auth(req);
    }

    @Override
    public IResponseMessage<?> call(SimpleRequestMessage req) {
        return broker.call(req);
    }

    @Override
    public IResponseMessage<?> api(ApiRequestMessage req) {
        return broker.api(req);
    }

    @Override
    public IResponseMessage<?> task(BeginRequestMessage req) {
        return broker.task(req);
    }

    @Override
    public IResponseMessage<?> anonymous(SimpleRequestMessage req) {
        return broker.anonymous(req);
    }

    @Override
    public IResponseMessage<?> quick(SimpleRequestMessage req) {
        return broker.quick(req);
    }

    @Override
    public IResponseMessage<?> token(SimpleRequestMessage req) {
        return broker.token(req);
    }

    @Override
    public IResponseMessage<?> exit(SimpleRequestMessage req) {
        return broker.exit(req);
    }

    @Override
    public IResponseMessage<?> poll(SimpleRequestMessage req) {
        return broker.poll(req);
    }

    @Override
    public IResponseMessage<?> paging(PagingRequestMessage req) {
        return broker.paging(req);
    }
}
