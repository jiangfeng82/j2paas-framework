/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.dialog;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.type.IResponseMessage;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class MessageBox {

    /**
     * @param resp
     * @param listener
     * @return
     */
    public final static MessageDlg createDialog(IResponseMessage<?> resp,
                                                EventListener<Event> listener) {
        char type = resp.getCode().toLowerCase().charAt(0);
        if (type == 'c')
            return new ConfirmMessageDlg(resp, listener);
        else if (type == 'w')
            return new WarningMessageDlg(resp, listener);
        else if (type == 'i')
            return new InfoMessageDlg(resp, listener);
        else
            return new ErrorMessageDlg(resp);

    }

    /**
     * @param resp
     */
    public final static void showMessage(IResponseMessage<?> resp) {
        if (resp.getCode().equals("E000")) {
            new ErrorMessageDlg(resp).show();
        } else {
            String icon = Messagebox.ERROR;
            char type = resp.getCode().toLowerCase().charAt(0);
            if (type == 'c')
                icon = Messagebox.QUESTION;
            else if (type == 'w')
                icon = Messagebox.EXCLAMATION;
            else if (type == 'i')
                icon = Messagebox.INFORMATION;
            Messagebox.show((String) resp.getBody(), resp.getCode(),
                    Messagebox.OK, icon);
        }
    }

    /**
     * @param title
     * @param message
     */
    public final static void showMessage(String title, String message) {
        Messagebox.show(message, title, Messagebox.OK, Messagebox.ERROR);
    }

    /**
     * @param title
     * @param message
     */
    public final static void showInfo(String title, String message) {
        Messagebox.show(message, title, Messagebox.OK, Messagebox.INFORMATION);
    }

    /**
     * @param ex
     */
    public final static void showMessage(EasyPlatformWithLabelKeyException ex) {
        MessageBox.showMessage(Labels.getLabel("message.dialog.title.error"),
                Labels.getLabel(ex.getMessage(), ex.getArgs()));
    }

    public final static void showNotification(String message, String type,
                                              Component ref, boolean closable) {
        Clients.showNotification(message, type, ref, (String) null, -1,
                closable);
    }

}
