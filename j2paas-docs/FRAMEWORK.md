# j2Paas 技术框架

### 一、介绍
J2PaaS采用前后端分离架构，提供服务治理、服务网关、智能路由、负载均衡、断路器、监控跟踪、分布式消息队列、配置管理等领域的功能。

### 二、技术栈

#### 后端：

| 名称 | 资源 |
|--- | --------|
|rpc框架 | ignite、grpc、dubbo、motan|
|报表框架 | jxls、jasperreport、poi |
| 持久框架 | jdbc |
| 程序构建 | maven |
| 数据库 |  达梦、mysql、oracle、db2、sqlserver、sybase |
| 消息中间件 | ignite、redis、rabbitMQ、activeMQ、kafka、rocketMQ |
| 缓存 | ehcache、ignite、redis |
| 工作流 | snaker |
| 安全框架 | apache shiro |
| 数据库连接池 | 阿里druid |
| 定时任务 | quartz |
| 分布式事务 | jotm、ignite、seata |
| 文件系统 | local、fastdfs、阿里oss、华为obs、腾讯cos以及七牛oss|
| 日志处理 | log4j2、ELK|
| 会话存储 | ignite、redis |
| 服务治理与发现 | zookeeper、consul |
| 分库分表 | Apache shardingSphere-jdbc|
| 容器 | docker/k8s |
| 逻辑引擎 | rhino、bsh |
| 分布式存储 | ignite |
| 分布式计算 | ignite |

#### 前端：

| 名称 | 资源 |
|--- | --------|
|MVC框架	|ZK MVC|
|web代理|	Nginx|
|构建工具	|Maven|
|JS版本	|ES6|
|基础JS框架|	JQuery.js|
|辅助库	|Boostrap|
|基础UI库	|ZK|
|CSS预处理|	Scss|
|图表系统|	ECharts/ZKCharts|
|富文本编辑器| Ckeditor/KindEditor/CodeMirror |
|API安全框架 | JWT |
|消息处理 | disruptor |

### 三、软件架构

#### 平台结构图：

![meta.png](img/framework/meta.png)

#### 软件架构图

![framework.png](img/framework/framework.png)

#### 部署架构图：

![deploy.png](img/framework/deploy.png)

### 四、模块说明
| 名称 | 描述 |
| --- | ----------- |
| easyplatform-bpm | 工作流模块，使用第3方开源snaker项目作为默认工作流引擎，如果没有特殊业务需求大部分场景都可以满足，当然，您也可以使用自定义实现类，引入标准bpmn2实现例如flowable、activiti、camuda等来实现复杂的工作流|
| easyplatform-core | 核心模块，涵盖项目的启动、管理、逻辑的解析和处理、固定任务和动态任务、缓存、资源调配等等 |
| easyplatform-dao | 数据库dao接口模块，纯jdbc驱动，没有使用第3方ORM框架，一切都是为了效率，再效率。。。|
| easyplatform-engine | 服务入口模块，包括权限管理、api管理、功能执行管理等等 |
| easyplatform-entities | 系统参数实体类模块，以xml格式保存在数据库，不同的业务功能对应不同的实现类，以便动态解析及执行 |
| easyplatform-jxls | excel报表默认的参考实现模块，支持excel常用的内置的统计函数 |
| easyplatform-messages | 前后端分离所需的消息数据模块 |
| easyplatform-report | jasper report报表模块，目前国内市场应该已经很少使用了，大部分都是用excel报表来代替 |
| easyplatform-script | 逻辑语言模块，前后端逻辑和事件处理的脚本引擎，开源版仅支持javascript |
| easyplatform-spi | 服务接口、插件和第3方开发api模块，扩展对应的业务接口，可以实现在应用的生命周期中执行业务逻辑，例如引入IoT、MQ以及编写RPC调用等等|
| easyplatform-support | 系统支持模块，帮助类、类型定义、aop等相关工具|
| easyplatform-web | 前端web模块|
| easyplatform-zkex | 前端web组件扩展模块，定义一些扩展组件需要的接口以及增强zk组件|
| easyplatform-bmap | 前端百度地图组件|
| easyplatform-bootstrap | 前端bootstrap扩展|
| easyplatform-bpmdesigner | 前端bpm设计组件，基于snaker工作流|
| easyplatform-bpmn | 前端bpmn组件|
| easyplatform-calendar | 前端日历组件|
| easyplatform-cardview | 前端卡片组件|
| easyplatform-carousel | 前端轮播图扩展|
| easyplatform-ckez | 前端ckeditor 5组件|
| easyplatform-cmez | 前端codemirror组件|
| easyplatform-datetimepicker | 前端日期组件|
| easyplatform-echarts | 前端echarts组件|
| easyplatform-filemanager | 前端文件管理组件|
| easyplatform-introcoach | 前端向导组件|
| easyplatform-kalendar | 前端日历增强组件|
| easyplatform-kindeditor | 前端kindeditor组件|
| easyplatform-mind | 前端脑图组件|
| easyplatform-orgchart | 前端组织结构图组件|
| easyplatform-tmap | 前端腾讯地图组件|
| easyplatform-upcamera | 前端相机组件|

