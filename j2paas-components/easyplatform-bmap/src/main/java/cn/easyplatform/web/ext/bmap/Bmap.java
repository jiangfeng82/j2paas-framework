/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.bmap;

import cn.easyplatform.lang.Strings;
import cn.easyplatform.web.ext.Widget;

import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.impl.XulElement;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;

import java.io.IOException;

/**
 * @author <a href="mailto:wigginzhang@epclouds.com">张文俊</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Bmap extends XulElement {

    private static final long serialVersionUID = 20090331171730L;
    public final static String ON_SAVE = "onSave";
    public final static String ON_LOCATION = "onLocation";
    public final static String ON_SEARCH = "onSearch";

    static {
        addClientEvent(Bmap.class, ON_SAVE, CE_REPEAT_IGNORE);
        addClientEvent(Bmap.class, ON_LOCATION, CE_REPEAT_IGNORE);
        addClientEvent(Bmap.class, ON_SEARCH, CE_REPEAT_IGNORE);
    }

    private String _mapStyle;
    private String[] _areaStyle;
    private boolean _enableMapClick = true;
    private boolean _infobox = true;
    private int _zoom = 15;
    private boolean _dragging;
    private boolean _scrollWheelZoom;
    private boolean _doubleClickZoom;
    private boolean _pinchToZoom;
    private boolean _choosePoint;
    private boolean _scaleControl;
    private String _scaleAnchor;
    private String _scaleOffset;
    private boolean _copyrightControl;
    private String _copyrightAnchor;
    private String _copyrightOffset;
    private String _copyrightContent;
    private boolean _maptypeControl;
    private String _maptypeAnchor;
    private String _maptypeOffset;
    private String _maptypeStyle;
    private String _maptypeArray;
    private boolean _trafficControl;
    private String _trafficAnchor;
    private String _trafficOffset;
    private boolean _navigationControl;
    private String _navigationAnchor;
    private String _navigationOffset;
    private String _navigationStyle;
    private boolean _geolocationControl;
    private String _geolocationAnchor;
    private String _geolocationOffset;
    //    private String _geolocationIcon;
    private boolean _autoLocation;
    private boolean _overviewMapControl;
    private String _overviewMapAnchor;
    private String _overviewMapOffset;
    private String _overviewMapSize;
    private boolean _drawingManager;
    private String _drawingMode;
    private String _drawingManagerAnchor;
    private String _drawingManagerOffset;
    private String[][] _drawingData;
    private String[][] _marks;
    private boolean _markerClusterer;
    private String[][] _customMarks;
    private String _position;
    private String _positionMark;
    private String _positionMarkSize;
    private boolean _drivingRoute;
    private String[] _drivingRoutePoints;


    public String getMapStyle() {
        return _mapStyle;
    }

    public void setMapStyle(String mapStyle) {
        this._mapStyle = mapStyle;
    }

    public String[] getAreaStyle() {
        return _areaStyle;
    }

    public void setAreaStyle(String[] areaStyle) {
        this._areaStyle = areaStyle;
        smartUpdate("areaStyle", _areaStyle);
    }

    public boolean getEnableMapClick() {
        return _enableMapClick;
    }

    public void setEnableMapClick(boolean enableMapClick) {
        if (this._enableMapClick != enableMapClick) {
            this._enableMapClick = enableMapClick;
            smartUpdate("enableMapClick", _enableMapClick);
        }
    }

    public boolean getInfobox() {
        return _infobox;
    }

    public void setInfobox(boolean infobox) {
        this._infobox = infobox;
        smartUpdate("infobox", _infobox);
    }

    public int getZoom() {
        return _zoom;
    }

    public void setZoom(int zoom) {
        this._zoom = zoom;
        smartUpdate("zoom", _zoom);
    }

    public boolean getDragging() {
        return _dragging;
    }

    public void setDragging(boolean dragging) {
        this._dragging = dragging;
    }

    public boolean getScrollWheelZoom() {
        return _scrollWheelZoom;
    }

    public void setScrollWheelZoom(boolean scrollWheelZoom) {
        this._scrollWheelZoom = scrollWheelZoom;
    }

    public boolean getPinchToZoom() {
        return _pinchToZoom;
    }

    public void setPinchToZoom(boolean pinchToZoom) {
        this._pinchToZoom = pinchToZoom;
    }

    public boolean getDoubleClickZoom() {
        return _doubleClickZoom;
    }

    public void setDoubleClickZoom(boolean doubleClickZoom) {
        this._doubleClickZoom = doubleClickZoom;
    }

    public boolean getChoosePoint() {
        return _choosePoint;
    }

    public void setChoosePoint(boolean choosePoint) {
        this._choosePoint = choosePoint;
        smartUpdate("choosePoint", _choosePoint);
    }

    public boolean getScaleControl() {
        return _scaleControl;
    }

    public void setScaleControl(boolean scaleControl) {
        this._scaleControl = scaleControl;
    }

    public String getScaleAnchor() {
        return _scaleAnchor;
    }

    public void setScaleAnchor(String scaleAnchor) {
        this._scaleAnchor = scaleAnchor;
    }

    public String getScaleOffset() {
        return _scaleOffset;
    }

    public void setScaleOffset(String scaleOffset) {
        this._scaleOffset = scaleOffset;
    }

    public boolean getCopyrightControl() {
        return _copyrightControl;
    }

    public void setCopyrightControl(boolean copyrightControl) {
        this._copyrightControl = copyrightControl;
    }

    public String getCopyrightAnchor() {
        return _copyrightAnchor;
    }

    public void setCopyrightAnchor(String copyrightAnchor) {
        this._copyrightAnchor = copyrightAnchor;
    }

    public String getCopyrightOffset() {
        return _copyrightOffset;
    }

    public void setCopyrightOffset(String copyrightOffset) {
        this._copyrightOffset = copyrightOffset;
    }

    public String getCopyrightContent() {
        return _copyrightContent;
    }

    public void setCopyrightContent(String copyrightContent) {
        this._copyrightContent = copyrightContent;
    }

    public boolean getMaptypeControl() {
        return _maptypeControl;
    }

    public void setMaptypeControl(boolean maptypeControl) {
        this._maptypeControl = maptypeControl;
    }

    public String getMaptypeAnchor() {
        return _maptypeAnchor;
    }

    public void setMaptypeAnchor(String maptypeAnchor) {
        this._maptypeAnchor = maptypeAnchor;
    }

    public String getMaptypeOffset() {
        return _maptypeOffset;
    }

    public void setMaptypeOffset(String maptypeOffset) {
        this._maptypeOffset = maptypeOffset;
    }

    public String getMaptypeStyle() {
        return _maptypeStyle;
    }

    public void setMaptypeStyle(String maptypeStyle) {
        this._maptypeStyle = maptypeStyle;
    }

    public String getMaptypeArray() {
        return _maptypeArray;
    }

    public void setMaptypeArray(String maptypeArray) {
        this._maptypeArray = maptypeArray;
    }

    public boolean getTrafficControl() {
        return _trafficControl;
    }

    public void setTrafficControl(boolean trafficControl) {
        this._trafficControl = trafficControl;
    }

    public String getTrafficAnchor() {
        return _trafficAnchor;
    }

    public void setTrafficAnchor(String trafficAnchor) {
        this._trafficAnchor = trafficAnchor;
    }

    public String getTrafficOffset() {
        return _trafficOffset;
    }

    public void setTrafficOffset(String trafficOffset) {
        this._trafficOffset = trafficOffset;
    }

    public boolean getNavigationControl() {
        return _navigationControl;
    }

    public void setNavigationControl(boolean navigationControl) {
        this._navigationControl = navigationControl;
    }

    public String getNavigationAnchor() {
        return _navigationAnchor;
    }

    public void setNavigationAnchor(String navigationAnchor) {
        this._navigationAnchor = navigationAnchor;
    }

    public String getNavigationOffset() {
        return _navigationOffset;
    }

    public void setNavigationOffset(String navigationOffset) {
        this._navigationOffset = navigationOffset;
    }

    public String getNavigationStyle() {
        return _navigationStyle;
    }

    public void setNavigationStyle(String navigationStyle) {
        this._navigationStyle = navigationStyle;
    }

    public boolean getGeolocationControl() {
        return _geolocationControl;
    }

    public void setGeolocationControl(boolean geolocationControl) {
        this._geolocationControl = geolocationControl;
    }

    public String getGeolocationAnchor() {
        return _geolocationAnchor;
    }

    public void setGeolocationAnchor(String geolocationAnchor) {
        this._geolocationAnchor = geolocationAnchor;
    }

    public String getGeolocationOffset() {
        return _geolocationOffset;
    }

    public void setGeolocationOffset(String geolocationOffset) {
        this._geolocationOffset = geolocationOffset;
    }

    public boolean getAutoLocation() {
        return _autoLocation;
    }

    public void setAutoLocation(boolean autoLocation) {
        this._autoLocation = autoLocation;
        smartUpdate("autoLocation", _autoLocation);
    }

    public boolean getOverviewMapControl() {
        return _overviewMapControl;
    }

    public void setOverviewMapControl(boolean overviewMapControl) {
        this._overviewMapControl = overviewMapControl;
    }

    public String getOverviewMapAnchor() {
        return _overviewMapAnchor;
    }

    public void setOverviewMapAnchor(String overviewMapAnchor) {
        this._overviewMapAnchor = overviewMapAnchor;
    }

    public String getOverviewMapOffset() {
        return _overviewMapOffset;
    }

    public void setOverviewMapOffset(String overviewMapOffset) {
        this._overviewMapOffset = overviewMapOffset;
    }

    public String getOverviewMapSize() {
        return _overviewMapSize;
    }

    public void setOverviewMapSize(String overviewMapSize) {
        this._overviewMapSize = overviewMapSize;
    }

    public boolean getDrawingManager() {
        return _drawingManager;
    }

    public void setDrawingManager(boolean drawingManager) {
        this._drawingManager = drawingManager;
    }

    public String getDrawingMode() {
        return _drawingMode;
    }

    public void setDrawingMode(String drawingMode) {
        this._drawingMode = drawingMode;
    }


    public String getDrawingManagerAnchor() {
        return _drawingManagerAnchor;
    }

    public void setDrawingManagerAnchor(String drawingManagerAnchor) {
        this._drawingManagerAnchor = drawingManagerAnchor;
    }

    public String getDrawingManagerOffset() {
        return _drawingManagerOffset;
    }

    public void setDrawingManagerOffset(String drawingManagerOffset) {
        this._drawingManagerOffset = drawingManagerOffset;
    }

    public String[][] getDrawingData() {
        return _drawingData;
    }

    public void setDrawingData(String[][] drawingData) {
        if (this._drawingData != drawingData) {
            this._drawingData = drawingData;
            smartUpdate("drawingData", _drawingData);
        }
    }

    public String[][] getMarks() {
        return _marks;
    }

    public void setMarks(String[][] marks) {
        if (this._marks != marks) {
            this._marks = marks;
            smartUpdate("marks", marks);
        }
    }

    public boolean getMarkerClusterer() {
        return _markerClusterer;
    }

    public void setMarkerClusterer(boolean markerClusterer) {
        this._markerClusterer = markerClusterer;
        smartUpdate("markerClusterer", markerClusterer);
    }

    public String[][] getCustomMarks() {
        return _customMarks;
    }

    public void setCustomMarks(String[][] customMarks) {
        if (this._customMarks != customMarks) {
            this._customMarks = customMarks;
            smartUpdate("customMarks", customMarks);
        }
    }

    public String getPosition() {
        return _position;
    }

    public void setPosition(String position) {
        this._position = position;
        smartUpdate("position", _position);
    }

    public String getPositionMark() {
        return _positionMark;
    }

    public void setPositionMark(String positionMark) {
        this._positionMark = positionMark;
        smartUpdate("positionMark", _positionMark);
    }

    public String getPositionMarkSize() {
        return _positionMarkSize;
    }

    public void setPositionMarkSize(String _positionMarkSize) {
        this._positionMarkSize = _positionMarkSize;
        smartUpdate("positionMarkSize", _positionMarkSize);
    }

    public String[] getDrivingRoutePoints() {
        return _drivingRoutePoints;
    }

    public void setDrivingRoutePoints(String[] drivingRoutePoints) {
        if (this._drivingRoutePoints != drivingRoutePoints) {
            this._drivingRoutePoints = drivingRoutePoints;
            smartUpdate("drivingRoutePoints", _drivingRoutePoints);
        }
    }

    public boolean isDrivingRoute() {
        return _drivingRoute;
    }

    public void setDrivingRoute(boolean drivingRoute) {
        if (drivingRoute != this._drivingRoute) {
            this._drivingRoute = drivingRoute;
            smartUpdate("_drivingRoute", drivingRoute);
        }
    }

    public void clearOverlays(){
        Clients.evalJavaScript("zk.$('"+ this.getUuid() +"').clearOverlays()");
    }

    public void showRouting(String type, String startPoint, String endPoint){
        Clients.evalJavaScript("zk.$('"+ this.getUuid() +"').showRouting('"+ type +"','"+ startPoint +"','"+ endPoint +"')");
    }

    public void search(String name){
        Clients.evalJavaScript("zk.$('"+ this.getUuid() +"').search('"+ name +"')");
    }

    public void service(org.zkoss.zk.au.AuRequest request, boolean everError) {
        final String cmd = request.getCommand();
        if (cmd.equals(Events.ON_CLICK)) {
            Event evt = new Event(cmd, this, request.getData());
            Events.postEvent(evt);
        }
        if (cmd.equals(ON_SAVE)) {
            Event evt = new Event(ON_SAVE, this, request.getData().get("data"));
            Events.postEvent(evt);
        }
        if (cmd.equals(ON_LOCATION)) {
            Event evt = new Event(ON_LOCATION, this, request.getData());
            Events.postEvent(evt);
        }
        if (cmd.equals(ON_SEARCH)) {
            Event evt = new Event(ON_SEARCH, this, request.getData());
            Events.postEvent(evt);
        }
    }

    protected void renderProperties(org.zkoss.zk.ui.sys.ContentRenderer renderer)
            throws IOException {
        super.renderProperties(renderer);
        renderer.render("mapStyle", _mapStyle);
        renderer.render("areaStyle", _areaStyle);
        renderer.render("enableMapClick", _enableMapClick);
        renderer.render("infobox", _infobox);
        renderer.render("zoom", _zoom);
        renderer.render("dragging", _dragging);
        renderer.render("scrollWheelZoom", _scrollWheelZoom);
        renderer.render("doubleClickZoom", _doubleClickZoom);
        renderer.render("pinchToZoom", _pinchToZoom);
        if (_scaleControl) {
            renderer.render("scaleControl", _scaleControl);
            if (!Strings.isBlank(_scaleAnchor))
                renderer.render("scaleAnchor", _scaleAnchor);
            if (!Strings.isBlank(_scaleOffset))
                renderer.render("scaleOffset", _scaleOffset);
        }
        if (_copyrightControl) {
            renderer.render("copyrightControl", _copyrightControl);
            if (!Strings.isBlank(_copyrightAnchor))
                renderer.render("copyrightAnchor", _copyrightAnchor);
            if (!Strings.isBlank(_copyrightOffset))
                renderer.render("copyrightOffset", _copyrightOffset);
            if (!Strings.isBlank(_copyrightContent))
                renderer.render("copyrightContent", _copyrightContent);
        }
        if (_maptypeControl) {
            renderer.render("maptypeControl", _maptypeControl);
            if (!Strings.isBlank(_maptypeAnchor))
                renderer.render("maptypeAnchor", _maptypeAnchor);
            if (!Strings.isBlank(_maptypeOffset))
                renderer.render("maptypeOffset", _maptypeOffset);
            if (!Strings.isBlank(_maptypeStyle))
                renderer.render("maptypeStyle", _maptypeStyle);
            if (!Strings.isBlank(_maptypeArray))
                renderer.render("maptypeArray", _maptypeArray);
        }
        if (_trafficControl) {
            renderer.render("trafficControl", _trafficControl);
            if (!Strings.isBlank(_trafficAnchor))
                renderer.render("trafficAnchor", _trafficAnchor);
            if (!Strings.isBlank(_trafficOffset))
                renderer.render("trafficOffset", _trafficOffset);
        }
        if (_navigationControl) {
            renderer.render("navigationControl", _navigationControl);
            if (!Strings.isBlank(_navigationAnchor))
                renderer.render("navigationAnchor", _navigationAnchor);
            if (!Strings.isBlank(_navigationOffset))
                renderer.render("navigationOffset", _navigationOffset);
            if (!Strings.isBlank(_navigationStyle))
                renderer.render("navigationStyle", _navigationStyle);
        }
        if (_geolocationControl) {
            renderer.render("geolocationControl", _geolocationControl);
            if (!Strings.isBlank(_geolocationAnchor))
                renderer.render("geolocationAnchor", _geolocationAnchor);
            if (!Strings.isBlank(_geolocationOffset))
                renderer.render("geolocationOffset", _geolocationOffset);
        }
        if (_overviewMapControl) {
            renderer.render("overviewMapControl", _overviewMapControl);
            if (!Strings.isBlank(_overviewMapAnchor))
                renderer.render("overviewMapAnchor", _overviewMapAnchor);
            if (!Strings.isBlank(_overviewMapOffset))
                renderer.render("overviewMapOffset", _overviewMapOffset);
            if (!Strings.isBlank(_overviewMapSize))
                renderer.render("overviewMapSize", _overviewMapSize);
        }
        if (_drawingManager) {
            renderer.render("drawingManager", _drawingManager);
            if (!Strings.isBlank(_drawingMode))
                renderer.render("drawingMode", _drawingMode);
            if (!Strings.isBlank(_drawingManagerAnchor))
                renderer.render("drawingManagerAnchor", _drawingManagerAnchor);
            if (!Strings.isBlank(_drawingManagerOffset))
                renderer.render("drawingManagerOffset", _drawingManagerOffset);
        }

        renderer.render("marks", _marks);
        renderer.render("markerClusterer", _markerClusterer);
        renderer.render("customMarks", _customMarks);
        renderer.render("choosePoint", _choosePoint);
        renderer.render("autoLocation", _autoLocation);
        renderer.render("position", _position);
        renderer.render("positionMark", _positionMark);
        renderer.render("positionMarkSize", _positionMarkSize);
        renderer.render("drivingRoute", _drivingRoute);
        renderer.render("drivingRoutePoints", _drivingRoutePoints);
        renderer.render("drawingData", _drawingData);
    }

    public void reload() {
        Widget ext = (Widget) getAttribute("$proxy");
        if (ext != null)
            ext.reload(this);
    }

}
