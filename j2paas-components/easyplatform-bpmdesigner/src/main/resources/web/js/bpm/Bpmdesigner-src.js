bpm.Bpmdesigner = zk.$extends(zul.Widget, {

	_model : "",
	_task : "",
	_order : "",
	_height : '200',
	_editable : false,
	_$elm : null,

	$define : {
		model : function(v) {
			this._model = v;
			if (this._snaker) {
				$.snakerflow.destroy(this.uuid);
				this._createDesigner();
			}
		},
		task : function(v) {
			this._task = v;
		},
		order : function(v) {
			this._order = v;
		},
		width : function(v) {
			if (!v || !this.$n())
				return;
			this._setSize(jq('#' + this.uuid), v, 'width');
		},
		height : function(v) {
			if (!v || !this.$n())
				return;
			this._setSize(jq('#' + this.uuid), v, 'height');
		},
		editable : function(v) {
			this._editable = v;
		}
	},
	setVflex : function(v) {
		if (v == 'min')
			v = false;
		if (this._snaker)
			this.$super(bpm.Bpmdesigner, 'setVflex', v);
		else
			this._tmpVflex = v;
	},
	setHflex : function(v) {
		if (v == 'min')
			v = false;
		if (this._snaker)
			this.$super(bpm.Bpmdesigner, 'setHflex', v);
		else
			this._tmpHflex = v;
	},
	setFlexSize_ : function(sz, ignoreMargins) {
		if (this._snaker) {
			var n = this.$n(), zkn = zk(n);
			if (sz.height !== undefined) {
				if (sz.height == 'auto')
					n.style.height = '';
				else if (sz.height != '')
					this.setFlexSizeH_(n, zkn, sz.height, ignoreMargins);
				else {
					n.style.height = this._height || '';
					if (this._height)
						this._setSize(this._height, 'height');
					else
						this._setSize('200px', 'height');
				}
			}
			if (sz.width !== undefined) {
				if (sz.width == 'auto')
					n.style.width = '';
				else if (sz.width != '')
					this.setFlexSizeW_(n, zkn, sz.width, ignoreMargins);
				else {
					n.style.width = this._width || '';
					if (this._width)
						this._setSize(this._width, 'width');
					else
						this._setSize('100%', 'width');
				}
			}
			return {
				height : n.offsetHeight,
				width : n.offsetWidth
			};
		}
	},
	setFlexSizeH_ : function(n, zkn, height, ignoreMargins) {
		this._hflexHeight = height;
		this.$super(bpm.Bpmdesigner, 'setFlexSizeH_', n, zkn, height,
				ignoreMargins);
		var h = parseInt(n.style.height);
		n.style.height = '';
		this._setSize(jq.px0(h), 'height');
	},
	setFlexSizeW_ : function(n, zkn, width, ignoreMargins) {
		this.$super(bpm.Bpmdesigner, 'setFlexSizeW_', n, zkn, width,
				ignoreMargins);
		var w = parseInt(n.style.width);
		this._setSize(jq.px0(w), 'width');
	},
	_setSize : function(value, prop) {
		value = this._getValue(value);
		if (!value)
			return;
		if (prop == 'width')
			jq(this.$n()).width(value);
		else
			jq(this.$n()).height(value);
	},
	_getValue : function(value) {
		if (!value)
			return null;
		if (value.endsWith('%'))
			return zk.ie ? jq.px0(jq(this.$n()).width()) : value;
		return jq.px0(zk.parseInt(value));
	},
	bind_ : function() {
		this.$supers('bind_', arguments);
		var wgt = this;
		setTimeout(function() {
			wgt._init();
		}, 50);
	},

	unbind_ : function() {
		this.$supers('unbind_', arguments);
		if (this._snaker)
			$.snakerflow.destroy(this.uuid);
		this._snaker = null;
	},
	_createDesigner : function() {
		var packagePath = zk.ajaxURI('/web/js/bpm/ext/snaker/', {
			desktop : this.desktop,
			au : true
		});
		this._snaker = $('#' + this.uuid + '-canvas').snakerflow(
				$.extend(true, {
					basePath : packagePath.substr(0, packagePath
							.lastIndexOf("/") + 1),
					orderId : this._order,
					restore : jq.evalJSON(this._model),
					editable : this._editable,
					zkWgt : this,
				}, eval("(" + this._task + ")")));
	},
	_init : function() {
		this._createDesigner();
		if (!this._tmpHflex && this._hflex) {
			this._tmpHflex = this._hflex;
			this.setHflex(null);
		}
		if (!this._tmpVflex && this._vflex) {
			this._tmpVflex = this._vflex;
			this.setVflex(null);
		}
		if (this._tmpHflex) {
			this.setHflex(this._tmpHflex, {
				force : true
			});
			this._tmpHflex = null;
		}
		if (this._tmpVflex) {
			this.setVflex(this._tmpVflex, {
				force : true
			});
			this._tmpVflex = null;
		}
//		if (this._editable)
//			jq(this).css('overflow', 'scroll');
//		else
//			jq(this).css('overflow', 'hidden');
	},
	setElement : function(elm) {
		this._$elm = elm;
	},
	setNodeState : function(json) {
		var obj = $.parseJSON(json);
		var tb = this._$elm.find("table");
		tb.empty();
		tb.append("<tr><td><span class='z-label'>" + msgepez['BPM_TASK_NAME']
				+ ":</span></td><td><span class='label label-info'>" + obj.name
				+ "</span></td></tr>");
		tb.append("<tr><td><span class='z-label'>" + msgepez['BPM_TASK_ACTORS']
				+ ":</span></td><td><span class='label label-info'>"
				+ obj.actors + "</span></td></tr>");
		tb.append("<tr><td><span class='z-label'>"
				+ msgepez['BPM_TASK_CREATETIME']
				+ ":</span></td><td><span class='label label-info'>"
				+ obj.createTime + "</span></td></tr>");
		tb.append("<tr><td><span class='z-label'>"
				+ msgepez['BPM_TASK_FINISHTIME']
				+ ":</span></td><td><span class='label label-info'>"
				+ obj.finishTime + "</span></td></tr>");
		this._$elm.show();
	},
	setInputValue : function(value) {
		this._$elm.val(value);
		this._$elm.trigger('change');
	}
});