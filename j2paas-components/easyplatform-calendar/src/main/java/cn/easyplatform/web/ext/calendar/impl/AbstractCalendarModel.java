/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.calendar.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import org.zkoss.io.Serializables;

import cn.easyplatform.web.ext.calendar.api.CalendarEvent;
import cn.easyplatform.web.ext.calendar.api.CalendarModel;
import cn.easyplatform.web.ext.calendar.event.CalendarDataEvent;
import cn.easyplatform.web.ext.calendar.event.CalendarDataListener;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
abstract public class AbstractCalendarModel implements CalendarModel, Serializable {
	private static final long serialVersionUID = 20090317164525L;
	private transient List<CalendarDataListener> _listeners = new LinkedList<CalendarDataListener>();

	/** Fires a {@link CalendarDataEvent} for all registered listener
	 * (thru {@link #addCalendarDataListener}.
	 *
	 * @see #fireEvent(int, Date, Date, TimeZone)
	 */
	protected void fireEvent(int type, CalendarEvent e) {
		fireEvent(type, e, null);
	}
	/** 
	 * @deprecated As of release 2.0-RC, replaced with {@link #fireEvent(int type, CalendarEvent e, TimeZone timezone)}
	 * Fires a {@link CalendarDataEvent} for all registered listener
	 * (thru {@link #addCalendarDataListener}.
	 *
	 * <p>Note: you can invoke this method only in an event listener.
	 */
	protected void fireEvent(int type, Date begin, Date end, TimeZone timezone) {
		final CalendarDataEvent evt = new CalendarDataEvent(this, type, begin, end, timezone);
		for (Iterator<CalendarDataListener> it = _listeners.iterator(); it.hasNext();)
			it.next().onChange(evt);
	}
	/** Fires a {@link CalendarDataEvent} for all registered listener
	 * (thru {@link #addCalendarDataListener}.
	 *
	 * <p>Note: you can invoke this method only in an event listener.
	 */
	protected void fireEvent(int type, CalendarEvent e, TimeZone timezone) {
		final CalendarDataEvent evt = new CalendarDataEvent(this, type, e, timezone);
		for (Iterator<CalendarDataListener> it = _listeners.iterator(); it.hasNext();)
			it.next().onChange(evt);
	}
	
	public void addCalendarDataListener(CalendarDataListener l) {
		if (l == null)
			throw new NullPointerException();
		_listeners.add(l);
	}

	public void removeCalendarDataListener(CalendarDataListener l) {
		_listeners.remove(l);
	}
	
	//Serializable//
	private synchronized void writeObject(java.io.ObjectOutputStream s)
	throws java.io.IOException {
		s.defaultWriteObject();
		Serializables.smartWrite(s, _listeners);
	}
	private synchronized void readObject(java.io.ObjectInputStream s)
	throws java.io.IOException, ClassNotFoundException {
		s.defaultReadObject();

		_listeners = new LinkedList<CalendarDataListener>();
		Serializables.smartRead(s, _listeners);
	}
}
