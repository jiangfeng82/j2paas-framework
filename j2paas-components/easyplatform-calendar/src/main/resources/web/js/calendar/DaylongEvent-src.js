calendar.DaylongEvent = zk.$extends(calendar.LongEvent, {
	
	getCornerStyle_: function() {
		return '';
	},
	
	getDays: function() {
		var node = this.$n();
		return this.parent._days - node._preOffset - node._afterOffset;
	}
});