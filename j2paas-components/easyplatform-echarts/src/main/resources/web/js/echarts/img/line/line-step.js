{
    title: {
        text: 'Step Line'
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data: ['Step Start', 'Step Middle', 'Step End']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        data: 'data'
    },
    yAxis: {
        type: 'value'
    },
    series: [
        {
            name: 'Step Start',
            type: 'line',
            step: 'start',
            data: 'data'
        },
        {
            name: 'Step Middle',
            type: 'line',
            step: 'middle',
            data: 'data'
        },
        {
            name: 'Step End',
            type: 'line',
            step: 'end',
            data: 'data'
        }
    ]
}