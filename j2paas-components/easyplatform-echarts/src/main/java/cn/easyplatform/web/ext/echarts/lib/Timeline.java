/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib;

import cn.easyplatform.web.ext.echarts.lib.style.*;
import cn.easyplatform.web.ext.echarts.lib.support.Point;
import cn.easyplatform.web.ext.echarts.lib.type.Orient;

/**
 * Description: Timeline
 *
 * @author liuzh
 */
public class Timeline extends Point {
    /**
     * 默认值true,显示策略，可选为：true（显示） | false（隐藏）
     */
    private Boolean show;
    /**
     * 默认为time,模式是时间类型，时间轴间隔根据时间跨度计算，可选为：'number'
     */
    private Object type = "slider";
    /**
     * 轴的类型
     */
    private String axisType;
    /**
     * 默认值0，当前索引位置，对应options数组，用于指定显示特定系列
     */
    private Integer currentIndex;
    /**
     * 默认值false,是否自动播放
     */
    private Boolean autoPlay;
    /**
     * 表示是否反向播放
     */
    private Boolean rewind;
    /**
     * 默认值true,是否循环播放
     */
    private Boolean loop;
    /**
     * 默认值2000，播放时间间隔，单位ms
     */
    private Integer playInterval;
    /**
     * 默认值true,拖拽或点击改变时间轴是否实时显示
     */
    private Boolean realtime;
    /**
     * 默认值left,播放控制器位置，可选为：'left' | 'right' | 'none'
     */
    private Object controlPosition;
    /**
     * 默认值5，内边距，单位px，默认各方向内边距为5，接受数组分别设定上右下左边距，同css，见下图
     */
    private Integer padding;
    /**
     * 摆放方式
     */
    private Object orient;
    /**
     * 是否反向放置 timeline，反向则首位颠倒过来
     */
    private Boolean inverse;
    /**
     * timeline标记的图形。
     * ECharts 提供的标记类型包括 'circle', 'rect', 'roundRect', 'triangle', 'diamond', 'pin', 'arrow'
     */
    private Object symbol;
    /**
     * timeline标记的大小，可以设置成诸如 10 这样单一的数字，也可以用数组分开表示宽和高，例如 [20, 10] 表示标记宽为20，高为10
     */
    private Object symbolSize;
    /**
     * timeline标记的旋转角度。注意在 markLine 中当 symbol 为 'arrow' 时会忽略 symbolRotate 强制设置为切线的角度
     */
    private Integer symbolRotate;
    /**
     * timeline标记相对于原本位置的偏移。默认情况下，标记会居中置放在数据对应的位置，但是如果 symbol 是自定义的矢量路径或者图片，就有可能不希望 symbol 居中。这时候可以使用该配置项配置 symbol 相对于原本居中的偏移，可以是绝对的像素值，也可以是相对的百分比
     */
    private Object[] symbolOffset;
    /**
     * 默认值{color: '#666', width: 1, type: 'dashed'}，时间轴轴线样式lineStyle控制线条样式，
     */
    private LineStyle lineStyle;
    /**
     * 轴的文本标签。有 normal 和 emphasis 两个状态，normal 是文本正常的样式，emphasis 是文本高亮的样式，比如鼠标悬浮或者图例联动高亮的时候会使用 emphasis 作为文本的样式
     */
    private LabelStyle label;
    /**
     * timeline 图形样式，有 normal 和 emphasis 两个状态。normal 是图形在默认状态下的样式；emphasis 是图形在高亮状态下的样式，比如在鼠标悬浮或者图例联动高亮时
     */
    private ItemStyle itemStyle;
    /**
     * 时间轴当前点
     *
     * @see CheckpointStyle
     */
    private CheckpointStyle checkpointStyle;
    /**
     * 时间轴控制器样式，可指定正常和高亮颜色
     */
    private ControlStyle controlStyle;

    private Emphasis emphasis;

    /**
     * timeline 数据。Array 的每一项，可以是直接的数值。 如果需要对每个数据项单独进行样式定义，则数据项写成 Object
     */
    private Object[] data;

    public Emphasis emphasis() {
        return emphasis;
    }

    public Timeline emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public Object[] data() {
        return data;
    }

    public Timeline data(Object... data) {
        this.data = data;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return itemStyle;
    }

    public Timeline itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public Object[] symbolOffset() {
        return symbolOffset;
    }

    public Timeline symbolOffset(Object... symbolOffset) {
        this.symbolOffset = symbolOffset;
        return this;
    }

    public Integer symbolRotate() {
        return symbolRotate;
    }

    public Timeline symbolRotate(Integer symbolRotate) {
        this.symbolRotate = symbolRotate;
        return this;
    }

    public Object orient() {
        return orient;
    }

    public Timeline orient(Object orient) {
        this.orient = orient;
        return this;
    }

    public String axisType() {
        return this.axisType;
    }

    public Timeline axisType(String axisType) {
        this.axisType = axisType;
        return this;
    }

    public Boolean rewind() {
        return this.rewind;
    }

    public Timeline rewind(Boolean rewind) {
        this.rewind = rewind;
        return this;
    }

    public Boolean inverse() {
        return this.inverse;
    }

    public Timeline inverse(Boolean inverse) {
        this.inverse = inverse;
        return this;
    }

    public String getAxisType() {
        return axisType;
    }

    public void setAxisType(String axisType) {
        this.axisType = axisType;
    }

    public Boolean getRewind() {
        return rewind;
    }

    public void setRewind(Boolean rewind) {
        this.rewind = rewind;
    }

    public Boolean getInverse() {
        return inverse;
    }

    public void setInverse(Boolean inverse) {
        this.inverse = inverse;
    }

    /**
     * 获取show值
     */
    public Boolean show() {
        return this.show;
    }

    /**
     * 设置show值
     *
     * @param show
     */
    public Timeline show(Boolean show) {
        this.show = show;
        return this;
    }

    /**
     * 获取type值
     */
    public Object type() {
        return this.type;
    }

    /**
     * 设置type值
     *
     * @param type
     */
    public Timeline type(Object type) {
        this.type = type;
        return this;
    }

    /**
     * 获取realtime值
     */
    public Boolean realtime() {
        return this.realtime;
    }

    /**
     * 设置realtime值
     *
     * @param realtime
     */
    public Timeline realtime(Boolean realtime) {
        this.realtime = realtime;
        return this;
    }

    /**
     * 获取padding值
     */
    public Integer padding() {
        return this.padding;
    }

    /**
     * 设置padding值
     *
     * @param padding
     */
    public Timeline padding(Integer padding) {
        this.padding = padding;
        return this;
    }

    /**
     * 获取controlPosition值
     */
    public Object controlPosition() {
        return this.controlPosition;
    }

    /**
     * 设置controlPosition值
     *
     * @param controlPosition
     */
    public Timeline controlPosition(Object controlPosition) {
        this.controlPosition = controlPosition;
        return this;
    }

    /**
     * 获取autoPlay值
     */
    public Boolean autoPlay() {
        return this.autoPlay;
    }

    /**
     * 设置autoPlay值
     *
     * @param autoPlay
     */
    public Timeline autoPlay(Boolean autoPlay) {
        this.autoPlay = autoPlay;
        return this;
    }

    /**
     * 获取loop值
     */
    public Boolean loop() {
        return this.loop;
    }

    /**
     * 设置loop值
     *
     * @param loop
     */
    public Timeline loop(Boolean loop) {
        this.loop = loop;
        return this;
    }

    /**
     * 获取playInterval值
     */
    public Integer playInterval() {
        return this.playInterval;
    }

    /**
     * 设置playInterval值
     *
     * @param playInterval
     */
    public Timeline playInterval(Integer playInterval) {
        this.playInterval = playInterval;
        return this;
    }

    /**
     * 默认值{color: '#666', width: 1, type: 'dashed'}，时间轴轴线样式lineStyle控制线条样式，
     *
     * @see LineStyle
     */
    public LineStyle lineStyle() {
        if (this.lineStyle == null) {
            this.lineStyle = new LineStyle();
        }
        return this.lineStyle;
    }

    /**
     * @see LabelStyle
     */
    public LabelStyle label() {
        if (this.label == null) {
            this.label = new LabelStyle();
        }
        return this.label;
    }

    /**
     * 时间轴当前点
     *
     * @see CheckpointStyle
     */
    public CheckpointStyle checkpointStyle() {
        if (this.checkpointStyle == null) {
            this.checkpointStyle = new CheckpointStyle();
        }
        return this.checkpointStyle;
    }

    /**
     * 时间轴控制器样式，可指定正常和高亮颜色
     */
    public ControlStyle controlStyle() {
        if (this.controlStyle == null) {
            this.controlStyle = new ControlStyle();
        }
        return this.controlStyle;
    }

    /**
     * 获取symbol值
     */
    public Object symbol() {
        return this.symbol;
    }

    /**
     * 设置symbol值
     *
     * @param symbol
     */
    public Timeline symbol(Object symbol) {
        this.symbol = symbol;
        return this;
    }

    /**
     * 获取symbolSize值
     */
    public Object symbolSize() {
        return this.symbolSize;
    }

    /**
     * 设置symbolSize值
     *
     * @param symbolSize
     */
    public Timeline symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    /**
     * 获取currentIndex值
     */
    public Integer currentIndex() {
        return this.currentIndex;
    }

    /**
     * 设置currentIndex值
     *
     * @param currentIndex
     */
    public Timeline currentIndex(Integer currentIndex) {
        this.currentIndex = currentIndex;
        return this;
    }

    /**
     * 获取lineStyle值
     */
    public LineStyle getLineStyle() {
        return lineStyle;
    }

    /**
     * 设置lineStyle值
     *
     * @param lineStyle
     */
    public void setLineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    /**
     * 获取label值
     */
    public LabelStyle getLabel() {
        return label;
    }

    /**
     * 设置label值
     *
     * @param label
     */
    public void setLabel(LabelStyle label) {
        this.label = label;
    }

    /**
     * 获取checkpointStyle值
     */
    public CheckpointStyle getCheckpointStyle() {
        return checkpointStyle;
    }

    /**
     * 设置checkpointStyle值
     *
     * @param checkpointStyle
     */
    public void setCheckpointStyle(CheckpointStyle checkpointStyle) {
        this.checkpointStyle = checkpointStyle;
    }

    /**
     * 获取controlStyle值
     */
    public ControlStyle getControlStyle() {
        return controlStyle;
    }

    /**
     * 设置controlStyle值
     *
     * @param controlStyle
     */
    public void setControlStyle(ControlStyle controlStyle) {
        this.controlStyle = controlStyle;
    }

    /**
     * 获取show值
     */
    public Boolean getShow() {
        return show;
    }

    /**
     * 设置show值
     *
     * @param show
     */
    public void setShow(Boolean show) {
        this.show = show;
    }

    /**
     * 获取type值
     */
    public Object getType() {
        return type;
    }

    /**
     * 设置type值
     *
     * @param type
     */
    public void setType(Object type) {
        this.type = type;
    }

    /**
     * 获取realtime值
     */
    public Boolean getRealtime() {
        return realtime;
    }

    /**
     * 设置realtime值
     *
     * @param realtime
     */
    public void setRealtime(Boolean realtime) {
        this.realtime = realtime;
    }

    /**
     * 获取padding值
     */
    public Integer getPadding() {
        return padding;
    }

    /**
     * 设置padding值
     *
     * @param padding
     */
    public void setPadding(Integer padding) {
        this.padding = padding;
    }

    /**
     * 获取controlPosition值
     */
    public Object getControlPosition() {
        return controlPosition;
    }

    /**
     * 设置controlPosition值
     *
     * @param controlPosition
     */
    public void setControlPosition(Orient controlPosition) {
        this.controlPosition = controlPosition;
    }

    /**
     * 获取autoPlay值
     */
    public Boolean getAutoPlay() {
        return autoPlay;
    }

    /**
     * 设置autoPlay值
     *
     * @param autoPlay
     */
    public void setAutoPlay(Boolean autoPlay) {
        this.autoPlay = autoPlay;
    }

    /**
     * 获取loop值
     */
    public Boolean getLoop() {
        return loop;
    }

    /**
     * 设置loop值
     *
     * @param loop
     */
    public void setLoop(Boolean loop) {
        this.loop = loop;
    }

    /**
     * 获取playInterval值
     */
    public Integer getPlayInterval() {
        return playInterval;
    }

    /**
     * 设置playInterval值
     *
     * @param playInterval
     */
    public void setPlayInterval(Integer playInterval) {
        this.playInterval = playInterval;
    }

    /**
     * 获取symbol值
     */
    public Object getSymbol() {
        return symbol;
    }

    /**
     * 设置symbol值
     *
     * @param symbol
     */
    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    /**
     * 获取symbolSize值
     */
    public Object getSymbolSize() {
        return symbolSize;
    }

    /**
     * 设置symbolSize值
     *
     * @param symbolSize
     */
    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    /**
     * 获取currentIndex值
     */
    public Integer getCurrentIndex() {
        return currentIndex;
    }

    /**
     * 设置currentIndex值
     *
     * @param currentIndex
     */
    public void setCurrentIndex(Integer currentIndex) {
        this.currentIndex = currentIndex;
    }

    public void setControlPosition(Object controlPosition) {
        this.controlPosition = controlPosition;
    }

    public Object getOrient() {
        return orient;
    }

    public void setOrient(Object orient) {
        this.orient = orient;
    }

    public Integer getSymbolRotate() {
        return symbolRotate;
    }

    public void setSymbolRotate(Integer symbolRotate) {
        this.symbolRotate = symbolRotate;
    }

    public Object[] getSymbolOffset() {
        return symbolOffset;
    }

    public void setSymbolOffset(Object[] symbolOffset) {
        this.symbolOffset = symbolOffset;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }
}
