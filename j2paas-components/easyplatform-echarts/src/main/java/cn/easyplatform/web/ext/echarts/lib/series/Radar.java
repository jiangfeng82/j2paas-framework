/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Radar extends Series {
    /**
     * 雷达图所使用的 radar 组件的 index
     */
    private Integer radarIndex;
    /**
     * 线条样式
     */
    private ItemStyle lineStyle;
    /**
     * 区域填充样式
     */
    private ItemStyle areaStyle;
    /**
     * 标志图形类型，默认自动选择（8种类型循环使用，不显示标志图形可设为'none'）
     */
    private Object symbol;
    /**
     * 标志图形大小，可计算特性启用情况建议增大以提高交互体验。实现气泡图时symbolSize需为Function，气泡大小取决于该方法返回值，传入参数为当前数据项（value数组）
     */
    private Object symbolSize;
    /**
     * 标志图形旋转角度[-180,180]
     */
    private Object symbolRotate;
    /**
     * 标记相对于原本位置的偏移
     */
    private Object symbolOffset;
    /**
     * 如果 symbol 是 path:// 的形式，是否在缩放时保持该图形的长宽比
     */
    private Boolean symbolKeepAspect;

    public Radar() {
        this.type="radar";
    }

    public Boolean symbolKeepAspect() {
        return this.symbolKeepAspect;
    }

    public Radar symbolKeepAspect(Boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
        return this;
    }

    public Integer radarIndex() {
        return this.radarIndex;
    }

    public Radar radarIndex(Integer radarIndex) {
        this.radarIndex = radarIndex;
        return this;
    }

    public ItemStyle lineStyle() {
        if (lineStyle == null)
            lineStyle = new ItemStyle();
        return this.lineStyle;
    }

    public Radar lineStyle(ItemStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public ItemStyle areaStyle() {
        if (areaStyle == null)
            areaStyle = new ItemStyle();
        return this.areaStyle;
    }

    public Radar areaStyle(ItemStyle areaStyle) {
        this.areaStyle = areaStyle;
        return this;
    }

    public Object symbol() {
        return this.symbol;
    }

    public Radar symbol(Object symbol) {
        this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return this.symbolSize;
    }

    public Radar symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public Object symbolRotate() {
        return this.symbolRotate;
    }

    public Radar symbolRotate(Object symbolRotate) {
        this.symbolRotate = symbolRotate;
        return this;
    }

    public Object symbolOffset() {
        return this.symbolOffset;
    }

    public Radar symbolOffset(Object value) {
            this.symbolOffset = value;
        return this;
    }

    public Radar symbolOffset(Object o1, Object o2) {
        this.symbolOffset = new Object[]{o1, o2};
        return this;
    }

    public Integer getRadarIndex() {
        return radarIndex;
    }

    public void setRadarIndex(Integer radarIndex) {
        this.radarIndex = radarIndex;
    }

    public ItemStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(ItemStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public ItemStyle getAreaStyle() {
        return areaStyle;
    }

    public void setAreaStyle(ItemStyle areaStyle) {
        this.areaStyle = areaStyle;
    }

    public Object getSymbol() {
        return symbol;
    }

    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public Object getSymbolRotate() {
        return symbolRotate;
    }

    public void setSymbolRotate(Object symbolRotate) {
        this.symbolRotate = symbolRotate;
    }

    public Object getSymbolOffset() {
        return symbolOffset;
    }

    public void setSymbolOffset(Object symbolOffset) {
        this.symbolOffset = symbolOffset;
    }

    public Boolean getSymbolKeepAspect() {
        return symbolKeepAspect;
    }

    public void setSymbolKeepAspect(Boolean symbolKeepAspect) {
        this.symbolKeepAspect = symbolKeepAspect;
    }
}
