/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib;

import cn.easyplatform.web.ext.echarts.lib.style.LabelStyle;
import cn.easyplatform.web.ext.echarts.lib.style.LineStyle;
import cn.easyplatform.web.ext.echarts.lib.style.ShadowStyle;
import cn.easyplatform.web.ext.echarts.lib.type.PointerType;

import java.io.Serializable;

/**
 * 坐标轴指示器，坐标轴触发有效
 *
 * @author liuzh
 */
public class AxisPointer implements Serializable {

    private static final long serialVersionUID = 6421899185681683630L;

    /**
     * 是否显示
     */
    private Boolean show;

    /**
     * 默认为直线，可选为：'line' | 'shadow' | 'cross'
     *
     * @see PointerType
     */
    private Object type;
    /**
     * 设置直线指示器
     *
     * @see LineStyle
     */
    private LineStyle lineStyle;
    /**
     * 设置阴影指示器
     */
    private ShadowStyle shadowStyle;
    /******************以下属性是3.5.1添加的*******************************/

    /**
     * 坐标轴指示器是否自动吸附到点上。默认自动判断
     * 这个功能在数值轴和时间轴上比较有意义，可以自动寻找细小的数值点。
     *
     * @since 3.5.1
     */
    private Boolean snap;
    /**
     * 坐标轴指示器的 z 值。控制图形的前后顺序。z值小的图形会被z值大的图形覆盖
     *
     * @since 3.5.1
     */
    private Integer z;
    /**
     * 坐标轴指示器的文本标签
     *
     * @since 3.5.1
     */
    private LabelStyle label;
    /**
     * 不同轴的 axisPointer 可以进行联动，在这里设置。联动表示轴能同步一起活动。轴依据他们的 axisPointer 当前对应的值来联动
     *
     * @since 3.5.1
     */
    private Object link;
    /**
     * 是否触发 tooltip。如果不想触发 tooltip 可以关掉
     *
     * @since 3.5.1
     */
    private Boolean triggerTooltip;
    /**
     * 当前的 value。在使用 axisPointer.handle 时，可以设置此值进行初始值设定，从而决定 axisPointer 的初始位置
     */
    private Object value;
    /**
     * 当前的状态，可取值为 'show' 和 'hide'
     */
    private Object status;
    /**
     * 提示框触发的条件，可选：'mousemove'鼠标移动时触发。'click'鼠标点击时触发。'none'
     */
    private Object triggerOn;
    /**
     * 拖拽手柄，适用于触屏的环境
     */
    private Handle handle;

    public Object link() {
        return this.link;
    }

    public AxisPointer link(Object... link) {
        if (link.length == 1)
            this.link = link[0];
        else
            this.link = link;
        return this;
    }

    public Boolean triggerTooltip() {
        return this.triggerTooltip;
    }

    public AxisPointer triggerTooltip(Boolean triggerTooltip) {
        this.triggerTooltip = triggerTooltip;
        return this;
    }

    public Object value() {
        return this.value;
    }

    public AxisPointer value(Object value) {
        this.value = value;
        return this;
    }

    public Object status() {
        return this.status;
    }

    public AxisPointer status(Object status) {
        this.status = status;
        return this;
    }

    public Object triggerOn() {
        return this.triggerOn;
    }

    public AxisPointer triggerOn(Object triggerOn) {
        this.triggerOn = triggerOn;
        return this;
    }

    public Handle handle() {
        if (handle == null)
            handle = new Handle();
        return this.handle;
    }

    public AxisPointer handle(Handle handle) {
        this.handle = handle;
        return this;
    }

    public Integer z() {
        return this.z;
    }

    public AxisPointer z(Integer z) {
        this.z = z;
        return this;
    }

    public Boolean snap() {
        return this.snap;
    }

    public AxisPointer snap(Boolean snap) {
        this.snap = snap;
        return this;
    }

    public AxisPointer lineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public AxisPointer shadowStyle(ShadowStyle shadowStyle) {
        this.shadowStyle = shadowStyle;
        return this;
    }

    public LineStyle lineStyle() {
        if (this.lineStyle == null)
            this.lineStyle = new LineStyle();
        return this.lineStyle;
    }

    public ShadowStyle shadowStyle() {
        if (this.shadowStyle == null)
            this.shadowStyle = new ShadowStyle();
        return this.shadowStyle;
    }

    public Object type() {
        return this.type;
    }

    public AxisPointer type(Object type) {
        this.type = type;
        return this;
    }

    public Boolean show() {
        return this.show;
    }

    public AxisPointer show(Boolean show) {
        this.show = show;
        return this;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public LineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }


    public ShadowStyle getShadowStyle() {
        return shadowStyle;
    }

    public void setShadowStyle(ShadowStyle shadowStyle) {
        this.shadowStyle = shadowStyle;
    }

    public Object getType() {
        return type;
    }

    public void setType(Object type) {
        this.type = type;
    }


    public Boolean getSnap() {
        return snap;
    }

    public void setSnap(Boolean snap) {
        this.snap = snap;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public LabelStyle getLabel() {
        return label;
    }

    public void setLabel(LabelStyle label) {
        this.label = label;
    }

    public Object getLink() {
        return link;
    }

    public void setLink(Object link) {
        this.link = link;
    }

    public Boolean getTriggerTooltip() {
        return triggerTooltip;
    }

    public void setTriggerTooltip(Boolean triggerTooltip) {
        this.triggerTooltip = triggerTooltip;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getTriggerOn() {
        return triggerOn;
    }

    public void setTriggerOn(Object triggerOn) {
        this.triggerOn = triggerOn;
    }

    public Handle getHandle() {
        return handle;
    }

    public void setHandle(Handle handle) {
        this.handle = handle;
    }

    public static class Handle implements Serializable {
        private Boolean show;
        private Object icon;
        private Object size;
        private Object margin;
        private Object color;
        private Object throttle;
        private Object shadowBlur;
        private Object shadowColor;
        private Object shadowOffsetX;
        private Object shadowOffsetY;

        public Boolean show() {
            return this.show;
        }

        public Handle show(Boolean show) {
            this.show = show;
            return this;
        }

        public Object icon() {
            return this.icon;
        }

        public Handle icon(Object icon) {
            this.icon = icon;
            return this;
        }

        public Object size() {
            return this.size;
        }

        public Handle size(Object size) {
            this.size = size;
            return this;
        }

        public Object margin() {
            return this.margin;
        }

        public Handle margin(Object margin) {
            this.margin = margin;
            return this;
        }

        public Object color() {
            return this.color;
        }

        public Handle color(Object color) {
            this.color = color;
            return this;
        }

        public Object throttle() {
            return this.throttle;
        }

        public Handle throttle(Object throttle) {
            this.throttle = throttle;
            return this;
        }

        public Object shadowBlur() {
            return this.shadowBlur;
        }

        public Handle shadowBlur(Object shadowBlur) {
            this.shadowBlur = shadowBlur;
            return this;
        }

        public Object shadowColor() {
            return this.shadowColor;
        }

        public Handle shadowColor(Object shadowColor) {
            this.shadowColor = shadowColor;
            return this;
        }

        public Object shadowOffsetX() {
            return this.shadowOffsetX;
        }

        public Handle shadowOffsetX(Object shadowOffsetX) {
            this.shadowOffsetX = shadowOffsetX;
            return this;
        }

        public Object shadowOffsetY() {
            return this.shadowOffsetY;
        }

        public Handle shadowOffsetY(Object shadowOffsetY) {
            this.shadowOffsetY = shadowOffsetY;
            return this;
        }
    }
}
