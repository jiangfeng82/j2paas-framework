/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.data;

import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;

import java.io.Serializable;

/**
 * 力导向图中节点的分类
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class FunnelData implements Serializable {

    private static final long serialVersionUID = 5805816011061262622L;

    private Object name;

    private Object value;

    private ItemStyle itemStyle;

    private ItemStyle label;

    private ItemStyle labelLine;

    public FunnelData(Object name) {
        this.name = name;
    }

    public FunnelData(Object name, Object value) {
        this.name = name;
        this.value = value;
    }

    public Object name() {
        return this.name;
    }

    public FunnelData name(Object name) {
        this.name = name;
        return this;
    }

    public Object value() {
        return this.value;
    }

    public FunnelData value(Object value) {
        this.value = value;
        return this;
    }

    public ItemStyle label() {
        if (this.label == null) {
            this.label = new ItemStyle();
        }
        return this.label;
    }

    public FunnelData label(ItemStyle label) {
        this.label = label;
        return this;
    }

    public ItemStyle itemStyle() {
        if (this.itemStyle == null) {
            this.itemStyle = new ItemStyle();
        }
        return this.itemStyle;
    }

    public FunnelData itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public ItemStyle labelLine() {
        if (this.labelLine == null) {
            this.labelLine = new ItemStyle();
        }
        return this.labelLine;
    }

    public FunnelData labelLine(ItemStyle labelLine) {
        this.labelLine = labelLine;
        return this;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public ItemStyle getLabel() {
        return label;
    }

    public void setLabel(ItemStyle label) {
        this.label = label;
    }

    public ItemStyle getLabelLine() {
        return labelLine;
    }

    public void setLabelLine(ItemStyle labelLine) {
        this.labelLine = labelLine;
    }
}
