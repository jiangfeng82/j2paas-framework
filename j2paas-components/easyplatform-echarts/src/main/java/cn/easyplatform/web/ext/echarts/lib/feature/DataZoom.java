/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.feature;

import cn.easyplatform.web.ext.echarts.lib.style.Emphasis;
import cn.easyplatform.web.ext.echarts.lib.style.TextStyle;
import org.zkoss.util.resource.Labels;

import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DataZoom extends Feature {

    private Object xAxisIndex;

    private Object yAxisIndex;

    private Icon icon;

    private TextStyle iconStyle;

    private Emphasis emphasis;

    public DataZoom() {
        this.show(true);
        Map title = new HashMap<String, String>();
        title.put("zoom", Labels.getLabel("echarts.title.zoom"));
        title.put("back", Labels.getLabel("echarts.title.back"));
        this.title(title);
    }

    public Emphasis emphasis() {
        if (emphasis == null)
            emphasis = new Emphasis();
        return emphasis;
    }

    public DataZoom emphasis(Emphasis emphasis) {
        this.emphasis = emphasis;
        return this;
    }

    public DataZoom iconStyle(TextStyle iconStyle) {
        this.iconStyle = iconStyle;
        return this;
    }

    public TextStyle iconStyle() {
        if (this.iconStyle == null)
            this.iconStyle = new TextStyle();
        return this.iconStyle;
    }

    public Object xAxisIndex() {
        return this.xAxisIndex;
    }

    public DataZoom xAxisIndex(Boolean xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
        return this;
    }

    public Object icon() {
        if (icon == null)
            icon = new Icon();
        return this.icon;
    }

    public DataZoom icon(Icon icon) {
        this.icon = icon;
        return this;
    }

    public Object yAxisIndex() {
        return this.yAxisIndex;
    }

    public DataZoom yAxisIndex(Boolean yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
        return this;
    }

    public Object getxAxisIndex() {
        return xAxisIndex;
    }

    public void setxAxisIndex(Object xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    public Object getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(Object yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

    public TextStyle getIconStyle() {
        return iconStyle;
    }

    public void setIconStyle(TextStyle iconStyle) {
        this.iconStyle = iconStyle;
    }

    public static class Icon {
        private String zoom;
        private String back;

        public String zoom() {
            return zoom;
        }

        public void zoom(String zoom) {
            this.zoom = zoom;
        }

        public String back() {
            return back;
        }

        public void back(String back) {
            this.back = back;
        }

        public String getZoom() {
            return zoom;
        }

        public void setZoom(String zoom) {
            this.zoom = zoom;
        }

        public String getBack() {
            return back;
        }

        public void setBack(String back) {
            this.back = back;
        }
    }
}
