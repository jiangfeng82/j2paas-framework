/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.style;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Normal extends Emphasis {
    /**
     * 文本位置，'left' / 'right' / 'top' / 'bottom'
     */
    private Object textPosition;
    /**
     * 文本对齐方式，'left' / 'right'
     */
    private Object textAlign;

    private Object width;

    private Object height;

    private Object curveness;

    private AreaStyle areaStyle;

    private TextStyle textStyle;

    private Object gapWidth;

    private Object borderColorSaturation;

    public Object gapWidth() {
        return gapWidth;
    }

    public Normal gapWidth(Object gapWidth) {
        this.gapWidth = gapWidth;
        return this;
    }

    public Object borderColorSaturation() {
        return borderColorSaturation;
    }

    public Normal borderColorSaturation(Object borderColorSaturation) {
        this.borderColorSaturation = borderColorSaturation;
        return this;
    }

    public TextStyle textStyle() {
        if (textStyle == null)
            textStyle = new TextStyle();
        return this.textStyle;
    }

    public Normal textStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
        return this;
    }

    public AreaStyle areaStyle() {
        if (areaStyle == null)
            areaStyle = new AreaStyle();
        return this.areaStyle;
    }

    public Normal areaStyle(AreaStyle areaStyle) {
        this.areaStyle = areaStyle;
        return this;
    }

    public Object curveness() {
        return curveness;
    }

    public Normal curveness(Object curveness) {
        this.curveness = curveness;
        return this;
    }


    public Object height() {
        return height;
    }

    public Normal height(Object height) {
        this.height = height;
        return this;
    }

    public Object width() {
        return width;
    }

    public Normal width(Object width) {
        this.width = width;
        return this;
    }

    public Object textPosition() {
        return textPosition;
    }

    public Normal textPosition(Object textPosition) {
        this.textPosition = textPosition;
        return this;
    }

    public Object textAlign() {
        return textAlign;
    }

    public Normal textAlign(Object textAlign) {
        this.textAlign = textAlign;
        return this;
    }

    public Object getTextPosition() {
        return textPosition;
    }

    public void setTextPosition(Object textPosition) {
        this.textPosition = textPosition;
    }

    public Object getTextAlign() {
        return textAlign;
    }

    public void setTextAlign(Object textAlign) {
        this.textAlign = textAlign;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public Object getCurveness() {
        return curveness;
    }

    public void setCurveness(Object curveness) {
        this.curveness = curveness;
    }

    public AreaStyle getAreaStyle() {
        return areaStyle;
    }

    public void setAreaStyle(AreaStyle areaStyle) {
        this.areaStyle = areaStyle;
    }

    public TextStyle getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
    }

    public Object getGapWidth() {
        return gapWidth;
    }

    public void setGapWidth(Object gapWidth) {
        this.gapWidth = gapWidth;
    }

    public Object getBorderColorSaturation() {
        return borderColorSaturation;
    }

    public void setBorderColorSaturation(Object borderColorSaturation) {
        this.borderColorSaturation = borderColorSaturation;
    }
}
