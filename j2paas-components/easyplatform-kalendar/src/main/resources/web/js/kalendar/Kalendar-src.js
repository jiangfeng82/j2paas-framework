/*
 * create by 陈云亮(shiny_vc@163.com)
 */
kalendar.Kalendar = zk.$extends(zul.Widget, {
	_value : null,
	_unused : null,
	
	$define : {
		value : function(v) {
			if (this.$n())
				this._createKalendar();
		},
		unused : function(v){
			if (this.$n())
				this._createKalendar();
		}
	},

	_createKalendar : function() {
		var wgt = this;
		var year = parseInt(this._value.substring(0,4));
		var month = parseInt(this._value.substring(5,7));
		var day = parseInt(this._value.substring(8,10));
		var date = new Date(this._value);
		var lastDay = new Date(year,month,0).getDate();
		var dayOfWeek = date.getDay();
		if(dayOfWeek == 0)
			dayOfWeek = 7;
		var ctx = zk.ajaxURI('/web/js/kalendar/img/', {
			desktop : this.desktop,
			au : true
		});
		var ctx = ctx.substr(0, ctx.lastIndexOf("/") + 1);
		var index = 1;
		var indexOfDay = day == lastDay ? 0 : day;
		var totalDay = 0;
		var nyear = year;
		var nmonth = month;
		$("#" + this.uuid + " .real").each(function() {
			$(this).find('li').each(function() {
				$(this).css('cursor','default').unbind().empty();
				if(index < dayOfWeek || totalDay >= 31){
					$('<br/><img src="'+ctx+'meiyou.gif"/>').appendTo($(this));
				}else{
					var unused = false;
					if(index == dayOfWeek){
						$(this).data('val',wgt._value).text(wgt._value.substring(5,7)+'/'+wgt._value.substring(8,10));
						unused = wgt._isUnused(year,month,day);
						if(unused)
							$('<br/><img src="'+ctx+'batu.gif"/>').appendTo($(this));
						else
							$('<br/><img src="'+ctx+'maru.gif"/>').appendTo($(this));
						indexOfDay++;
					}else if(indexOfDay == 1){
						lastDay=31-(lastDay-day);
						if(month == 12){
							nmonth = 1;
							nyear++;
						}else
							nmonth++;
						$(this).data('val',nyear+'/'+nmonth+'/'+indexOfDay).text((nmonth > 9 ? nmonth : '0'+nmonth)+'/0'+indexOfDay);
						unused = wgt._isUnused(nyear,nmonth,indexOfDay);
						if(unused)
							$('<br/><img src="'+ctx+'batu.gif"/>').appendTo($(this));
						else
							$('<br/><img src="'+ctx+'maru.gif"/>').appendTo($(this));
						indexOfDay++;
					}else if(indexOfDay <= lastDay){
						$(this).data('val',nyear+'/'+nmonth+'/'+indexOfDay).text(indexOfDay > 9 ? ''+indexOfDay : '0'+indexOfDay);
						unused = wgt._isUnused(nyear,nmonth,indexOfDay);
						if(unused)
							$('<br/><img src="'+ctx+'batu.gif"/>').appendTo($(this));
						else
							$('<br/><img src="'+ctx+'maru.gif"/>').appendTo($(this));
						if(indexOfDay==lastDay)
							indexOfDay = 1;
						else
							indexOfDay++;
					}
					totalDay++;
					if(!unused){
						$(this).css('cursor','pointer').bind('click',function() {
							zAu.send(new zk.Event(wgt, 'onSelect', {
								val : $(this).data('val')
							}), 0);
						});
					}
						
				}
				index++;
			});
		});
	},
    
	_isUnused : function(year,month,day){
		if(!this._unused)
			return false;
		var dates = this._unused.split(',');
		for(var i = 0; i < dates.length; i++){
			var y = parseInt(dates[i].substring(0,4));
			var m = parseInt(dates[i].substring(4,6));
			var d = parseInt(dates[i].substring(6,8));
			if(year == y && m == month && day == d)
				return true;
		}
		return false;
	},
	
	bind_ : function() {
		this.$supers(kalendar.Kalendar, 'bind_', arguments);
		this._createKalendar();
	},
	
	unbind_ : function() {
		this.$supers(kalendar.Kalendar, 'unbind_', arguments);
	}
});
