/* NavigationLevel.java

	Purpose:

	Description:

	History:
		Fri Aug 24 12:09:26 CST 2018, Created by rudyhuang

Copyright (C) 2018 Potix Corporation. All Rights Reserved.
*/
package org.zkoss.zuti.zul;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.zkoss.util.Pair;

/**
 * An object that represents the navigation level.
 *
 * @param <T> The data type
 * @author rudyhuang
 * @since 8.6.0
 */
public interface NavigationLevel<T> {
	/**
	 * Gets the level number.
	 * The first level is 1.
	 *
	 * @return the level number
	 */
	int getLevel();

	/**
	 * Gets the context of this level if any.
	 *
	 * @return the context map. Might be {@code null}
	 */
	Map<String, Object> getContext();

	/**
	 * Sets the context of this level.
	 *
	 * @param context the context map
	 * @return this object
	 */
	NavigationLevel<T> setContext(Map<String, Object> context);

	/**
	 * Gets the current key. If no data exists in this level, the result is {@code null}.
	 *
	 * @return the current key. Might be {@code null}
	 */
	@org.zkoss.bind.annotation.DependsOn("current")
	String getCurrentKey();

	/**
	 * Gets the data associated with the current key if any.
	 *
	 * @return the data object. Might be {@code null}
	 */
	T getCurrent();

	/**
	 * Gets the child navigation level if any.
	 *
	 * @return the child navigation level. Might be {@code null}
	 */
	@org.zkoss.bind.annotation.DependsOn("current")
	NavigationLevel<T> getChild();

	/**
	 * Navigates to the specified key in the current level.
	 * If the specified key does not exist, it will throw an exception.
	 *
	 * @param key the item key
	 * @return this object
	 * @throws IllegalArgumentException if the key is invalid or not found in the level
	 */
	NavigationLevel<T> navigateTo(String key);

	/**
	 * Gets the item iterator in this level.
	 * The {@link Pair#getX()} is the key, and {@link Pair#getY()} is the data.
	 *
	 * @return the item iterator
	 */
	Iterator<Pair<String, T>> getItemIterator();

	/**
	 * Gets the list of items in this level.
	 * Note: The performance is no good.
	 *
	 * @return the list of items
	 */
	List<Pair<String, T>> getItems();
}
