/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.lang.Lang;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.web.ext.*;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.ext.Disable;
import org.zkoss.zul.Button;
import org.zkoss.zul.Grid;

import java.util.Iterator;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Buttongroup extends Grid implements ZkExt, Reloadable, Disable,
        Assignable, Queryable {
    /**
     *
     */
    private static final long serialVersionUID = -804686716002814536L;

    /**
     * 查询语句
     */
    private Object _query;

    /**
     * 数据连接的资源id
     */
    private String _dbId;

    /**
     * 在显示时是否要马上执行查询
     */
    private boolean _immediate = true;

    /**
     * Buttongroup的列数
     */
    private int _cols = 1;

    /**
     *
     */
    private boolean _disabled;

    /**
     * 过滤表达式,target一定存在
     */
    private String _filter;

    /**
     * 自定义按钮，用逗号分隔，此时组件的值是以按钮所在排列顺序作为值
     */
    private String _buttons;

    /**
     * 按钮选中时的风格,btn-default,btn-primary,btn-success,btn-info,btn-warning,btn-
     * danger,btn-link
     */
    private String _jackOn = "primary";

    /**
     * 按钮未选中时的风格
     */
    private String _jackOff = "default";

    /**
     * 选中的按钮值
     */
    private Object _value;

    /**
     * 允许有一个自定义的选项
     */
    private String _emptyLabel;

    /**
     * 自定义选项的值
     */
    private String _emptyValue = "";

    /**
     * 子项风格
     */
    private String _itemStyle;

    /**
     * 选中时样式
     */
    private String _selItemStyle;

    /**
     * 大小:lg,sm,xs
     */
    private String _size;
    /**
     * 是否必需重新加载
     */
    private boolean _force;

    @Override
    public boolean isForce() {
        return _force;
    }

    public void setForce(boolean force) {
        this._force = force;
    }

    public String getSize() {
        return _size;
    }

    public void setSize(String size) {
        this._size = size;
    }

    /**
     * @return the selItemStyle
     */
    public String getSelItemStyle() {
        return _selItemStyle;
    }

    /**
     * @param selItemStyle the selItemStyle to set
     */
    public void setSelItemStyle(String selItemStyle) {
        this._selItemStyle = selItemStyle;
    }

    /**
     * @return the itemStyle
     */
    public String getItemStyle() {
        return _itemStyle;
    }

    /**
     * @param itemStyle the itemStyle to set
     */
    public void setItemStyle(String itemStyle) {
        this._itemStyle = itemStyle;
    }

    public String getLabel() {
        Iterator<Component> itr = queryAll("button").iterator();
        while (itr.hasNext()) {
            Button cb = (Button) itr.next();
            Object o = cb.getAttribute("value");
            if (Lang.equals(o, _value))
                return cb.getLabel();
        }
        return "";
    }

    public String getEmptyLabel() {
        return _emptyLabel;
    }

    public void setEmptyLabel(String emptyLabel) {
        this._emptyLabel = emptyLabel;
    }

    public String getEmptyValue() {
        return _emptyValue;
    }

    public void setEmptyValue(String emptyValue) {
        this._emptyValue = emptyValue;
    }

    public Object getValue() {
        return _value;
    }

    public void setValue(Object value) {
        this._value = value;
        Iterator<Component> itr = queryAll("button").iterator();
        StringBuilder sb = new StringBuilder();
        while (itr.hasNext()) {
            Button cb = (Button) itr.next();
            Object o = cb.getAttribute("value");
            sb.setLength(0);
            sb.append("btn btn-block btn-");
            if (Lang.equals(o, _value))
                sb.append(_jackOn);
            else
                sb.append(_jackOff);
            if (!Strings.isBlank(_size))
                sb.append(" btn-").append(_size);
            cb.setZclass(sb.toString());
        }
        sb = null;
    }

    public String getJackOn() {
        return _jackOn;
    }

    public void setJackOn(String jackOn) {
        this._jackOn = jackOn;
    }

    public String getJackOff() {
        return _jackOff;
    }

    public void setJackOff(String jackOff) {
        this._jackOff = jackOff;
    }

    public String getFilter() {
        return _filter;
    }

    public void setFilter(String filter) {
        this._filter = filter;
    }

    public boolean isImmediate() {
        return _immediate;
    }

    public void setImmediate(boolean immediate) {
        this._immediate = immediate;
    }

    public Object getQuery() {
        return _query;
    }

    public void setQuery(Object query) {
        this._query = query;
    }

    public String getDbId() {
        return _dbId;
    }

    public void setDbId(String dbId) {
        this._dbId = dbId;
    }

    public int getCols() {
        return _cols;
    }

    public void setCols(int cols) {
        this._cols = cols;
    }

    @Override
    public void reload() {
        Widget ext = (Widget) getAttribute("$proxy");
        ext.reload(this);
    }

    @Override
    public boolean isDisabled() {
        return _disabled;
    }

    @Override
    public void setDisabled(boolean disabled) {
        if (this._disabled != disabled) {
            Iterator<Component> itr = queryAll("button").iterator();
            while (itr.hasNext()) {
                Button cb = (Button) itr.next();
                cb.setDisabled(disabled);
            }
            this._disabled = disabled;
        }
    }

    public String getButtons() {
        return _buttons;
    }

    public void setButtons(String buttons) {
        this._buttons = buttons;
    }
}
