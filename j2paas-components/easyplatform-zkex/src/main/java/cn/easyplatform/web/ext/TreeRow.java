/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext;

import cn.easyplatform.type.FieldVo;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TreeRow {

    private Treeitem item;

    /**
     * @param item
     */
    public TreeRow(Treeitem item) {
        this.item = item;
    }

    public Object getValue(int index) {
        Node node = item.getValue();
        FieldVo[] data = node.getData();
        return data[index];
    }

    public Object getValue(String name) {
        Node node = item.getValue();
        FieldVo[] data = node.getData();
        for (FieldVo fv : data) {
            if (fv.getName().equalsIgnoreCase(name))
                return fv.getValue();
        }
        return null;
    }

    public void setValue(int index, Object value) {
        Node node = item.getValue();
        FieldVo[] data = node.getData();
        data[index].setValue(value);
        for (int i = 0; i < item.getChildren().size(); i++) {
            if (index == i) {
                ((Treecell) item.getChildren().get(i))
                        .setLabel(value == null ? "" : value.toString());
                break;
            }
        }
    }

    public void setValue(String name, Object value) {
        Node node = item.getValue();
        FieldVo[] data = node.getData();
        int index = 0;
        for (; index < data.length; index++) {
            if (data[index].getName().equalsIgnoreCase(name))
                break;
        }
        setValue(index, value);
    }
}
