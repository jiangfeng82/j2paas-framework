/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext;

import cn.easyplatform.type.FieldVo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Node {
    private Object id;
    private Object parentId;
    private String name;
    private Node parent;
    private List<Node> children;
    private int level;
    private FieldVo[] data;
    private String index;

    public Node(int size) {
        children = new ArrayList<>(size);
    }

    public Node(Node root) {
        children = new ArrayList<>(1);
        children.add(root);
    }

    public Node(String id) {
        this.id = id;
        children = new ArrayList<>();
    }

    public Node(Object id, String name) {
        this.id = id;
        this.name = name;
    }

    public Node(Object id, Object parentId, String name) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;
    }

    public Node(Object id, Object parentId, FieldVo[] data) {
        this.id = id;
        this.parentId = parentId;
        this.data = data;
    }

    public Node(Object id, FieldVo[] data) {
        this.id = id;
        this.data = data;
    }

    public Node(Object id, FieldVo[] data, String name) {
        this.id = id;
        this.data = data;
        this.name = name;
    }

    public Node(FieldVo[] data) {
        this.data = data;
    }

    public void appendChild(Node node) {
        if (children == null)
            children = new ArrayList<>();
        node.level = this.level + 1;
        node.parent = this;
        children.add(node);
        node.index = this.index == null ? this.children.size() + "" : this.index + "." + children.size();
    }

    public void resetLevel() {
        if (children != null) {
            for (int i = 0; i < children.size(); i++) {
                Node node = children.get(i);
                node.level = this.level + 1;
                node.index = this.index == null ? (i + 1) + "" : this.index + "." + (i + 1);
                node.resetLevel();
            }
        }
    }

    public Object getId() {
        return id;
    }

    public Object getParentId() {
        return parentId;
    }

    public String getName() {
        return name;
    }

    public Node getParent() {
        return parent;
    }

    public List<Node> getChildren() {
        return children;
    }

    public int getLevel() {
        return level;
    }

    public boolean isLeaf() {
        return children == null || children.isEmpty();
    }

    public FieldVo[] getData() {
        return data;
    }

    public String getIndex() {
        return index;
    }
}
