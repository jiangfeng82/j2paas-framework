/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.cmez;

import org.zkoss.zk.ui.Component;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DropEvent extends org.zkoss.zk.ui.event.DropEvent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int line;
	
	private int ch;
	
	/**
	 * @param name
	 * @param target
	 * @param dragged
	 * @param x
	 * @param y
	 * @param pageX
	 * @param pageY
	 * @param keys
	 */
	public DropEvent(String name, Component target, Component dragged, int x,
			int y, int pageX, int pageY, int keys,int line,int ch) {
		super(name, target, dragged, x, y, pageX, pageY, keys);
		this.line = line;
		this.ch = ch;
	}

	/**
	 * @return the line
	 */
	public int getLine() {
		return line;
	}

	/**
	 * @return the ch
	 */
	public int getCh() {
		return ch;
	}

}
