/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform;

import java.util.Map;

import org.zkoss.lang.Objects;
import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.impl.XulElement;

public class Datetimepicker extends XulElement {

	static {
		addClientEvent(Datetimepicker.class, "onSelect", 0);
	}

	private String _value;
	private String _placeholder;
	private boolean _readonly;
	private String _format;
	private String _locale;
	private boolean _showClose;
	private boolean _showTodayButton;
	private boolean _showClear;

	protected void renderProperties(org.zkoss.zk.ui.sys.ContentRenderer renderer)
	throws java.io.IOException {
		super.renderProperties(renderer);

		render(renderer, "value", _value);
		render(renderer, "placeholder", _placeholder);
		render(renderer, "readonly", _readonly);
		render(renderer, "format", _format);
		render(renderer, "locale", _locale);
		render(renderer, "showClose", _showClose);
		render(renderer, "showTodayButton", _showTodayButton);
		render(renderer, "showClear", _showClear);
	}

	public void service(AuRequest request, boolean everError) {
		final String cmd = request.getCommand();
		final Map data = request.getData();

		if (cmd.equals("onSelect")) {
			final String datetime = (String)data.get("datetime");
//			System.out.println("do onSelect, data:" + datetime.getClass());
			Events.postEvent(Event.getEvent(request));
		} else
			super.service(request, everError);
	}

	/**
	 * The default zclass is "z-datetimepicker"
	 */
	public String getZclass() {
		return (this._zclass != null ? this._zclass : "z-datetimepicker");
	}


	public String getFormat() {
		return _format;
	}

	public void setFormat(String format) {
		if (!Objects.equals(_format, format)) {
			_format = format;
			smartUpdate("format", _format);
		}
	}

	public String getValue() {
		return _value;
	}

	public void setValue(String value) {
		if (!Objects.equals(_value, value)) {
			this._value = value;
			smartUpdate("value", _value);
		}
	}

	public String getPlaceholder() {
		return _placeholder;
	}

	public void setPlaceholder(String _placeholder) {
		this._placeholder = _placeholder;
	}

	public boolean isReadonly() {
		return _readonly;
	}

	public void setReadonly(boolean _readonly) {
		this._readonly = _readonly;
	}

	public String getLocale() {
		return _locale;
	}

	public void setLocale(String _locale) {
		this._locale = _locale;
	}

	public boolean isShowClose() {
		return _showClose;
	}

	public void setShowClose(boolean _showClose) {
		this._showClose = _showClose;
	}

	public boolean isShowTodayButton() {
		return _showTodayButton;
	}

	public void setShowTodayButton(boolean _showTodayButton) {
		this._showTodayButton = _showTodayButton;
	}

	public boolean isShowClear() {
		return _showClear;
	}

	public void setShowClear(boolean _showClear) {
		this._showClear = _showClear;
	}
}

