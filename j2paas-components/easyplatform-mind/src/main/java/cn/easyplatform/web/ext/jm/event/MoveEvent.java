/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.jm.event;

import java.util.Map;
import java.util.Objects;

import cn.easyplatform.web.ext.jm.Node;
import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MoveEvent extends Event {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String sourceId;

    private String targetId;

    private String direction;

    public MoveEvent(String name, Component target, Node data) {
        super(name, target, data);
    }

    public static final MoveEvent getNodeEvent(AuRequest request, Node node) {
        final Component comp = request.getComponent();
        final Map<String, Object> data = request.getData();
        MoveEvent evt = new MoveEvent(request.getCommand(), comp, node);
        evt.sourceId = (String) data.get("opid");
        evt.targetId = (String) data.get("npid");
        evt.direction = Objects.equals(data.get("direct"), -1) ? "left" : "right";
        return evt;
    }

    /**
     * @return the sourceId
     */
    public String getSourceId() {
        return sourceId;
    }

    /**
     * @return the targetId
     */
    public String getTargetId() {
        return targetId;
    }

    public String getDirection() {
        return direction;
    }
}
