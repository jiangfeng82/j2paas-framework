/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.component;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.dos.Record;
import cn.easyplatform.engine.runtime.datalist.DataListUtils;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.FieldUpdateVo;
import cn.easyplatform.messages.vos.datalist.ListFieldUpdateVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;

import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class FieldUpdateCmd extends AbstractCommand<SimpleRequestMessage> {

	/**
	 * @param req
	 */
	public FieldUpdateCmd(SimpleRequestMessage req) {
		super(req);
	}

	@SuppressWarnings("unchecked")
	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		if (req.getBody() instanceof ListFieldUpdateVo)
			return updateList(cc, (ListFieldUpdateVo) req.getBody());
		else
			return updatePage(cc.getWorkflowContext().getRecord(),
					(List<FieldUpdateVo>) req.getBody());
	}

	private IResponseMessage<?> updateList(CommandContext cc,
			ListFieldUpdateVo uvs) {
		ListContext lc = cc.getWorkflowContext().getList(uvs.getId());
		if (lc == null)
			return MessageUtils.dataListNotFound(uvs.getId());
		RecordContext rc = lc.getRecord(uvs.getKeys());
		if (rc == null) {
			Record record = DataListUtils.getRecord(cc, lc, uvs.getKeys());
			rc = lc.createRecord(uvs.getKeys(), record);
			rc.setParameter("814", 'R');
			lc.appendRecord(rc);
		}
		if (rc.getParameterAsChar("814") != 'C')
			rc.setParameter("814", 'U');
		rc.setParameter("815", Boolean.TRUE);
		return updatePage(rc, uvs.getFields());
	}

	private IResponseMessage<?> updatePage(RecordContext rc,
			List<FieldUpdateVo> uvs) {
		for (FieldUpdateVo uv : uvs)
			rc.setValue(uv.getName(), uv.getValue());
		return new SimpleResponseMessage();
	}

}
