/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.application;

import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.i18n.I18N;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Nums;
import cn.easyplatform.messages.request.SimpleTextRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.type.IResponseMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/9/2 9:45
 * @Modified By:
 */
public class GetWizardCmd extends AbstractCommand<SimpleTextRequestMessage> {

    public GetWizardCmd(SimpleTextRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        List<FieldDo> params = new ArrayList<>();
        params.add(new FieldDo(FieldType.INT, Nums.toInt(req.getBody(), 0)));
        FieldDo[] field = cc.getBizDao().selectOne("SELECT guideStep FROM sys_guide_info WHERE guideID=?", params);
        if (field == null)
            return new SimpleResponseMessage("e024", I18N.getLabel("context.record.not.found", req.getBody()));
        return new SimpleResponseMessage(field[0].getValue());
    }
}
