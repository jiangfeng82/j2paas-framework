/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.runtime.jxls;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.vos.ReportVo;
import org.jxls.common.Context;
import org.jxls.transform.poi.PoiTransformer;

import java.util.List;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/10/17 9:56
 * @Modified By:
 */
abstract class AbstractJxlsReportHandler<T> {

    /**
     * 根据源创建内容
     *
     * @param ctx
     * @param rv
     * @param source
     * @return
     */
    abstract Context createContext(CommandContext ctx, ReportVo rv, T source);

    /**
     * 创建记录内容
     *
     * @param rc
     * @return
     */
    Context create(WorkflowContext wc, RecordContext rc, int layer) {
        Context ctx = PoiTransformer.createInitialContext(rc.toMap());
        ctx.putVar("jdbc", new JdbcHelper());
        List<ListContext> lists = wc.getLists(layer);
        for (ListContext lc : lists)
            ctx.putVar(lc.getId(), lc.toMapList(layer, false));
        return ctx;
    }
}
