/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.task;

import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.entities.helper.EventLogic;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.RefreshRequestMessage;
import cn.easyplatform.messages.response.FieldsUpdateResponseMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.support.scripting.BreakPoint;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;
import cn.easyplatform.util.RuntimeUtils;

import java.util.HashMap;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class RefreshCmd extends AbstractCommand<RefreshRequestMessage> {

	/**
	 * @param req
	 */
	public RefreshCmd(RefreshRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		if (req.getActionFlag() < 0) {
			cc.setBreakPoint(null);
			return new SimpleResponseMessage();
		}
		Map<String, Object> data = req.getBody();
		BreakPoint bp = cc.getBreakPoint();
		WorkflowContext ctx = cc.getWorkflowContext();
		EventLogic el = RuntimeUtils.castTo(cc, ctx.getRecord(),
				ctx.getParameter("836"));
		if (bp == null) {
			for (Map.Entry<String, Object> entry : data.entrySet()) {
				FieldDo fd = ctx.getRecord().getField(entry.getKey());
				fd.setValue(RuntimeUtils.castTo(fd, entry.getValue()));
			}
			if (el != null) {
				String result = RuntimeUtils.eval(cc, el, ctx.getRecord());
				if (!result.equals("0000"))
					return MessageUtils.byErrorCode(cc, ctx.getRecord(),
							ctx.getId(), result);
			}
		} else if (el != null && bp.isInstance(el)) {
			String result = RuntimeUtils.eval(cc, bp);
			if (!result.equals("0000"))
				return MessageUtils.byErrorCode(cc, ctx.getRecord(),
						ctx.getId(), result);
		}
		Map<String, Object> map = new HashMap<String, Object>();
		RecordContext rc = ctx.getRecord();
		for (String name : data.keySet())
			map.put(name, rc.getValue(name));
		return new FieldsUpdateResponseMessage(map);
	}

	@Override
	public String getName() {
		return "task.Refresh";
	}
}
