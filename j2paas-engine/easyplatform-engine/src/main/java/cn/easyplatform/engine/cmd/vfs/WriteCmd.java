/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.vfs;

import cn.easyplatform.i18n.I18N;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.vfs.WriteRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.VfsVo;
import cn.easyplatform.type.IResponseMessage;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class WriteCmd extends AbstractCommand<WriteRequestMessage> {

    private static Logger log = LoggerFactory.getLogger(WriteCmd.class);

    /**
     * @param req
     */
    public WriteCmd(WriteRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        VfsVo fv = req.getBody();
        if (fv.getPath().startsWith("$7")) {
            String path = cc.getRealPath(fv.getPath());
            OutputStream os = null;
            try {
                File file = new File(path);
                os = new BufferedOutputStream(new FileOutputStream(file));
                IOUtils.write(fv.getData(), os);
                return new SimpleResponseMessage();
            } catch (Exception e) {
                log.error("vfs write", e);
                return new SimpleResponseMessage("e027", I18N.getLabel("vfs.access.error", fv.getName()));
            } finally {
                IOUtils.closeQuietly(os);
            }
        } else {
            File file = new File(req.getBody().getPath());
            OutputStream os = null;
            try {
                os = new BufferedOutputStream(new FileOutputStream(file));
                IOUtils.write(req.getBody().getData(), os);
            } catch (IOException e) {
                log.error("vfs write", e);
                return new SimpleResponseMessage("e027", I18N.getLabel("vfs.access.error", fv.getName()));
            } finally {
                IOUtils.closeQuietly(os);
            }
        }
        return new SimpleResponseMessage();
    }

}
