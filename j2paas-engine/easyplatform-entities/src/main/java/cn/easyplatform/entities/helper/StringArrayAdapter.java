/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.helper;

import javax.xml.bind.annotation.adapters.XmlAdapter;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class StringArrayAdapter extends XmlAdapter<String, String[]> {

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
     */
    @Override
    public String[] unmarshal(String v) throws Exception {
        return v.split("\\,");
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
     */
    @Override
    public String marshal(String[] v) throws Exception {
        if (v.length == 0)
            return "";
        else if (v.length == 1)
            return v[0];
        StringBuilder sb = new StringBuilder();
        for (String s : v)
            sb.append(s).append(',');
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

}
