
package cn.easyplatform.support.scripting;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface DebugEventListener {

	void onLineChange(int lineNumber);
	
	void onExceptionThrown(Throwable ex);
	
	void onExit(boolean byThrow, Object resultOrException);
}
