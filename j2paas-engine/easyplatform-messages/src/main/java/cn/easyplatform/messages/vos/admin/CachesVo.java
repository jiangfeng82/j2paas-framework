/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.admin;

import java.io.Serializable;
import java.util.List;
/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class CachesVo implements Serializable {

    private boolean entityEnabled;
    private boolean resourceEnabled;
    private List<CacheVo> entityList;
    private List<CacheVo> resourceList;

    public CachesVo(boolean entityEnabled, boolean resourceEnabled, List<CacheVo> entityList, List<CacheVo> resourceList) {
        this.entityEnabled = entityEnabled;
        this.resourceEnabled = resourceEnabled;
        this.entityList = entityList;
        this.resourceList = resourceList;
    }

    public boolean isEntityEnabled() {
        return entityEnabled;
    }

    public boolean isResourceEnabled() {
        return resourceEnabled;
    }

    public List<CacheVo> getEntityList() {
        return entityList;
    }

    public List<CacheVo> getResourceList() {
        return resourceList;
    }
}
