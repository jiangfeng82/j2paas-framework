/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import java.io.Serializable;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ExpressionVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int type;

	private String expr;

	private Map<String, Object> values;

	public ExpressionVo(int type, String expr) {
		this(type, expr, null);
	}

	public ExpressionVo(int type, String expr, Map<String, Object> values) {
		this.type = type;
		this.expr = expr;
		this.values = values;
	}

	public String getExpr() {
		return expr;
	}

	public int getType() {
		return type;
	}

	public Map<String, Object> getValues() {
		return values;
	}

}
