/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import cn.easyplatform.type.FieldVo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TaskRuntimeVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<String, Object> systemVars = new HashMap<String, Object>();

	private List<FieldVo> userVars = new ArrayList<FieldVo>();

	private List<FieldVo> fieldVars = new ArrayList<FieldVo>();

	private List<ListVo> listVars = null;
	
	public Map<String, Object> getSystemVars() {
		return systemVars;
	}

	public void setSystemVar(String name, Object value) {
		this.systemVars.put(name, value);
	}

	public List<FieldVo> getUserVars() {
		return userVars;
	}

	public void setUserVar(FieldVo var) {
		this.userVars.add(var);
	}

	public List<FieldVo> getFieldVars() {
		return fieldVars;
	}

	public void setFieldVar(FieldVo var) {
		this.fieldVars.add(var);
	}

	public List<ListVo> getListVars() {
		return listVars;
	}

	public void setListVar(ListVo lv) {
		if(listVars == null)
			listVars = new ArrayList<ListVo>();
		listVars.add(lv);
	}

}
