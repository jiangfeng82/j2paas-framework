/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ReportVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

	private String entityId;

	private String type;

	private String from;

	private char state;

	private String ids;

	private String listOrder;

	private String init;

	private Boolean showProgress;
	
	/**
	 * @param id
	 * @param entityId
	 * @param type
	 * @param from
	 * @param state
	 * @param ids
	 */
	public ReportVo(String id, String entityId, String type, String from,
			char state, String ids, String listOrder, String init,Boolean showProgress) {
		this.id = id;
		this.entityId = entityId;
		this.type = type;
		this.from = from;
		this.state = state;
		this.ids = ids;
		this.listOrder = listOrder;
		this.init = init;
		this.showProgress = showProgress;
	}

	public String getEntityId() {
		return entityId;
	}

	public String getType() {
		return type;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @return the state
	 */
	public char getState() {
		return state;
	}

	/**
	 * @return the ids
	 */
	public String getIds() {
		return ids;
	}

	/**
	 * @return the listOrder
	 */
	public String getListOrder() {
		return listOrder;
	}

	/**
	 * @return the init
	 */
	public String getInit() {
		return init;
	}

	/**
	 * @return the showProgress
	 */
	public Boolean getShowProgress() {
		return showProgress;
	}


}
