/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.table;

import cn.easyplatform.entities.transform.TransformerFactory;
import cn.easyplatform.lang.Dumps;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.type.FieldType;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TableTest  {

	@Test
	public void testJava2Xml() {

		List<TableField> fields = new ArrayList<TableField>();
		TableField tf = new TableField();
		tf.setName("id");
		tf.setType(FieldType.VARCHAR);
		tf.setLength(20);
		tf.setDescription("用户id");
		fields.add(tf);

		tf = new TableField();
		tf.setName("name");
		tf.setType(FieldType.VARCHAR);
		tf.setLength(20);
		tf.setDescription("用户名称");
		fields.add(tf);

		tf = new TableField();
		tf.setName("password");
		tf.setType(FieldType.VARCHAR);
		tf.setLength(80);
		tf.setDescription("密码");
		fields.add(tf);

		tf = new TableField();
		tf.setName("sex");
		tf.setType(FieldType.VARCHAR);
		tf.setLength(10);
		tf.setDescription("性别");
		fields.add(tf);

		tf = new TableField();
		tf.setName("phone");
		tf.setType(FieldType.VARCHAR);
		tf.setLength(16);
		tf.setDescription("电话");
		fields.add(tf);

		List<String> key = new ArrayList<String>();
		key.add("id");
		key.add("name");

		List<TableIndex> indexes = new ArrayList<TableIndex>();
		TableIndex ti = new TableIndex();
		ti.setName("index-1");
		ti.setUnique(true);
		//List<String> ff = new ArrayList<String>();
		//ff.add("name");
		//ff.add("phone");
		ti.setFields("name,phone");
		indexes.add(ti);

		List<TableFk> fks = new ArrayList<TableFk>();
		TableFk tfk = new TableFk();
		tfk.setName("customer->customer_order");
		tfk.setAction("on update cascade on delete set null");
		tfk.setReferences("customer_order");
		TableFkField tkff = new TableFkField();
		tkff.setFrom("id");
		tkff.setTo("userid");
		fks.add(tfk);

		TableBean table = new TableBean();
		table.setFields(fields);
		table.setKey(key);
		table.setIndexes(indexes);
		table.setForeignKeys(fks);

		try {
			URL url = getClass().getResource(
					"/cn/easyplatform/entities/beans/table");
			File file = new File(url.getFile() + "/table.xml");
			TransformerFactory.newInstance().transformToXml(table,
					new FileOutputStream(file));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testXml2Java() {
		try {
			URL url = getClass().getResource(
					"/cn/easyplatform/entities/beans/table/table.xml");
			TableBean lb = TransformerFactory.newInstance().transformFromXml(
					TableBean.class, Streams.buff(url.openStream()));
			System.out.println(Dumps.obj(lb));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
