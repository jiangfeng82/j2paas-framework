/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.castor.castor;

import java.math.BigDecimal;

public class String2BigDecimal extends String2Number<BigDecimal> {

	@Override
	protected BigDecimal getPrimitiveDefaultValue() {
		return new BigDecimal(0);
	}

	@Override
	protected BigDecimal valueOf(String str) {
		return new BigDecimal(str);
	}

}
