/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class IdCardUtils {

	/** 中国公民身份证号码最小长度。 */
	private static final int CHINA_ID_MIN_LENGTH = 15;

	/** 中国公民身份证号码最大长度。 */
	private static final int CHINA_ID_MAX_LENGTH = 18;

	/** 省、直辖市代码表 */
	private static final Map<String, String> areaCodes = new HashMap<String, String>();

	/** 每位加权因子 */
	private static final int power[] = { 7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9,
			10, 5, 8, 4, 2 };

	/** 最低年限 */
	private static final int MIN = 1930;

	/** 台湾身份首字母对应数字 */
	private static Map<String, Integer> twFirstCode = new HashMap<String, Integer>();

	/** 香港身份首字母对应数字 */
	private static Map<String, Integer> hkFirstCode = new HashMap<String, Integer>();

	static {
		areaCodes.put("11", "北京");
		areaCodes.put("12", "天津");
		areaCodes.put("13", "河北");
		areaCodes.put("14", "山西");
		areaCodes.put("15", "内蒙古");
		areaCodes.put("21", "辽宁");
		areaCodes.put("22", "吉林");
		areaCodes.put("23", "黑龙江");
		areaCodes.put("31", "上海");
		areaCodes.put("32", "江苏");
		areaCodes.put("33", "浙江");
		areaCodes.put("34", "安徽");
		areaCodes.put("35", "福建");
		areaCodes.put("36", "江西");
		areaCodes.put("37", "山东");
		areaCodes.put("41", "河南");
		areaCodes.put("42", "湖北");
		areaCodes.put("43", "湖南");
		areaCodes.put("44", "广东");
		areaCodes.put("45", "广西");
		areaCodes.put("46", "海南");
		areaCodes.put("50", "重庆");
		areaCodes.put("51", "四川");
		areaCodes.put("52", "贵州");
		areaCodes.put("53", "云南");
		areaCodes.put("54", "西藏");
		areaCodes.put("61", "陕西");
		areaCodes.put("62", "甘肃");
		areaCodes.put("63", "青海");
		areaCodes.put("64", "宁夏");
		areaCodes.put("65", "新疆");
		areaCodes.put("71", "台湾");
		areaCodes.put("81", "香港");
		areaCodes.put("82", "澳门");
		areaCodes.put("91", "国外");
		areaCodes.put("140202", "山西省大同市城区");
		areaCodes.put("140203", "山西省大同市矿区");
		areaCodes.put("140211", "山西省大同市南郊区");
		areaCodes.put("140212", "山西省大同市新荣区");
		areaCodes.put("140221", "山西省大同市阳高县");
		areaCodes.put("140222", "山西省大同市天镇县");
		areaCodes.put("140223", "山西省大同市广灵县");
		areaCodes.put("140224", "山西省大同市灵丘县");
		areaCodes.put("140225", "山西省大同市浑源县");
		areaCodes.put("140226", "山西省大同市左云县");
		areaCodes.put("140227", "山西省大同市大同县");
		areaCodes.put("140300", "山西省阳泉市");
		areaCodes.put("140301", "山西省阳泉市");
		areaCodes.put("140302", "山西省阳泉市城区");
		areaCodes.put("140303", "山西省阳泉市矿区");
		areaCodes.put("140311", "山西省阳泉市郊区");
		areaCodes.put("140321", "山西省阳泉市平定县");
		areaCodes.put("140322", "山西省阳泉市盂县");
		areaCodes.put("140400", "山西省长治市");
		areaCodes.put("140401", "山西省长治市");
		areaCodes.put("140402", "山西省长治市城区");
		areaCodes.put("140411", "山西省长治市郊区");
		areaCodes.put("140421", "山西省长治市长治县");
		areaCodes.put("140423", "山西省长治市襄垣县");
		areaCodes.put("140424", "山西省长治市屯留县");
		areaCodes.put("140425", "山西省长治市平顺县");
		areaCodes.put("140426", "山西省长治市黎城县");
		areaCodes.put("140427", "山西省长治市壶关县");
		areaCodes.put("140428", "山西省长治市长子县");
		areaCodes.put("140429", "山西省长治市武乡县");
		areaCodes.put("140430", "山西省长治市沁县");
		areaCodes.put("140431", "山西省长治市沁源县");
		areaCodes.put("140481", "山西省长治市潞城市");
		areaCodes.put("140500", "山西省晋城市");
		areaCodes.put("140501", "山西省晋城市");
		areaCodes.put("140502", "山西省晋城市城区");
		areaCodes.put("140521", "山西省晋城市沁水县");
		areaCodes.put("140522", "山西省晋城市阳城县");
		areaCodes.put("140524", "山西省晋城市陵川县");
		areaCodes.put("140525", "山西省晋城市泽州县");
		areaCodes.put("140581", "山西省晋城市高平市");
		areaCodes.put("140600", "山西省朔州市");
		areaCodes.put("140601", "山西省朔州市");
		areaCodes.put("140602", "山西省朔州市朔城区");
		areaCodes.put("140603", "山西省朔州市平鲁区");
		areaCodes.put("140621", "山西省朔州市山阴县");
		areaCodes.put("140622", "山西省朔州市应县");
		areaCodes.put("140623", "山西省朔州市右玉县");
		areaCodes.put("140624", "山西省朔州市怀仁县");
		areaCodes.put("140700", "山西省晋中市");
		areaCodes.put("140701", "山西省晋中市");
		areaCodes.put("140702", "山西省晋中市榆次区");
		areaCodes.put("140721", "山西省晋中市榆社县");
		areaCodes.put("140722", "山西省晋中市左权县");
		areaCodes.put("140723", "山西省晋中市和顺县");
		areaCodes.put("140724", "山西省晋中市昔阳县");
		areaCodes.put("140725", "山西省晋中市寿阳县");
		areaCodes.put("140726", "山西省晋中市太谷县");
		areaCodes.put("140727", "山西省晋中市祁县");
		areaCodes.put("140728", "山西省晋中市平遥县");
		areaCodes.put("140729", "山西省晋中市灵石县");
		areaCodes.put("140781", "山西省晋中市介休市");
		areaCodes.put("140800", "山西省运城市");
		areaCodes.put("130531", "河北省邢台市广宗县");
		areaCodes.put("130532", "河北省邢台市平乡县");
		areaCodes.put("130533", "河北省邢台市威县");
		areaCodes.put("130534", "河北省邢台市清河县");
		areaCodes.put("130535", "河北省邢台市临西县");
		areaCodes.put("130581", "河北省邢台市南宫市");
		areaCodes.put("130582", "河北省邢台市沙河市");
		areaCodes.put("130600", "河北省保定市");
		areaCodes.put("130601", "河北省保定市");
		areaCodes.put("130602", "河北省保定市新市区");
		areaCodes.put("130603", "河北省保定市北市区");
		areaCodes.put("130604", "河北省保定市南市区");
		areaCodes.put("130621", "河北省保定市满城县");
		areaCodes.put("130622", "河北省保定市清苑县");
		areaCodes.put("130623", "河北省保定市涞水县");
		areaCodes.put("130624", "河北省保定市阜平县");
		areaCodes.put("130625", "河北省保定市徐水县");
		areaCodes.put("130626", "河北省保定市定兴县");
		areaCodes.put("130627", "河北省保定市唐县");
		areaCodes.put("130628", "河北省保定市高阳县");
		areaCodes.put("130629", "河北省保定市容城县");
		areaCodes.put("130630", "河北省保定市涞源县");
		areaCodes.put("130631", "河北省保定市望都县");
		areaCodes.put("130632", "河北省保定市安新县");
		areaCodes.put("130633", "河北省保定市易县");
		areaCodes.put("130634", "河北省保定市曲阳县");
		areaCodes.put("130635", "河北省保定市蠡县");
		areaCodes.put("130636", "河北省保定市顺平县");
		areaCodes.put("130637", "河北省保定市博野县");
		areaCodes.put("130638", "河北省保定市雄县");
		areaCodes.put("130681", "河北省保定市涿州市");
		areaCodes.put("130682", "河北省保定市定州市");
		areaCodes.put("130683", "河北省保定市安国市");
		areaCodes.put("130684", "河北省保定市高碑店市");
		areaCodes.put("130700", "河北省张家口市");
		areaCodes.put("130701", "河北省张家口市");
		areaCodes.put("130702", "河北省张家口市桥东区");
		areaCodes.put("130703", "河北省张家口市桥西区");
		areaCodes.put("130705", "河北省张家口市宣化区");
		areaCodes.put("130706", "河北省张家口市下花园区");
		areaCodes.put("130721", "河北省张家口市宣化县");
		areaCodes.put("130722", "河北省张家口市张北县");
		areaCodes.put("130723", "河北省张家口市康保县");
		areaCodes.put("130724", "河北省张家口市沽源县");
		areaCodes.put("130725", "河北省张家口市尚义县");
		areaCodes.put("130726", "河北省张家口市蔚县");
		areaCodes.put("130727", "河北省张家口市阳原县");
		areaCodes.put("130728", "河北省张家口市怀安县");
		areaCodes.put("130729", "河北省张家口市万全县");
		areaCodes.put("130730", "河北省张家口市怀来县");
		areaCodes.put("130731", "河北省张家口市涿鹿县");
		areaCodes.put("130732", "河北省张家口市赤城县");
		areaCodes.put("130733", "河北省张家口市崇礼县");
		areaCodes.put("130800", "河北省承德市");
		areaCodes.put("130801", "河北省承德市");
		areaCodes.put("130802", "河北省承德市双桥区");
		areaCodes.put("130803", "河北省承德市双滦区");
		areaCodes.put("130804", "河北省承德市鹰手营子矿区");
		areaCodes.put("130821", "河北省承德市承德县");
		areaCodes.put("130822", "河北省承德市兴隆县");
		areaCodes.put("130823", "河北省承德市平泉县");
		areaCodes.put("130824", "河北省承德市滦平县");
		areaCodes.put("110000", "北京市");
		areaCodes.put("110100", "北京市");
		areaCodes.put("110101", "北京市东城区");
		areaCodes.put("110102", "北京市西城区");
		areaCodes.put("110103", "北京市崇文区");
		areaCodes.put("110104", "北京市宣武区");
		areaCodes.put("110105", "北京市朝阳区");
		areaCodes.put("110106", "北京市丰台区");
		areaCodes.put("110107", "北京市石景山区");
		areaCodes.put("110108", "北京市海淀区");
		areaCodes.put("110109", "北京市门头沟区");
		areaCodes.put("110111", "北京市房山区");
		areaCodes.put("110112", "北京市通州区");
		areaCodes.put("110113", "北京市顺义区");
		areaCodes.put("110114", "北京市昌平区");
		areaCodes.put("110115", "北京市大兴区");
		areaCodes.put("110116", "北京市怀柔区");
		areaCodes.put("110117", "北京市平谷区");
		areaCodes.put("110200", "北京市");
		areaCodes.put("110228", "北京市密云县");
		areaCodes.put("110229", "北京市延庆县");
		areaCodes.put("120000", "天津市");
		areaCodes.put("120100", "天津市");
		areaCodes.put("120101", "天津市和平区");
		areaCodes.put("120102", "天津市河东区");
		areaCodes.put("120103", "天津市河西区");
		areaCodes.put("120104", "天津市南开区");
		areaCodes.put("120105", "天津市河北区");
		areaCodes.put("120106", "天津市红桥区");
		areaCodes.put("120107", "天津市塘沽区");
		areaCodes.put("120108", "天津市汉沽区");
		areaCodes.put("120109", "天津市大港区");
		areaCodes.put("120110", "天津市东丽区");
		areaCodes.put("120111", "天津市西青区");
		areaCodes.put("120112", "天津市津南区");
		areaCodes.put("120113", "天津市北辰区");
		areaCodes.put("120114", "天津市武清区");
		areaCodes.put("120115", "天津市宝坻区");
		areaCodes.put("120200", "天津市");
		areaCodes.put("120221", "天津市宁河县");
		areaCodes.put("120223", "天津市静海县");
		areaCodes.put("120225", "天津市蓟县");
		areaCodes.put("130000", "河北省");
		areaCodes.put("130100", "河北省石家庄市");
		areaCodes.put("130101", "河北省石家庄市");
		areaCodes.put("130102", "河北省石家庄市长安区");
		areaCodes.put("130103", "河北省石家庄市桥东区");
		areaCodes.put("130104", "河北省石家庄市桥西区");
		areaCodes.put("130105", "河北省石家庄市新华区");
		areaCodes.put("130107", "河北省石家庄市井陉矿区");
		areaCodes.put("130108", "河北省石家庄市裕华区");
		areaCodes.put("130121", "河北省石家庄市井陉县");
		areaCodes.put("130123", "河北省石家庄市正定县");
		areaCodes.put("130124", "河北省石家庄市栾城县");
		areaCodes.put("130125", "河北省石家庄市行唐县");
		areaCodes.put("130126", "河北省石家庄市灵寿县");
		areaCodes.put("130127", "河北省石家庄市高邑县");
		areaCodes.put("130128", "河北省石家庄市深泽县");
		areaCodes.put("130129", "河北省石家庄市赞皇县");
		areaCodes.put("130130", "河北省石家庄市无极县");
		areaCodes.put("130131", "河北省石家庄市平山县");
		areaCodes.put("130132", "河北省石家庄市元氏县");
		areaCodes.put("130133", "河北省石家庄市赵县");
		areaCodes.put("130181", "河北省石家庄市辛集市");
		areaCodes.put("130182", "河北省石家庄市藁城市");
		areaCodes.put("130183", "河北省石家庄市晋州市");
		areaCodes.put("130825", "河北省承德市隆化县");
		areaCodes.put("130826", "河北省承德市丰宁满族自治县");
		areaCodes.put("130827", "河北省承德市宽城满族自治县");
		areaCodes.put("130828", "河北省承德市围场满族蒙古族自治县");
		areaCodes.put("130900", "河北省沧州市");
		areaCodes.put("130901", "河北省沧州市");
		areaCodes.put("130902", "河北省沧州市新华区");
		areaCodes.put("130903", "河北省沧州市运河区");
		areaCodes.put("130921", "河北省沧州市沧县");
		areaCodes.put("130922", "河北省沧州市青县");
		areaCodes.put("130923", "河北省沧州市东光县");
		areaCodes.put("130924", "河北省沧州市海兴县");
		areaCodes.put("130925", "河北省沧州市盐山县");
		areaCodes.put("130926", "河北省沧州市肃宁县");
		areaCodes.put("130927", "河北省沧州市南皮县");
		areaCodes.put("130928", "河北省沧州市吴桥县");
		areaCodes.put("130929", "河北省沧州市献县");
		areaCodes.put("130930", "河北省沧州市孟村回族自治县");
		areaCodes.put("130981", "河北省沧州市泊头市");
		areaCodes.put("130982", "河北省沧州市任丘市");
		areaCodes.put("130983", "河北省沧州市黄骅市");
		areaCodes.put("130984", "河北省沧州市河间市");
		areaCodes.put("131000", "河北省廊坊市");
		areaCodes.put("131001", "河北省廊坊市");
		areaCodes.put("131002", "河北省廊坊市安次区");
		areaCodes.put("131003", "河北省廊坊市广阳区");
		areaCodes.put("131022", "河北省廊坊市固安县");
		areaCodes.put("131023", "河北省廊坊市永清县");
		areaCodes.put("131024", "河北省廊坊市香河县");
		areaCodes.put("131025", "河北省廊坊市大城县");
		areaCodes.put("131026", "河北省廊坊市文安县");
		areaCodes.put("131028", "河北省廊坊市大厂回族自治县");
		areaCodes.put("131081", "河北省廊坊市霸州市");
		areaCodes.put("131082", "河北省廊坊市三河市");
		areaCodes.put("131100", "河北省衡水市");
		areaCodes.put("131101", "河北省衡水市");
		areaCodes.put("131102", "河北省衡水市桃城区");
		areaCodes.put("131121", "河北省衡水市枣强县");
		areaCodes.put("131122", "河北省衡水市武邑县");
		areaCodes.put("131123", "河北省衡水市武强县");
		areaCodes.put("131124", "河北省衡水市饶阳县");
		areaCodes.put("131125", "河北省衡水市安平县");
		areaCodes.put("131126", "河北省衡水市故城县");
		areaCodes.put("131127", "河北省衡水市景县");
		areaCodes.put("131128", "河北省衡水市阜城县");
		areaCodes.put("131181", "河北省衡水市冀州市");
		areaCodes.put("131182", "河北省衡水市深州市");
		areaCodes.put("140000", "山西省");
		areaCodes.put("140100", "山西省太原市");
		areaCodes.put("140101", "山西省太原市");
		areaCodes.put("140105", "山西省太原市小店区");
		areaCodes.put("140106", "山西省太原市迎泽区");
		areaCodes.put("140107", "山西省太原市杏花岭区");
		areaCodes.put("140108", "山西省太原市尖草坪区");
		areaCodes.put("140109", "山西省太原市万柏林区");
		areaCodes.put("140110", "山西省太原市晋源区");
		areaCodes.put("140121", "山西省太原市清徐县");
		areaCodes.put("140122", "山西省太原市阳曲县");
		areaCodes.put("140123", "山西省太原市娄烦县");
		areaCodes.put("140181", "山西省太原市古交市");
		areaCodes.put("140200", "山西省大同市");
		areaCodes.put("140201", "山西省大同市");
		areaCodes.put("130184", "河北省石家庄市新乐市");
		areaCodes.put("130185", "河北省石家庄市鹿泉市");
		areaCodes.put("130200", "河北省唐山市");
		areaCodes.put("130201", "河北省唐山市");
		areaCodes.put("130202", "河北省唐山市路南区");
		areaCodes.put("130203", "河北省唐山市路北区");
		areaCodes.put("130204", "河北省唐山市古冶区");
		areaCodes.put("130205", "河北省唐山市开平区");
		areaCodes.put("130207", "河北省唐山市丰南区");
		areaCodes.put("130208", "河北省唐山市丰润区");
		areaCodes.put("130223", "河北省唐山市滦县");
		areaCodes.put("130224", "河北省唐山市滦南县");
		areaCodes.put("130225", "河北省唐山市乐亭县");
		areaCodes.put("130227", "河北省唐山市迁西县");
		areaCodes.put("130229", "河北省唐山市玉田县");
		areaCodes.put("130230", "河北省唐山市唐海县");
		areaCodes.put("130281", "河北省唐山市遵化市");
		areaCodes.put("130283", "河北省唐山市迁安市");
		areaCodes.put("130300", "河北省秦皇岛市");
		areaCodes.put("130301", "河北省秦皇岛市");
		areaCodes.put("130302", "河北省秦皇岛市海港区");
		areaCodes.put("130303", "河北省秦皇岛市山海关区");
		areaCodes.put("130304", "河北省秦皇岛市北戴河区");
		areaCodes.put("130321", "河北省秦皇岛市青龙满族自治县");
		areaCodes.put("130322", "河北省秦皇岛市昌黎县");
		areaCodes.put("130323", "河北省秦皇岛市抚宁县");
		areaCodes.put("130324", "河北省秦皇岛市卢龙县");
		areaCodes.put("130400", "河北省邯郸市");
		areaCodes.put("130401", "河北省邯郸市");
		areaCodes.put("130402", "河北省邯郸市邯山区");
		areaCodes.put("130403", "河北省邯郸市丛台区");
		areaCodes.put("130404", "河北省邯郸市复兴区");
		areaCodes.put("130406", "河北省邯郸市峰峰矿区");
		areaCodes.put("130421", "河北省邯郸市邯郸县");
		areaCodes.put("130423", "河北省邯郸市临漳县");
		areaCodes.put("130424", "河北省邯郸市成安县");
		areaCodes.put("130425", "河北省邯郸市大名县");
		areaCodes.put("130426", "河北省邯郸市涉县");
		areaCodes.put("130427", "河北省邯郸市磁县");
		areaCodes.put("130428", "河北省邯郸市肥乡县");
		areaCodes.put("130429", "河北省邯郸市永年县");
		areaCodes.put("130430", "河北省邯郸市邱县");
		areaCodes.put("130431", "河北省邯郸市鸡泽县");
		areaCodes.put("130432", "河北省邯郸市广平县");
		areaCodes.put("130433", "河北省邯郸市馆陶县");
		areaCodes.put("130434", "河北省邯郸市魏县");
		areaCodes.put("130435", "河北省邯郸市曲周县");
		areaCodes.put("130481", "河北省邯郸市武安市");
		areaCodes.put("130500", "河北省邢台市");
		areaCodes.put("130501", "河北省邢台市");
		areaCodes.put("130502", "河北省邢台市桥东区");
		areaCodes.put("130503", "河北省邢台市桥西区");
		areaCodes.put("130521", "河北省邢台市邢台县");
		areaCodes.put("130522", "河北省邢台市临城县");
		areaCodes.put("130523", "河北省邢台市内丘县");
		areaCodes.put("130524", "河北省邢台市柏乡县");
		areaCodes.put("130525", "河北省邢台市隆尧县");
		areaCodes.put("130526", "河北省邢台市任县");
		areaCodes.put("130527", "河北省邢台市南和县");
		areaCodes.put("130528", "河北省邢台市宁晋县");
		areaCodes.put("130529", "河北省邢台市巨鹿县");
		areaCodes.put("130530", "河北省邢台市新河县");
		areaCodes.put("230183", "黑龙江省哈尔滨市尚志市");
		areaCodes.put("230184", "黑龙江省哈尔滨市五常市");
		areaCodes.put("230200", "黑龙江省齐齐哈尔市");
		areaCodes.put("230201", "黑龙江省齐齐哈尔市");
		areaCodes.put("230202", "黑龙江省齐齐哈尔市龙沙区");
		areaCodes.put("230203", "黑龙江省齐齐哈尔市建华区");
		areaCodes.put("230204", "黑龙江省齐齐哈尔市铁锋区");
		areaCodes.put("230205", "黑龙江省齐齐哈尔市昂昂溪区");
		areaCodes.put("230206", "黑龙江省齐齐哈尔市富拉尔基区");
		areaCodes.put("230207", "黑龙江省齐齐哈尔市碾子山区");
		areaCodes.put("230208", "黑龙江省齐齐哈尔市梅里斯达斡尔族区");
		areaCodes.put("230221", "黑龙江省齐齐哈尔市龙江县");
		areaCodes.put("230223", "黑龙江省齐齐哈尔市依安县");
		areaCodes.put("230224", "黑龙江省齐齐哈尔市泰来县");
		areaCodes.put("230225", "黑龙江省齐齐哈尔市甘南县");
		areaCodes.put("230227", "黑龙江省齐齐哈尔市富裕县");
		areaCodes.put("230229", "黑龙江省齐齐哈尔市克山县");
		areaCodes.put("230230", "黑龙江省齐齐哈尔市克东县");
		areaCodes.put("230231", "黑龙江省齐齐哈尔市拜泉县");
		areaCodes.put("230281", "黑龙江省齐齐哈尔市讷河市");
		areaCodes.put("230300", "黑龙江省鸡西市");
		areaCodes.put("230301", "黑龙江省鸡西市");
		areaCodes.put("230302", "黑龙江省鸡西市鸡冠区");
		areaCodes.put("230303", "黑龙江省鸡西市恒山区");
		areaCodes.put("230304", "黑龙江省鸡西市滴道区");
		areaCodes.put("230305", "黑龙江省鸡西市梨树区");
		areaCodes.put("230306", "黑龙江省鸡西市城子河区");
		areaCodes.put("230307", "黑龙江省鸡西市麻山区");
		areaCodes.put("230321", "黑龙江省鸡西市鸡东县");
		areaCodes.put("230381", "黑龙江省鸡西市虎林市");
		areaCodes.put("230382", "黑龙江省鸡西市密山市");
		areaCodes.put("230400", "黑龙江省鹤岗市");
		areaCodes.put("230401", "黑龙江省鹤岗市");
		areaCodes.put("230402", "黑龙江省鹤岗市向阳区");
		areaCodes.put("230403", "黑龙江省鹤岗市工农区");
		areaCodes.put("230404", "黑龙江省鹤岗市南山区");
		areaCodes.put("230405", "黑龙江省鹤岗市兴安区");
		areaCodes.put("230406", "黑龙江省鹤岗市东山区");
		areaCodes.put("230407", "黑龙江省鹤岗市兴山区");
		areaCodes.put("230421", "黑龙江省鹤岗市萝北县");
		areaCodes.put("230422", "黑龙江省鹤岗市绥滨县");
		areaCodes.put("230500", "黑龙江省双鸭山市");
		areaCodes.put("230501", "黑龙江省双鸭山市");
		areaCodes.put("230502", "黑龙江省双鸭山市尖山区");
		areaCodes.put("230503", "黑龙江省双鸭山市岭东区");
		areaCodes.put("230505", "黑龙江省双鸭山市四方台区");
		areaCodes.put("230506", "黑龙江省双鸭山市宝山区");
		areaCodes.put("230521", "黑龙江省双鸭山市集贤县");
		areaCodes.put("230522", "黑龙江省双鸭山市友谊县");
		areaCodes.put("230523", "黑龙江省双鸭山市宝清县");
		areaCodes.put("230524", "黑龙江省双鸭山市饶河县");
		areaCodes.put("230600", "黑龙江省大庆市");
		areaCodes.put("230601", "黑龙江省大庆市");
		areaCodes.put("230602", "黑龙江省大庆市萨尔图区");
		areaCodes.put("150927", "内蒙古自治区乌兰察布市察哈尔右翼中旗");
		areaCodes.put("150928", "内蒙古自治区乌兰察布市察哈尔右翼后旗");
		areaCodes.put("150929", "内蒙古自治区乌兰察布市四子王旗");
		areaCodes.put("150981", "内蒙古自治区乌兰察布市丰镇市");
		areaCodes.put("152200", "内蒙古自治区兴安盟");
		areaCodes.put("152201", "内蒙古自治区兴安盟乌兰浩特市");
		areaCodes.put("152202", "内蒙古自治区兴安盟阿尔山市");
		areaCodes.put("152221", "内蒙古自治区兴安盟科尔沁右翼前旗");
		areaCodes.put("152222", "内蒙古自治区兴安盟科尔沁右翼中旗");
		areaCodes.put("152223", "内蒙古自治区兴安盟扎赉特旗");
		areaCodes.put("152224", "内蒙古自治区兴安盟突泉县");
		areaCodes.put("152500", "内蒙古自治区锡林郭勒盟");
		areaCodes.put("152501", "内蒙古自治区锡林郭勒盟二连浩特市");
		areaCodes.put("152502", "内蒙古自治区锡林郭勒盟锡林浩特市");
		areaCodes.put("152522", "内蒙古自治区锡林郭勒盟阿巴嘎旗");
		areaCodes.put("211221", "辽宁省铁岭市铁岭县");
		areaCodes.put("211223", "辽宁省铁岭市西丰县");
		areaCodes.put("211224", "辽宁省铁岭市昌图县");
		areaCodes.put("211281", "辽宁省铁岭市调兵山市");
		areaCodes.put("211282", "辽宁省铁岭市开原市");
		areaCodes.put("211300", "辽宁省朝阳市");
		areaCodes.put("211301", "辽宁省朝阳市");
		areaCodes.put("211302", "辽宁省朝阳市双塔区");
		areaCodes.put("211303", "辽宁省朝阳市龙城区");
		areaCodes.put("211321", "辽宁省朝阳市朝阳县");
		areaCodes.put("211322", "辽宁省朝阳市建平县");
		areaCodes.put("211324", "辽宁省朝阳市喀喇沁左翼蒙古族自治县");
		areaCodes.put("211381", "辽宁省朝阳市北票市");
		areaCodes.put("211382", "辽宁省朝阳市凌源市");
		areaCodes.put("211400", "辽宁省葫芦岛市");
		areaCodes.put("211401", "辽宁省葫芦岛市");
		areaCodes.put("211402", "辽宁省葫芦岛市连山区");
		areaCodes.put("211403", "辽宁省葫芦岛市龙港区");
		areaCodes.put("211404", "辽宁省葫芦岛市南票区");
		areaCodes.put("211421", "辽宁省葫芦岛市绥中县");
		areaCodes.put("211422", "辽宁省葫芦岛市建昌县");
		areaCodes.put("211481", "辽宁省葫芦岛市兴城市");
		areaCodes.put("220000", "吉林省");
		areaCodes.put("220100", "吉林省长春市");
		areaCodes.put("220101", "吉林省长春市");
		areaCodes.put("220102", "吉林省长春市南关区");
		areaCodes.put("220103", "吉林省长春市宽城区");
		areaCodes.put("220104", "吉林省长春市朝阳区");
		areaCodes.put("220105", "吉林省长春市二道区");
		areaCodes.put("220106", "吉林省长春市绿园区");
		areaCodes.put("220112", "吉林省长春市双阳区");
		areaCodes.put("220122", "吉林省长春市农安县");
		areaCodes.put("220181", "吉林省长春市九台市");
		areaCodes.put("220182", "吉林省长春市榆树市");
		areaCodes.put("220183", "吉林省长春市德惠市");
		areaCodes.put("220200", "吉林省吉林市");
		areaCodes.put("220201", "吉林省吉林市");
		areaCodes.put("220202", "吉林省吉林市昌邑区");
		areaCodes.put("220203", "吉林省吉林市龙潭区");
		areaCodes.put("220204", "吉林省吉林市船营区");
		areaCodes.put("220211", "吉林省吉林市丰满区");
		areaCodes.put("220221", "吉林省吉林市永吉县");
		areaCodes.put("220281", "吉林省吉林市蛟河市");
		areaCodes.put("220282", "吉林省吉林市桦甸市");
		areaCodes.put("220283", "吉林省吉林市舒兰市");
		areaCodes.put("220284", "吉林省吉林市磐石市");
		areaCodes.put("220300", "吉林省四平市");
		areaCodes.put("220301", "吉林省四平市");
		areaCodes.put("220302", "吉林省四平市铁西区");
		areaCodes.put("220303", "吉林省四平市铁东区");
		areaCodes.put("220322", "吉林省四平市梨树县");
		areaCodes.put("220323", "吉林省四平市伊通满族自治县");
		areaCodes.put("220381", "吉林省四平市公主岭市");
		areaCodes.put("220382", "吉林省四平市双辽市");
		areaCodes.put("220400", "吉林省辽源市");
		areaCodes.put("220401", "吉林省辽源市");
		areaCodes.put("220402", "吉林省辽源市龙山区");
		areaCodes.put("220403", "吉林省辽源市西安区");
		areaCodes.put("220421", "吉林省辽源市东丰县");
		areaCodes.put("220422", "吉林省辽源市东辽县");
		areaCodes.put("220500", "吉林省通化市");
		areaCodes.put("210500", "辽宁省本溪市");
		areaCodes.put("150000", "内蒙古自治区");
		areaCodes.put("150100", "内蒙古自治区呼和浩特市");
		areaCodes.put("150101", "内蒙古自治区呼和浩特市");
		areaCodes.put("150102", "内蒙古自治区呼和浩特市新城区");
		areaCodes.put("150103", "内蒙古自治区呼和浩特市回民区");
		areaCodes.put("150104", "内蒙古自治区呼和浩特市玉泉区");
		areaCodes.put("150105", "内蒙古自治区呼和浩特市赛罕区");
		areaCodes.put("150121", "内蒙古自治区呼和浩特市土默特左旗");
		areaCodes.put("150122", "内蒙古自治区呼和浩特市托克托县");
		areaCodes.put("150123", "内蒙古自治区呼和浩特市和林格尔县");
		areaCodes.put("150124", "内蒙古自治区呼和浩特市清水河县");
		areaCodes.put("150125", "内蒙古自治区呼和浩特市武川县");
		areaCodes.put("150200", "内蒙古自治区包头市");
		areaCodes.put("150201", "内蒙古自治区包头市");
		areaCodes.put("150202", "内蒙古自治区包头市东河区");
		areaCodes.put("150203", "内蒙古自治区包头市昆都仑区");
		areaCodes.put("150204", "内蒙古自治区包头市青山区");
		areaCodes.put("150205", "内蒙古自治区包头市石拐区");
		areaCodes.put("150206", "内蒙古自治区包头市白云矿区");
		areaCodes.put("150207", "内蒙古自治区包头市九原区");
		areaCodes.put("150221", "内蒙古自治区包头市土默特右旗");
		areaCodes.put("150222", "内蒙古自治区包头市固阳县");
		areaCodes.put("150223", "内蒙古自治区包头市达尔罕茂明安联合旗");
		areaCodes.put("150300", "内蒙古自治区乌海市");
		areaCodes.put("150301", "内蒙古自治区乌海市");
		areaCodes.put("150302", "内蒙古自治区乌海市海勃湾区");
		areaCodes.put("150303", "内蒙古自治区乌海市海南区");
		areaCodes.put("150304", "内蒙古自治区乌海市乌达区");
		areaCodes.put("150400", "内蒙古自治区赤峰市");
		areaCodes.put("150401", "内蒙古自治区赤峰市");
		areaCodes.put("150402", "内蒙古自治区赤峰市红山区");
		areaCodes.put("150403", "内蒙古自治区赤峰市元宝山区");
		areaCodes.put("150404", "内蒙古自治区赤峰市松山区");
		areaCodes.put("150421", "内蒙古自治区赤峰市阿鲁科尔沁旗");
		areaCodes.put("150422", "内蒙古自治区赤峰市巴林左旗");
		areaCodes.put("150423", "内蒙古自治区赤峰市巴林右旗");
		areaCodes.put("150424", "内蒙古自治区赤峰市林西县");
		areaCodes.put("150425", "内蒙古自治区赤峰市克什克腾旗");
		areaCodes.put("150426", "内蒙古自治区赤峰市翁牛特旗");
		areaCodes.put("150428", "内蒙古自治区赤峰市喀喇沁旗");
		areaCodes.put("150429", "内蒙古自治区赤峰市宁城县");
		areaCodes.put("150430", "内蒙古自治区赤峰市敖汉旗");
		areaCodes.put("150500", "内蒙古自治区通辽市");
		areaCodes.put("150501", "内蒙古自治区通辽市");
		areaCodes.put("150502", "内蒙古自治区通辽市科尔沁区");
		areaCodes.put("150521", "内蒙古自治区通辽市科尔沁左翼中旗");
		areaCodes.put("150522", "内蒙古自治区通辽市科尔沁左翼后旗");
		areaCodes.put("150523", "内蒙古自治区通辽市开鲁县");
		areaCodes.put("150524", "内蒙古自治区通辽市库伦旗");
		areaCodes.put("150525", "内蒙古自治区通辽市奈曼旗");
		areaCodes.put("150526", "内蒙古自治区通辽市扎鲁特旗");
		areaCodes.put("150581", "内蒙古自治区通辽市霍林郭勒市");
		areaCodes.put("150600", "内蒙古自治区鄂尔多斯市");
		areaCodes.put("150602", "内蒙古自治区鄂尔多斯市东胜区");
		areaCodes.put("150621", "内蒙古自治区鄂尔多斯市达拉特旗");
		areaCodes.put("330122", "浙江省杭州市桐庐县");
		areaCodes.put("140801", "山西省运城市");
		areaCodes.put("140802", "山西省运城市盐湖区");
		areaCodes.put("140821", "山西省运城市临猗县");
		areaCodes.put("140822", "山西省运城市万荣县");
		areaCodes.put("140823", "山西省运城市闻喜县");
		areaCodes.put("140824", "山西省运城市稷山县");
		areaCodes.put("140825", "山西省运城市新绛县");
		areaCodes.put("140826", "山西省运城市绛县");
		areaCodes.put("140827", "山西省运城市垣曲县");
		areaCodes.put("140828", "山西省运城市夏县");
		areaCodes.put("140829", "山西省运城市平陆县");
		areaCodes.put("140830", "山西省运城市芮城县");
		areaCodes.put("140881", "山西省运城市永济市");
		areaCodes.put("140882", "山西省运城市河津市");
		areaCodes.put("140900", "山西省忻州市");
		areaCodes.put("140901", "山西省忻州市");
		areaCodes.put("140902", "山西省忻州市忻府区");
		areaCodes.put("140921", "山西省忻州市定襄县");
		areaCodes.put("140922", "山西省忻州市五台县");
		areaCodes.put("140923", "山西省忻州市代县");
		areaCodes.put("140924", "山西省忻州市繁峙县");
		areaCodes.put("140925", "山西省忻州市宁武县");
		areaCodes.put("140926", "山西省忻州市静乐县");
		areaCodes.put("140927", "山西省忻州市神池县");
		areaCodes.put("140928", "山西省忻州市五寨县");
		areaCodes.put("140929", "山西省忻州市岢岚县");
		areaCodes.put("140930", "山西省忻州市河曲县");
		areaCodes.put("140931", "山西省忻州市保德县");
		areaCodes.put("140932", "山西省忻州市偏关县");
		areaCodes.put("140981", "山西省忻州市原平市");
		areaCodes.put("141000", "山西省临汾市");
		areaCodes.put("141001", "山西省临汾市");
		areaCodes.put("141002", "山西省临汾市尧都区");
		areaCodes.put("141021", "山西省临汾市曲沃县");
		areaCodes.put("141022", "山西省临汾市翼城县");
		areaCodes.put("141023", "山西省临汾市襄汾县");
		areaCodes.put("141024", "山西省临汾市洪洞县");
		areaCodes.put("141025", "山西省临汾市古县");
		areaCodes.put("141026", "山西省临汾市安泽县");
		areaCodes.put("141027", "山西省临汾市浮山县");
		areaCodes.put("141028", "山西省临汾市吉县");
		areaCodes.put("141029", "山西省临汾市乡宁县");
		areaCodes.put("141030", "山西省临汾市大宁县");
		areaCodes.put("141031", "山西省临汾市隰县");
		areaCodes.put("141032", "山西省临汾市永和县");
		areaCodes.put("141033", "山西省临汾市蒲县");
		areaCodes.put("141034", "山西省临汾市汾西县");
		areaCodes.put("141081", "山西省临汾市侯马市");
		areaCodes.put("141082", "山西省临汾市霍州市");
		areaCodes.put("141100", "山西省吕梁市");
		areaCodes.put("141101", "山西省吕梁市");
		areaCodes.put("141102", "山西省吕梁市离石区");
		areaCodes.put("141121", "山西省吕梁市文水县");
		areaCodes.put("141122", "山西省吕梁市交城县");
		areaCodes.put("141123", "山西省吕梁市兴县");
		areaCodes.put("141124", "山西省吕梁市临县");
		areaCodes.put("141125", "山西省吕梁市柳林县");
		areaCodes.put("141126", "山西省吕梁市石楼县");
		areaCodes.put("141127", "山西省吕梁市岚县");
		areaCodes.put("141128", "山西省吕梁市方山县");
		areaCodes.put("141129", "山西省吕梁市中阳县");
		areaCodes.put("141130", "山西省吕梁市交口县");
		areaCodes.put("141181", "山西省吕梁市孝义市");
		areaCodes.put("210501", "辽宁省本溪市");
		areaCodes.put("210502", "辽宁省本溪市平山区");
		areaCodes.put("210503", "辽宁省本溪市溪湖区");
		areaCodes.put("210504", "辽宁省本溪市明山区");
		areaCodes.put("210505", "辽宁省本溪市南芬区");
		areaCodes.put("210521", "辽宁省本溪市本溪满族自治县");
		areaCodes.put("210522", "辽宁省本溪市桓仁满族自治县");
		areaCodes.put("210600", "辽宁省丹东市");
		areaCodes.put("210601", "辽宁省丹东市");
		areaCodes.put("210602", "辽宁省丹东市元宝区");
		areaCodes.put("210603", "辽宁省丹东市振兴区");
		areaCodes.put("210604", "辽宁省丹东市振安区");
		areaCodes.put("210624", "辽宁省丹东市宽甸满族自治县");
		areaCodes.put("210681", "辽宁省丹东市东港市");
		areaCodes.put("210682", "辽宁省丹东市凤城市");
		areaCodes.put("210700", "辽宁省锦州市");
		areaCodes.put("210701", "辽宁省锦州市");
		areaCodes.put("210702", "辽宁省锦州市古塔区");
		areaCodes.put("210703", "辽宁省锦州市凌河区");
		areaCodes.put("210711", "辽宁省锦州市太和区");
		areaCodes.put("210726", "辽宁省锦州市黑山县");
		areaCodes.put("210727", "辽宁省锦州市义县");
		areaCodes.put("210781", "辽宁省锦州市凌海市");
		areaCodes.put("210782", "辽宁省锦州市北宁市");
		areaCodes.put("210800", "辽宁省营口市");
		areaCodes.put("210801", "辽宁省营口市");
		areaCodes.put("210802", "辽宁省营口市站前区");
		areaCodes.put("210803", "辽宁省营口市西市区");
		areaCodes.put("210804", "辽宁省营口市鲅鱼圈区");
		areaCodes.put("210811", "辽宁省营口市老边区");
		areaCodes.put("210881", "辽宁省营口市盖州市");
		areaCodes.put("210882", "辽宁省营口市大石桥市");
		areaCodes.put("210900", "辽宁省阜新市");
		areaCodes.put("210901", "辽宁省阜新市");
		areaCodes.put("210902", "辽宁省阜新市海州区");
		areaCodes.put("210903", "辽宁省阜新市新邱区");
		areaCodes.put("210904", "辽宁省阜新市太平区");
		areaCodes.put("210905", "辽宁省阜新市清河门区");
		areaCodes.put("210911", "辽宁省阜新市细河区");
		areaCodes.put("210921", "辽宁省阜新市阜新蒙古族自治县");
		areaCodes.put("210922", "辽宁省阜新市彰武县");
		areaCodes.put("211000", "辽宁省辽阳市");
		areaCodes.put("211001", "辽宁省辽阳市");
		areaCodes.put("211002", "辽宁省辽阳市白塔区");
		areaCodes.put("211003", "辽宁省辽阳市文圣区");
		areaCodes.put("211004", "辽宁省辽阳市宏伟区");
		areaCodes.put("211005", "辽宁省辽阳市弓长岭区");
		areaCodes.put("211011", "辽宁省辽阳市太子河区");
		areaCodes.put("211021", "辽宁省辽阳市辽阳县");
		areaCodes.put("211081", "辽宁省辽阳市灯塔市");
		areaCodes.put("211100", "辽宁省盘锦市");
		areaCodes.put("211101", "辽宁省盘锦市");
		areaCodes.put("211102", "辽宁省盘锦市双台子区");
		areaCodes.put("211103", "辽宁省盘锦市兴隆台区");
		areaCodes.put("211121", "辽宁省盘锦市大洼县");
		areaCodes.put("211122", "辽宁省盘锦市盘山县");
		areaCodes.put("211200", "辽宁省铁岭市");
		areaCodes.put("211201", "辽宁省铁岭市");
		areaCodes.put("150601", "内蒙古自治区鄂尔多斯市");
		areaCodes.put("321300", "江苏省宿迁市");
		areaCodes.put("321301", "江苏省宿迁市");
		areaCodes.put("321302", "江苏省宿迁市宿城区");
		areaCodes.put("321311", "江苏省宿迁市宿豫区");
		areaCodes.put("321322", "江苏省宿迁市沭阳县");
		areaCodes.put("321323", "江苏省宿迁市泗阳县");
		areaCodes.put("321324", "江苏省宿迁市泗洪县");
		areaCodes.put("330000", "浙江省");
		areaCodes.put("330100", "浙江省杭州市");
		areaCodes.put("330101", "浙江省杭州市");
		areaCodes.put("330102", "浙江省杭州市上城区");
		areaCodes.put("330103", "浙江省杭州市下城区");
		areaCodes.put("330104", "浙江省杭州市江干区");
		areaCodes.put("330105", "浙江省杭州市拱墅区");
		areaCodes.put("330106", "浙江省杭州市西湖区");
		areaCodes.put("330108", "浙江省杭州市滨江区");
		areaCodes.put("330109", "浙江省杭州市萧山区");
		areaCodes.put("330110", "浙江省杭州市余杭区");
		areaCodes.put("340303", "安徽省蚌埠市蚌山区");
		areaCodes.put("330782", "浙江省金华市义乌市");
		areaCodes.put("330783", "浙江省金华市东阳市");
		areaCodes.put("330784", "浙江省金华市永康市");
		areaCodes.put("330800", "浙江省衢州市");
		areaCodes.put("330801", "浙江省衢州市");
		areaCodes.put("330802", "浙江省衢州市柯城区");
		areaCodes.put("330803", "浙江省衢州市衢江区");
		areaCodes.put("330822", "浙江省衢州市常山县");
		areaCodes.put("330824", "浙江省衢州市开化县");
		areaCodes.put("330825", "浙江省衢州市龙游县");
		areaCodes.put("330881", "浙江省衢州市江山市");
		areaCodes.put("330900", "浙江省舟山市");
		areaCodes.put("330901", "浙江省舟山市");
		areaCodes.put("330902", "浙江省舟山市定海区");
		areaCodes.put("330903", "浙江省舟山市普陀区");
		areaCodes.put("330921", "浙江省舟山市岱山县");
		areaCodes.put("330922", "浙江省舟山市嵊泗县");
		areaCodes.put("331000", "浙江省台州市");
		areaCodes.put("331001", "浙江省台州市");
		areaCodes.put("331002", "浙江省台州市椒江区");
		areaCodes.put("331003", "浙江省台州市黄岩区");
		areaCodes.put("331004", "浙江省台州市路桥区");
		areaCodes.put("331021", "浙江省台州市玉环县");
		areaCodes.put("331022", "浙江省台州市三门县");
		areaCodes.put("331023", "浙江省台州市天台县");
		areaCodes.put("331024", "浙江省台州市仙居县");
		areaCodes.put("331081", "浙江省台州市温岭市");
		areaCodes.put("331082", "浙江省台州市临海市");
		areaCodes.put("331100", "浙江省丽水市");
		areaCodes.put("331101", "浙江省丽水市");
		areaCodes.put("331102", "浙江省丽水市莲都区");
		areaCodes.put("331121", "浙江省丽水市青田县");
		areaCodes.put("331122", "浙江省丽水市缙云县");
		areaCodes.put("331123", "浙江省丽水市遂昌县");
		areaCodes.put("331124", "浙江省丽水市松阳县");
		areaCodes.put("331125", "浙江省丽水市云和县");
		areaCodes.put("331126", "浙江省丽水市庆元县");
		areaCodes.put("331127", "浙江省丽水市景宁畲族自治县");
		areaCodes.put("331181", "浙江省丽水市龙泉市");
		areaCodes.put("340000", "安徽省");
		areaCodes.put("340100", "安徽省合肥市");
		areaCodes.put("340101", "安徽省合肥市");
		areaCodes.put("340102", "安徽省合肥市瑶海区");
		areaCodes.put("340103", "安徽省合肥市庐阳区");
		areaCodes.put("340104", "安徽省合肥市蜀山区");
		areaCodes.put("340111", "安徽省合肥市包河区");
		areaCodes.put("340121", "安徽省合肥市长丰县");
		areaCodes.put("340122", "安徽省合肥市肥东县");
		areaCodes.put("340123", "安徽省合肥市肥西县");
		areaCodes.put("340200", "安徽省芜湖市");
		areaCodes.put("340201", "安徽省芜湖市");
		areaCodes.put("340202", "安徽省芜湖市镜湖区");
		areaCodes.put("340203", "安徽省芜湖市马塘区");
		areaCodes.put("340204", "安徽省芜湖市新芜区");
		areaCodes.put("340207", "安徽省芜湖市鸠江区");
		areaCodes.put("340221", "安徽省芜湖市芜湖县");
		areaCodes.put("340222", "安徽省芜湖市繁昌县");
		areaCodes.put("340223", "安徽省芜湖市南陵县");
		areaCodes.put("340300", "安徽省蚌埠市");
		areaCodes.put("340301", "安徽省蚌埠市");
		areaCodes.put("340302", "安徽省蚌埠市龙子湖区");
		areaCodes.put("320802", "江苏省淮安市清河区");
		areaCodes.put("340304", "安徽省蚌埠市禹会区");
		areaCodes.put("340311", "安徽省蚌埠市淮上区");
		areaCodes.put("340321", "安徽省蚌埠市怀远县");
		areaCodes.put("340322", "安徽省蚌埠市五河县");
		areaCodes.put("340323", "安徽省蚌埠市固镇县");
		areaCodes.put("340400", "安徽省淮南市");
		areaCodes.put("340401", "安徽省淮南市");
		areaCodes.put("340402", "安徽省淮南市大通区");
		areaCodes.put("340403", "安徽省淮南市田家庵区");
		areaCodes.put("340404", "安徽省淮南市谢家集区");
		areaCodes.put("340405", "安徽省淮南市八公山区");
		areaCodes.put("340406", "安徽省淮南市潘集区");
		areaCodes.put("340421", "安徽省淮南市凤台县");
		areaCodes.put("340500", "安徽省马鞍山市");
		areaCodes.put("340501", "安徽省马鞍山市");
		areaCodes.put("340502", "安徽省马鞍山市金家庄区");
		areaCodes.put("340503", "安徽省马鞍山市花山区");
		areaCodes.put("340504", "安徽省马鞍山市雨山区");
		areaCodes.put("340521", "安徽省马鞍山市当涂县");
		areaCodes.put("340600", "安徽省淮北市");
		areaCodes.put("340601", "安徽省淮北市");
		areaCodes.put("340602", "安徽省淮北市杜集区");
		areaCodes.put("340603", "安徽省淮北市相山区");
		areaCodes.put("340604", "安徽省淮北市烈山区");
		areaCodes.put("340621", "安徽省淮北市濉溪县");
		areaCodes.put("340700", "安徽省铜陵市");
		areaCodes.put("340701", "安徽省铜陵市");
		areaCodes.put("340702", "安徽省铜陵市铜官山区");
		areaCodes.put("340703", "安徽省铜陵市狮子山区");
		areaCodes.put("340711", "安徽省铜陵市郊区");
		areaCodes.put("340721", "安徽省铜陵市铜陵县");
		areaCodes.put("340800", "安徽省安庆市");
		areaCodes.put("340801", "安徽省安庆市");
		areaCodes.put("340802", "安徽省安庆市迎江区");
		areaCodes.put("340803", "安徽省安庆市大观区");
		areaCodes.put("340811", "安徽省安庆市郊区");
		areaCodes.put("340822", "安徽省安庆市怀宁县");
		areaCodes.put("340823", "安徽省安庆市枞阳县");
		areaCodes.put("340824", "安徽省安庆市潜山县");
		areaCodes.put("340825", "安徽省安庆市太湖县");
		areaCodes.put("340826", "安徽省安庆市宿松县");
		areaCodes.put("340827", "安徽省安庆市望江县");
		areaCodes.put("340828", "安徽省安庆市岳西县");
		areaCodes.put("340881", "安徽省安庆市桐城市");
		areaCodes.put("341000", "安徽省黄山市");
		areaCodes.put("341001", "安徽省黄山市");
		areaCodes.put("341002", "安徽省黄山市屯溪区");
		areaCodes.put("341003", "安徽省黄山市黄山区");
		areaCodes.put("341004", "安徽省黄山市徽州区");
		areaCodes.put("341021", "安徽省黄山市歙县");
		areaCodes.put("341022", "安徽省黄山市休宁县");
		areaCodes.put("341023", "安徽省黄山市黟县");
		areaCodes.put("341024", "安徽省黄山市祁门县");
		areaCodes.put("341100", "安徽省滁州市");
		areaCodes.put("341101", "安徽省滁州市");
		areaCodes.put("341102", "安徽省滁州市琅琊区");
		areaCodes.put("341103", "安徽省滁州市南谯区");
		areaCodes.put("341122", "安徽省滁州市来安县");
		areaCodes.put("341124", "安徽省滁州市全椒县");
		areaCodes.put("341125", "安徽省滁州市定远县");
		areaCodes.put("341126", "安徽省滁州市凤阳县");
		areaCodes.put("231123", "黑龙江省黑河市逊克县");
		areaCodes.put("330127", "浙江省杭州市淳安县");
		areaCodes.put("330182", "浙江省杭州市建德市");
		areaCodes.put("330183", "浙江省杭州市富阳市");
		areaCodes.put("330185", "浙江省杭州市临安市");
		areaCodes.put("330200", "浙江省宁波市");
		areaCodes.put("330201", "浙江省宁波市");
		areaCodes.put("330203", "浙江省宁波市海曙区");
		areaCodes.put("330204", "浙江省宁波市江东区");
		areaCodes.put("330205", "浙江省宁波市江北区");
		areaCodes.put("330206", "浙江省宁波市北仑区");
		areaCodes.put("330211", "浙江省宁波市镇海区");
		areaCodes.put("330212", "浙江省宁波市鄞州区");
		areaCodes.put("330225", "浙江省宁波市象山县");
		areaCodes.put("330226", "浙江省宁波市宁海县");
		areaCodes.put("330281", "浙江省宁波市余姚市");
		areaCodes.put("330282", "浙江省宁波市慈溪市");
		areaCodes.put("330283", "浙江省宁波市奉化市");
		areaCodes.put("330300", "浙江省温州市");
		areaCodes.put("330301", "浙江省温州市");
		areaCodes.put("330302", "浙江省温州市鹿城区");
		areaCodes.put("330303", "浙江省温州市龙湾区");
		areaCodes.put("330304", "浙江省温州市瓯海区");
		areaCodes.put("330322", "浙江省温州市洞头县");
		areaCodes.put("330324", "浙江省温州市永嘉县");
		areaCodes.put("330326", "浙江省温州市平阳县");
		areaCodes.put("330327", "浙江省温州市苍南县");
		areaCodes.put("330328", "浙江省温州市文成县");
		areaCodes.put("330329", "浙江省温州市泰顺县");
		areaCodes.put("330381", "浙江省温州市瑞安市");
		areaCodes.put("330382", "浙江省温州市乐清市");
		areaCodes.put("330400", "浙江省嘉兴市");
		areaCodes.put("330401", "浙江省嘉兴市");
		areaCodes.put("330402", "浙江省嘉兴市秀城区");
		areaCodes.put("330411", "浙江省嘉兴市秀洲区");
		areaCodes.put("330421", "浙江省嘉兴市嘉善县");
		areaCodes.put("330424", "浙江省嘉兴市海盐县");
		areaCodes.put("330481", "浙江省嘉兴市海宁市");
		areaCodes.put("330482", "浙江省嘉兴市平湖市");
		areaCodes.put("330483", "浙江省嘉兴市桐乡市");
		areaCodes.put("330500", "浙江省湖州市");
		areaCodes.put("330501", "浙江省湖州市");
		areaCodes.put("330502", "浙江省湖州市吴兴区");
		areaCodes.put("330503", "浙江省湖州市南浔区");
		areaCodes.put("330521", "浙江省湖州市德清县");
		areaCodes.put("330522", "浙江省湖州市长兴县");
		areaCodes.put("330523", "浙江省湖州市安吉县");
		areaCodes.put("330600", "浙江省绍兴市");
		areaCodes.put("330601", "浙江省绍兴市");
		areaCodes.put("330602", "浙江省绍兴市越城区");
		areaCodes.put("330621", "浙江省绍兴市绍兴县");
		areaCodes.put("330624", "浙江省绍兴市新昌县");
		areaCodes.put("330681", "浙江省绍兴市诸暨市");
		areaCodes.put("330682", "浙江省绍兴市上虞市");
		areaCodes.put("330683", "浙江省绍兴市嵊州市");
		areaCodes.put("330700", "浙江省金华市");
		areaCodes.put("330701", "浙江省金华市");
		areaCodes.put("330702", "浙江省金华市婺城区");
		areaCodes.put("330703", "浙江省金华市金东区");
		areaCodes.put("330723", "浙江省金华市武义县");
		areaCodes.put("330726", "浙江省金华市浦江县");
		areaCodes.put("330727", "浙江省金华市磐安县");
		areaCodes.put("320205", "江苏省无锡市锡山区");
		areaCodes.put("320206", "江苏省无锡市惠山区");
		areaCodes.put("320211", "江苏省无锡市滨湖区");
		areaCodes.put("320281", "江苏省无锡市江阴市");
		areaCodes.put("320282", "江苏省无锡市宜兴市");
		areaCodes.put("320300", "江苏省徐州市");
		areaCodes.put("320301", "江苏省徐州市");
		areaCodes.put("320302", "江苏省徐州市鼓楼区");
		areaCodes.put("320303", "江苏省徐州市云龙区");
		areaCodes.put("320304", "江苏省徐州市九里区");
		areaCodes.put("320305", "江苏省徐州市贾汪区");
		areaCodes.put("320311", "江苏省徐州市泉山区");
		areaCodes.put("320321", "江苏省徐州市丰县");
		areaCodes.put("320322", "江苏省徐州市沛县");
		areaCodes.put("320323", "江苏省徐州市铜山县");
		areaCodes.put("320324", "江苏省徐州市睢宁县");
		areaCodes.put("320381", "江苏省徐州市新沂市");
		areaCodes.put("320382", "江苏省徐州市邳州市");
		areaCodes.put("320400", "江苏省常州市");
		areaCodes.put("320401", "江苏省常州市");
		areaCodes.put("320402", "江苏省常州市天宁区");
		areaCodes.put("320404", "江苏省常州市钟楼区");
		areaCodes.put("320405", "江苏省常州市戚墅堰区");
		areaCodes.put("320411", "江苏省常州市新北区");
		areaCodes.put("320412", "江苏省常州市武进区");
		areaCodes.put("320481", "江苏省常州市溧阳市");
		areaCodes.put("320482", "江苏省常州市金坛市");
		areaCodes.put("320500", "江苏省苏州市");
		areaCodes.put("320501", "江苏省苏州市");
		areaCodes.put("320502", "江苏省苏州市沧浪区");
		areaCodes.put("320503", "江苏省苏州市平江区");
		areaCodes.put("320504", "江苏省苏州市金阊区");
		areaCodes.put("320505", "江苏省苏州市虎丘区");
		areaCodes.put("320506", "江苏省苏州市吴中区");
		areaCodes.put("320507", "江苏省苏州市相城区");
		areaCodes.put("320581", "江苏省苏州市常熟市");
		areaCodes.put("320582", "江苏省苏州市张家港市");
		areaCodes.put("320583", "江苏省苏州市昆山市");
		areaCodes.put("320584", "江苏省苏州市吴江市");
		areaCodes.put("320585", "江苏省苏州市太仓市");
		areaCodes.put("320600", "江苏省南通市");
		areaCodes.put("320601", "江苏省南通市");
		areaCodes.put("320602", "江苏省南通市崇川区");
		areaCodes.put("320611", "江苏省南通市港闸区");
		areaCodes.put("320621", "江苏省南通市海安县");
		areaCodes.put("320623", "江苏省南通市如东县");
		areaCodes.put("320681", "江苏省南通市启东市");
		areaCodes.put("320682", "江苏省南通市如皋市");
		areaCodes.put("320683", "江苏省南通市通州市");
		areaCodes.put("320684", "江苏省南通市海门市");
		areaCodes.put("320700", "江苏省连云港市");
		areaCodes.put("320701", "江苏省连云港市");
		areaCodes.put("320703", "江苏省连云港市连云区");
		areaCodes.put("320705", "江苏省连云港市新浦区");
		areaCodes.put("320706", "江苏省连云港市海州区");
		areaCodes.put("320721", "江苏省连云港市赣榆县");
		areaCodes.put("320722", "江苏省连云港市东海县");
		areaCodes.put("230603", "黑龙江省大庆市龙凤区");
		areaCodes.put("230604", "黑龙江省大庆市让胡路区");
		areaCodes.put("230605", "黑龙江省大庆市红岗区");
		areaCodes.put("230606", "黑龙江省大庆市大同区");
		areaCodes.put("220501", "吉林省通化市");
		areaCodes.put("220502", "吉林省通化市东昌区");
		areaCodes.put("220503", "吉林省通化市二道江区");
		areaCodes.put("220521", "吉林省通化市通化县");
		areaCodes.put("220523", "吉林省通化市辉南县");
		areaCodes.put("220524", "吉林省通化市柳河县");
		areaCodes.put("220581", "吉林省通化市梅河口市");
		areaCodes.put("220582", "吉林省通化市集安市");
		areaCodes.put("220600", "吉林省白山市");
		areaCodes.put("220601", "吉林省白山市");
		areaCodes.put("220602", "吉林省白山市八道江区");
		areaCodes.put("220621", "吉林省白山市抚松县");
		areaCodes.put("220622", "吉林省白山市靖宇县");
		areaCodes.put("220623", "吉林省白山市长白朝鲜族自治县");
		areaCodes.put("220625", "吉林省白山市江源县");
		areaCodes.put("220681", "吉林省白山市临江市");
		areaCodes.put("220700", "吉林省松原市");
		areaCodes.put("220701", "吉林省松原市");
		areaCodes.put("220702", "吉林省松原市宁江区");
		areaCodes.put("220721", "吉林省松原市前郭尔罗斯蒙古族自治县");
		areaCodes.put("220722", "吉林省松原市长岭县");
		areaCodes.put("220723", "吉林省松原市乾安县");
		areaCodes.put("220724", "吉林省松原市扶余县");
		areaCodes.put("220800", "吉林省白城市");
		areaCodes.put("220801", "吉林省白城市");
		areaCodes.put("220802", "吉林省白城市洮北区");
		areaCodes.put("220821", "吉林省白城市镇赉县");
		areaCodes.put("220822", "吉林省白城市通榆县");
		areaCodes.put("220881", "吉林省白城市洮南市");
		areaCodes.put("220882", "吉林省白城市大安市");
		areaCodes.put("222400", "吉林省延边朝鲜族自治州");
		areaCodes.put("222401", "吉林省延边朝鲜族自治州延吉市");
		areaCodes.put("222402", "吉林省延边朝鲜族自治州图们市");
		areaCodes.put("222403", "吉林省延边朝鲜族自治州敦化市");
		areaCodes.put("222404", "吉林省延边朝鲜族自治州珲春市");
		areaCodes.put("222405", "吉林省延边朝鲜族自治州龙井市");
		areaCodes.put("222406", "吉林省延边朝鲜族自治州和龙市");
		areaCodes.put("222424", "吉林省延边朝鲜族自治州汪清县");
		areaCodes.put("222426", "吉林省延边朝鲜族自治州安图县");
		areaCodes.put("230000", "黑龙江省");
		areaCodes.put("230100", "黑龙江省哈尔滨市");
		areaCodes.put("230101", "黑龙江省哈尔滨市");
		areaCodes.put("230102", "黑龙江省哈尔滨市道里区");
		areaCodes.put("230103", "黑龙江省哈尔滨市南岗区");
		areaCodes.put("230104", "黑龙江省哈尔滨市道外区");
		areaCodes.put("230106", "黑龙江省哈尔滨市香坊区");
		areaCodes.put("230107", "黑龙江省哈尔滨市动力区");
		areaCodes.put("230108", "黑龙江省哈尔滨市平房区");
		areaCodes.put("230109", "黑龙江省哈尔滨市松北区");
		areaCodes.put("230111", "黑龙江省哈尔滨市呼兰区");
		areaCodes.put("230123", "黑龙江省哈尔滨市依兰县");
		areaCodes.put("230124", "黑龙江省哈尔滨市方正县");
		areaCodes.put("230125", "黑龙江省哈尔滨市宾县");
		areaCodes.put("230126", "黑龙江省哈尔滨市巴彦县");
		areaCodes.put("230127", "黑龙江省哈尔滨市木兰县");
		areaCodes.put("230128", "黑龙江省哈尔滨市通河县");
		areaCodes.put("230129", "黑龙江省哈尔滨市延寿县");
		areaCodes.put("230181", "黑龙江省哈尔滨市阿城市");
		areaCodes.put("230182", "黑龙江省哈尔滨市双城市");
		areaCodes.put("150622", "内蒙古自治区鄂尔多斯市准格尔旗");
		areaCodes.put("150623", "内蒙古自治区鄂尔多斯市鄂托克前旗");
		areaCodes.put("150624", "内蒙古自治区鄂尔多斯市鄂托克旗");
		areaCodes.put("150625", "内蒙古自治区鄂尔多斯市杭锦旗");
		areaCodes.put("150626", "内蒙古自治区鄂尔多斯市乌审旗");
		areaCodes.put("150627", "内蒙古自治区鄂尔多斯市伊金霍洛旗");
		areaCodes.put("150700", "内蒙古自治区呼伦贝尔市");
		areaCodes.put("150701", "内蒙古自治区呼伦贝尔市");
		areaCodes.put("150702", "内蒙古自治区呼伦贝尔市海拉尔区");
		areaCodes.put("150721", "内蒙古自治区呼伦贝尔市阿荣旗");
		areaCodes.put("150722", "内蒙古自治区呼伦贝尔市莫力达瓦达斡尔族自治旗");
		areaCodes.put("150723", "内蒙古自治区呼伦贝尔市鄂伦春自治旗");
		areaCodes.put("150724", "内蒙古自治区呼伦贝尔市鄂温克族自治旗");
		areaCodes.put("150725", "内蒙古自治区呼伦贝尔市陈巴尔虎旗");
		areaCodes.put("150726", "内蒙古自治区呼伦贝尔市新巴尔虎左旗");
		areaCodes.put("150727", "内蒙古自治区呼伦贝尔市新巴尔虎右旗");
		areaCodes.put("150781", "内蒙古自治区呼伦贝尔市满洲里市");
		areaCodes.put("150782", "内蒙古自治区呼伦贝尔市牙克石市");
		areaCodes.put("150783", "内蒙古自治区呼伦贝尔市扎兰屯市");
		areaCodes.put("150784", "内蒙古自治区呼伦贝尔市额尔古纳市");
		areaCodes.put("150785", "内蒙古自治区呼伦贝尔市根河市");
		areaCodes.put("150800", "内蒙古自治区巴彦淖尔市");
		areaCodes.put("150801", "内蒙古自治区巴彦淖尔市");
		areaCodes.put("150802", "内蒙古自治区巴彦淖尔市临河区");
		areaCodes.put("150821", "内蒙古自治区巴彦淖尔市五原县");
		areaCodes.put("150822", "内蒙古自治区巴彦淖尔市磴口县");
		areaCodes.put("150823", "内蒙古自治区巴彦淖尔市乌拉特前旗");
		areaCodes.put("150824", "内蒙古自治区巴彦淖尔市乌拉特中旗");
		areaCodes.put("150825", "内蒙古自治区巴彦淖尔市乌拉特后旗");
		areaCodes.put("150826", "内蒙古自治区巴彦淖尔市杭锦后旗");
		areaCodes.put("150900", "内蒙古自治区乌兰察布市");
		areaCodes.put("150901", "内蒙古自治区乌兰察布市");
		areaCodes.put("150902", "内蒙古自治区乌兰察布市集宁区");
		areaCodes.put("150921", "内蒙古自治区乌兰察布市卓资县");
		areaCodes.put("150922", "内蒙古自治区乌兰察布市化德县");
		areaCodes.put("150923", "内蒙古自治区乌兰察布市商都县");
		areaCodes.put("150924", "内蒙古自治区乌兰察布市兴和县");
		areaCodes.put("150925", "内蒙古自治区乌兰察布市凉城县");
		areaCodes.put("150926", "内蒙古自治区乌兰察布市察哈尔右翼前旗");
		areaCodes.put("320100", "江苏省南京市");
		areaCodes.put("320101", "江苏省南京市");
		areaCodes.put("320102", "江苏省南京市玄武区");
		areaCodes.put("320103", "江苏省南京市白下区");
		areaCodes.put("320104", "江苏省南京市秦淮区");
		areaCodes.put("320105", "江苏省南京市建邺区");
		areaCodes.put("320106", "江苏省南京市鼓楼区");
		areaCodes.put("320107", "江苏省南京市下关区");
		areaCodes.put("320111", "江苏省南京市浦口区");
		areaCodes.put("320113", "江苏省南京市栖霞区");
		areaCodes.put("320114", "江苏省南京市雨花台区");
		areaCodes.put("320115", "江苏省南京市江宁区");
		areaCodes.put("320116", "江苏省南京市六合区");
		areaCodes.put("320124", "江苏省南京市溧水县");
		areaCodes.put("320125", "江苏省南京市高淳县");
		areaCodes.put("320200", "江苏省无锡市");
		areaCodes.put("320201", "江苏省无锡市");
		areaCodes.put("320202", "江苏省无锡市崇安区");
		areaCodes.put("320203", "江苏省无锡市南长区");
		areaCodes.put("320204", "江苏省无锡市北塘区");
		areaCodes.put("230621", "黑龙江省大庆市肇州县");
		areaCodes.put("371602", "山东省滨州市滨城区");
		areaCodes.put("371621", "山东省滨州市惠民县");
		areaCodes.put("371622", "山东省滨州市阳信县");
		areaCodes.put("371623", "山东省滨州市无棣县");
		areaCodes.put("371624", "山东省滨州市沾化县");
		areaCodes.put("371625", "山东省滨州市博兴县");
		areaCodes.put("371626", "山东省滨州市邹平县");
		areaCodes.put("371700", "山东省荷泽市");
		areaCodes.put("371701", "山东省荷泽市");
		areaCodes.put("371702", "山东省荷泽市牡丹区");
		areaCodes.put("371721", "山东省荷泽市曹县");
		areaCodes.put("371722", "山东省荷泽市单县");
		areaCodes.put("371723", "山东省荷泽市成武县");
		areaCodes.put("371724", "山东省荷泽市巨野县");
		areaCodes.put("371725", "山东省荷泽市郓城县");
		areaCodes.put("371726", "山东省荷泽市鄄城县");
		areaCodes.put("371727", "山东省荷泽市定陶县");
		areaCodes.put("371728", "山东省荷泽市东明县");
		areaCodes.put("410000", "河南省");
		areaCodes.put("410100", "河南省郑州市");
		areaCodes.put("410101", "河南省郑州市");
		areaCodes.put("410102", "河南省郑州市中原区");
		areaCodes.put("410103", "河南省郑州市二七区");
		areaCodes.put("410104", "河南省郑州市管城回族区");
		areaCodes.put("410105", "河南省郑州市金水区");
		areaCodes.put("410106", "河南省郑州市上街区");
		areaCodes.put("410108", "河南省郑州市惠济区");
		areaCodes.put("410122", "河南省郑州市中牟县");
		areaCodes.put("410181", "河南省郑州市巩义市");
		areaCodes.put("410182", "河南省郑州市荥阳市");
		areaCodes.put("410183", "河南省郑州市新密市");
		areaCodes.put("410184", "河南省郑州市新郑市");
		areaCodes.put("410185", "河南省郑州市登封市");
		areaCodes.put("410200", "河南省开封市");
		areaCodes.put("410201", "河南省开封市");
		areaCodes.put("410202", "河南省开封市龙亭区");
		areaCodes.put("410203", "河南省开封市顺河回族区");
		areaCodes.put("410204", "河南省开封市鼓楼区");
		areaCodes.put("410205", "河南省开封市南关区");
		areaCodes.put("410211", "河南省开封市郊区");
		areaCodes.put("410221", "河南省开封市杞县");
		areaCodes.put("410222", "河南省开封市通许县");
		areaCodes.put("410223", "河南省开封市尉氏县");
		areaCodes.put("410224", "河南省开封市开封县");
		areaCodes.put("410225", "河南省开封市兰考县");
		areaCodes.put("410300", "河南省洛阳市");
		areaCodes.put("410301", "河南省洛阳市");
		areaCodes.put("410302", "河南省洛阳市老城区");
		areaCodes.put("410303", "河南省洛阳市西工区");
		areaCodes.put("410304", "河南省洛阳市廛河回族区");
		areaCodes.put("410305", "河南省洛阳市涧西区");
		areaCodes.put("410306", "河南省洛阳市吉利区");
		areaCodes.put("410307", "河南省洛阳市洛龙区");
		areaCodes.put("410322", "河南省洛阳市孟津县");
		areaCodes.put("410323", "河南省洛阳市新安县");
		areaCodes.put("410324", "河南省洛阳市栾川县");
		areaCodes.put("410325", "河南省洛阳市嵩县");
		areaCodes.put("410326", "河南省洛阳市汝阳县");
		areaCodes.put("410327", "河南省洛阳市宜阳县");
		areaCodes.put("410328", "河南省洛阳市洛宁县");
		areaCodes.put("341181", "安徽省滁州市天长市");
		areaCodes.put("341182", "安徽省滁州市明光市");
		areaCodes.put("370900", "山东省泰安市");
		areaCodes.put("370901", "山东省泰安市");
		areaCodes.put("370902", "山东省泰安市泰山区");
		areaCodes.put("370903", "山东省泰安市岱岳区");
		areaCodes.put("370921", "山东省泰安市宁阳县");
		areaCodes.put("370923", "山东省泰安市东平县");
		areaCodes.put("370982", "山东省泰安市新泰市");
		areaCodes.put("370983", "山东省泰安市肥城市");
		areaCodes.put("371000", "山东省威海市");
		areaCodes.put("371001", "山东省威海市");
		areaCodes.put("371002", "山东省威海市环翠区");
		areaCodes.put("371081", "山东省威海市文登市");
		areaCodes.put("371082", "山东省威海市荣成市");
		areaCodes.put("371083", "山东省威海市乳山市");
		areaCodes.put("371100", "山东省日照市");
		areaCodes.put("371101", "山东省日照市");
		areaCodes.put("371102", "山东省日照市东港区");
		areaCodes.put("371103", "山东省日照市岚山区");
		areaCodes.put("371121", "山东省日照市五莲县");
		areaCodes.put("371122", "山东省日照市莒县");
		areaCodes.put("371200", "山东省莱芜市");
		areaCodes.put("371201", "山东省莱芜市");
		areaCodes.put("371202", "山东省莱芜市莱城区");
		areaCodes.put("371203", "山东省莱芜市钢城区");
		areaCodes.put("371300", "山东省临沂市");
		areaCodes.put("371301", "山东省临沂市");
		areaCodes.put("371302", "山东省临沂市兰山区");
		areaCodes.put("371311", "山东省临沂市罗庄区");
		areaCodes.put("371312", "山东省临沂市河东区");
		areaCodes.put("371321", "山东省临沂市沂南县");
		areaCodes.put("371322", "山东省临沂市郯城县");
		areaCodes.put("371323", "山东省临沂市沂水县");
		areaCodes.put("371324", "山东省临沂市苍山县");
		areaCodes.put("371325", "山东省临沂市费县");
		areaCodes.put("371326", "山东省临沂市平邑县");
		areaCodes.put("371327", "山东省临沂市莒南县");
		areaCodes.put("371328", "山东省临沂市蒙阴县");
		areaCodes.put("371329", "山东省临沂市临沭县");
		areaCodes.put("371400", "山东省德州市");
		areaCodes.put("371401", "山东省德州市");
		areaCodes.put("371402", "山东省德州市德城区");
		areaCodes.put("371421", "山东省德州市陵县");
		areaCodes.put("371422", "山东省德州市宁津县");
		areaCodes.put("371423", "山东省德州市庆云县");
		areaCodes.put("371424", "山东省德州市临邑县");
		areaCodes.put("371425", "山东省德州市齐河县");
		areaCodes.put("371426", "山东省德州市平原县");
		areaCodes.put("371427", "山东省德州市夏津县");
		areaCodes.put("371428", "山东省德州市武城县");
		areaCodes.put("371481", "山东省德州市乐陵市");
		areaCodes.put("371482", "山东省德州市禹城市");
		areaCodes.put("371500", "山东省聊城市");
		areaCodes.put("371501", "山东省聊城市");
		areaCodes.put("371502", "山东省聊城市东昌府区");
		areaCodes.put("371521", "山东省聊城市阳谷县");
		areaCodes.put("371522", "山东省聊城市莘县");
		areaCodes.put("371523", "山东省聊城市茌平县");
		areaCodes.put("371524", "山东省聊城市东阿县");
		areaCodes.put("371525", "山东省聊城市冠县");
		areaCodes.put("371526", "山东省聊城市高唐县");
		areaCodes.put("371581", "山东省聊城市临清市");
		areaCodes.put("421123", "湖北省黄冈市罗田县");
		areaCodes.put("371601", "山东省滨州市");
		areaCodes.put("350629", "福建省漳州市华安县");
		areaCodes.put("350681", "福建省漳州市龙海市");
		areaCodes.put("350700", "福建省南平市");
		areaCodes.put("350701", "福建省南平市");
		areaCodes.put("350702", "福建省南平市延平区");
		areaCodes.put("350721", "福建省南平市顺昌县");
		areaCodes.put("350722", "福建省南平市浦城县");
		areaCodes.put("350723", "福建省南平市光泽县");
		areaCodes.put("350724", "福建省南平市松溪县");
		areaCodes.put("350725", "福建省南平市政和县");
		areaCodes.put("350781", "福建省南平市邵武市");
		areaCodes.put("350782", "福建省南平市武夷山市");
		areaCodes.put("350783", "福建省南平市建瓯市");
		areaCodes.put("350784", "福建省南平市建阳市");
		areaCodes.put("350800", "福建省龙岩市");
		areaCodes.put("350801", "福建省龙岩市");
		areaCodes.put("350802", "福建省龙岩市新罗区");
		areaCodes.put("350821", "福建省龙岩市长汀县");
		areaCodes.put("350822", "福建省龙岩市永定县");
		areaCodes.put("350823", "福建省龙岩市上杭县");
		areaCodes.put("350824", "福建省龙岩市武平县");
		areaCodes.put("350825", "福建省龙岩市连城县");
		areaCodes.put("350881", "福建省龙岩市漳平市");
		areaCodes.put("350900", "福建省宁德市");
		areaCodes.put("350901", "福建省宁德市");
		areaCodes.put("350902", "福建省宁德市蕉城区");
		areaCodes.put("350921", "福建省宁德市霞浦县");
		areaCodes.put("350922", "福建省宁德市古田县");
		areaCodes.put("350923", "福建省宁德市屏南县");
		areaCodes.put("350924", "福建省宁德市寿宁县");
		areaCodes.put("350925", "福建省宁德市周宁县");
		areaCodes.put("350926", "福建省宁德市柘荣县");
		areaCodes.put("350981", "福建省宁德市福安市");
		areaCodes.put("350982", "福建省宁德市福鼎市");
		areaCodes.put("360000", "江西省");
		areaCodes.put("360100", "江西省南昌市");
		areaCodes.put("360101", "江西省南昌市");
		areaCodes.put("360102", "江西省南昌市东湖区");
		areaCodes.put("360103", "江西省南昌市西湖区");
		areaCodes.put("360104", "江西省南昌市青云谱区");
		areaCodes.put("360105", "江西省南昌市湾里区");
		areaCodes.put("360111", "江西省南昌市青山湖区");
		areaCodes.put("360121", "江西省南昌市南昌县");
		areaCodes.put("360122", "江西省南昌市新建县");
		areaCodes.put("360123", "江西省南昌市安义县");
		areaCodes.put("360124", "江西省南昌市进贤县");
		areaCodes.put("360200", "江西省景德镇市");
		areaCodes.put("360201", "江西省景德镇市");
		areaCodes.put("360202", "江西省景德镇市昌江区");
		areaCodes.put("360203", "江西省景德镇市珠山区");
		areaCodes.put("360222", "江西省景德镇市浮梁县");
		areaCodes.put("360281", "江西省景德镇市乐平市");
		areaCodes.put("360300", "江西省萍乡市");
		areaCodes.put("360301", "江西省萍乡市");
		areaCodes.put("360302", "江西省萍乡市安源区");
		areaCodes.put("360313", "江西省萍乡市湘东区");
		areaCodes.put("360321", "江西省萍乡市莲花县");
		areaCodes.put("360322", "江西省萍乡市上栗县");
		areaCodes.put("360323", "江西省萍乡市芦溪县");
		areaCodes.put("360400", "江西省九江市");
		areaCodes.put("360401", "江西省九江市");
		areaCodes.put("360402", "江西省九江市庐山区");
		areaCodes.put("360421", "江西省九江市九江县");
		areaCodes.put("370303", "山东省淄博市张店区");
		areaCodes.put("370304", "山东省淄博市博山区");
		areaCodes.put("370305", "山东省淄博市临淄区");
		areaCodes.put("370306", "山东省淄博市周村区");
		areaCodes.put("370321", "山东省淄博市桓台县");
		areaCodes.put("370322", "山东省淄博市高青县");
		areaCodes.put("370323", "山东省淄博市沂源县");
		areaCodes.put("370400", "山东省枣庄市");
		areaCodes.put("370401", "山东省枣庄市");
		areaCodes.put("370402", "山东省枣庄市市中区");
		areaCodes.put("370403", "山东省枣庄市薛城区");
		areaCodes.put("370404", "山东省枣庄市峄城区");
		areaCodes.put("370405", "山东省枣庄市台儿庄区");
		areaCodes.put("370406", "山东省枣庄市山亭区");
		areaCodes.put("370481", "山东省枣庄市滕州市");
		areaCodes.put("370500", "山东省东营市");
		areaCodes.put("370501", "山东省东营市");
		areaCodes.put("370502", "山东省东营市东营区");
		areaCodes.put("370503", "山东省东营市河口区");
		areaCodes.put("370521", "山东省东营市垦利县");
		areaCodes.put("370522", "山东省东营市利津县");
		areaCodes.put("370523", "山东省东营市广饶县");
		areaCodes.put("370600", "山东省烟台市");
		areaCodes.put("370601", "山东省烟台市");
		areaCodes.put("370602", "山东省烟台市芝罘区");
		areaCodes.put("370611", "山东省烟台市福山区");
		areaCodes.put("370612", "山东省烟台市牟平区");
		areaCodes.put("370613", "山东省烟台市莱山区");
		areaCodes.put("370634", "山东省烟台市长岛县");
		areaCodes.put("370681", "山东省烟台市龙口市");
		areaCodes.put("370682", "山东省烟台市莱阳市");
		areaCodes.put("370683", "山东省烟台市莱州市");
		areaCodes.put("370684", "山东省烟台市蓬莱市");
		areaCodes.put("370685", "山东省烟台市招远市");
		areaCodes.put("370686", "山东省烟台市栖霞市");
		areaCodes.put("370687", "山东省烟台市海阳市");
		areaCodes.put("370700", "山东省潍坊市");
		areaCodes.put("370701", "山东省潍坊市");
		areaCodes.put("370702", "山东省潍坊市潍城区");
		areaCodes.put("370703", "山东省潍坊市寒亭区");
		areaCodes.put("370704", "山东省潍坊市坊子区");
		areaCodes.put("370705", "山东省潍坊市奎文区");
		areaCodes.put("370724", "山东省潍坊市临朐县");
		areaCodes.put("370725", "山东省潍坊市昌乐县");
		areaCodes.put("370781", "山东省潍坊市青州市");
		areaCodes.put("370782", "山东省潍坊市诸城市");
		areaCodes.put("370783", "山东省潍坊市寿光市");
		areaCodes.put("370784", "山东省潍坊市安丘市");
		areaCodes.put("370785", "山东省潍坊市高密市");
		areaCodes.put("370786", "山东省潍坊市昌邑市");
		areaCodes.put("370800", "山东省济宁市");
		areaCodes.put("370801", "山东省济宁市");
		areaCodes.put("370802", "山东省济宁市市中区");
		areaCodes.put("370811", "山东省济宁市任城区");
		areaCodes.put("211202", "辽宁省铁岭市银州区");
		areaCodes.put("211204", "辽宁省铁岭市清河区");
		areaCodes.put("141182", "山西省吕梁市汾阳市");
		areaCodes.put("152523", "内蒙古自治区锡林郭勒盟苏尼特左旗");
		areaCodes.put("152524", "内蒙古自治区锡林郭勒盟苏尼特右旗");
		areaCodes.put("152525", "内蒙古自治区锡林郭勒盟东乌珠穆沁旗");
		areaCodes.put("152526", "内蒙古自治区锡林郭勒盟西乌珠穆沁旗");
		areaCodes.put("152527", "内蒙古自治区锡林郭勒盟太仆寺旗");
		areaCodes.put("152528", "内蒙古自治区锡林郭勒盟镶黄旗");
		areaCodes.put("152529", "内蒙古自治区锡林郭勒盟正镶白旗");
		areaCodes.put("152530", "内蒙古自治区锡林郭勒盟正蓝旗");
		areaCodes.put("152531", "内蒙古自治区锡林郭勒盟多伦县");
		areaCodes.put("152900", "内蒙古自治区阿拉善盟");
		areaCodes.put("152921", "内蒙古自治区阿拉善盟阿拉善左旗");
		areaCodes.put("152922", "内蒙古自治区阿拉善盟阿拉善右旗");
		areaCodes.put("152923", "内蒙古自治区阿拉善盟额济纳旗");
		areaCodes.put("210000", "辽宁省");
		areaCodes.put("210100", "辽宁省沈阳市");
		areaCodes.put("210101", "辽宁省沈阳市");
		areaCodes.put("210102", "辽宁省沈阳市和平区");
		areaCodes.put("210103", "辽宁省沈阳市沈河区");
		areaCodes.put("210104", "辽宁省沈阳市大东区");
		areaCodes.put("210105", "辽宁省沈阳市皇姑区");
		areaCodes.put("210106", "辽宁省沈阳市铁西区");
		areaCodes.put("210111", "辽宁省沈阳市苏家屯区");
		areaCodes.put("210112", "辽宁省沈阳市东陵区");
		areaCodes.put("210113", "辽宁省沈阳市新城子区");
		areaCodes.put("210114", "辽宁省沈阳市于洪区");
		areaCodes.put("210122", "辽宁省沈阳市辽中县");
		areaCodes.put("210123", "辽宁省沈阳市康平县");
		areaCodes.put("210124", "辽宁省沈阳市法库县");
		areaCodes.put("210181", "辽宁省沈阳市新民市");
		areaCodes.put("210200", "辽宁省大连市");
		areaCodes.put("210201", "辽宁省大连市");
		areaCodes.put("210202", "辽宁省大连市中山区");
		areaCodes.put("210203", "辽宁省大连市西岗区");
		areaCodes.put("210204", "辽宁省大连市沙河口区");
		areaCodes.put("210211", "辽宁省大连市甘井子区");
		areaCodes.put("210212", "辽宁省大连市旅顺口区");
		areaCodes.put("210213", "辽宁省大连市金州区");
		areaCodes.put("210224", "辽宁省大连市长海县");
		areaCodes.put("210281", "辽宁省大连市瓦房店市");
		areaCodes.put("210282", "辽宁省大连市普兰店市");
		areaCodes.put("210283", "辽宁省大连市庄河市");
		areaCodes.put("210300", "辽宁省鞍山市");
		areaCodes.put("210301", "辽宁省鞍山市");
		areaCodes.put("210302", "辽宁省鞍山市铁东区");
		areaCodes.put("210303", "辽宁省鞍山市铁西区");
		areaCodes.put("210304", "辽宁省鞍山市立山区");
		areaCodes.put("210311", "辽宁省鞍山市千山区");
		areaCodes.put("210321", "辽宁省鞍山市台安县");
		areaCodes.put("210323", "辽宁省鞍山市岫岩满族自治县");
		areaCodes.put("210381", "辽宁省鞍山市海城市");
		areaCodes.put("210400", "辽宁省抚顺市");
		areaCodes.put("210401", "辽宁省抚顺市");
		areaCodes.put("210402", "辽宁省抚顺市新抚区");
		areaCodes.put("210403", "辽宁省抚顺市东洲区");
		areaCodes.put("210404", "辽宁省抚顺市望花区");
		areaCodes.put("210411", "辽宁省抚顺市顺城区");
		areaCodes.put("210421", "辽宁省抚顺市抚顺县");
		areaCodes.put("210422", "辽宁省抚顺市新宾满族自治县");
		areaCodes.put("210423", "辽宁省抚顺市清原满族自治县");
		areaCodes.put("320803", "江苏省淮安市楚州区");
		areaCodes.put("320804", "江苏省淮安市淮阴区");
		areaCodes.put("320811", "江苏省淮安市清浦区");
		areaCodes.put("320826", "江苏省淮安市涟水县");
		areaCodes.put("320829", "江苏省淮安市洪泽县");
		areaCodes.put("320830", "江苏省淮安市盱眙县");
		areaCodes.put("320831", "江苏省淮安市金湖县");
		areaCodes.put("320900", "江苏省盐城市");
		areaCodes.put("320901", "江苏省盐城市");
		areaCodes.put("320902", "江苏省盐城市亭湖区");
		areaCodes.put("320903", "江苏省盐城市盐都区");
		areaCodes.put("320921", "江苏省盐城市响水县");
		areaCodes.put("320922", "江苏省盐城市滨海县");
		areaCodes.put("320923", "江苏省盐城市阜宁县");
		areaCodes.put("320924", "江苏省盐城市射阳县");
		areaCodes.put("320925", "江苏省盐城市建湖县");
		areaCodes.put("320981", "江苏省盐城市东台市");
		areaCodes.put("320982", "江苏省盐城市大丰市");
		areaCodes.put("321000", "江苏省扬州市");
		areaCodes.put("321001", "江苏省扬州市");
		areaCodes.put("321002", "江苏省扬州市广陵区");
		areaCodes.put("321003", "江苏省扬州市邗江区");
		areaCodes.put("321011", "江苏省扬州市郊区");
		areaCodes.put("321023", "江苏省扬州市宝应县");
		areaCodes.put("321081", "江苏省扬州市仪征市");
		areaCodes.put("321084", "江苏省扬州市高邮市");
		areaCodes.put("321088", "江苏省扬州市江都市");
		areaCodes.put("321100", "江苏省镇江市");
		areaCodes.put("321101", "江苏省镇江市");
		areaCodes.put("321102", "江苏省镇江市京口区");
		areaCodes.put("321111", "江苏省镇江市润州区");
		areaCodes.put("321112", "江苏省镇江市丹徒区");
		areaCodes.put("321181", "江苏省镇江市丹阳市");
		areaCodes.put("321182", "江苏省镇江市扬中市");
		areaCodes.put("321183", "江苏省镇江市句容市");
		areaCodes.put("321200", "江苏省泰州市");
		areaCodes.put("321201", "江苏省泰州市");
		areaCodes.put("321202", "江苏省泰州市海陵区");
		areaCodes.put("321203", "江苏省泰州市高港区");
		areaCodes.put("321281", "江苏省泰州市兴化市");
		areaCodes.put("321282", "江苏省泰州市靖江市");
		areaCodes.put("321283", "江苏省泰州市泰兴市");
		areaCodes.put("321284", "江苏省泰州市姜堰市");
		areaCodes.put("341621", "安徽省亳州市涡阳县");
		areaCodes.put("341622", "安徽省亳州市蒙城县");
		areaCodes.put("341623", "安徽省亳州市利辛县");
		areaCodes.put("341700", "安徽省池州市");
		areaCodes.put("341701", "安徽省池州市");
		areaCodes.put("341702", "安徽省池州市贵池区");
		areaCodes.put("341721", "安徽省池州市东至县");
		areaCodes.put("341722", "安徽省池州市石台县");
		areaCodes.put("341723", "安徽省池州市青阳县");
		areaCodes.put("341800", "安徽省宣城市");
		areaCodes.put("341801", "安徽省宣城市");
		areaCodes.put("341802", "安徽省宣城市宣州区");
		areaCodes.put("341821", "安徽省宣城市郎溪县");
		areaCodes.put("341822", "安徽省宣城市广德县");
		areaCodes.put("341823", "安徽省宣城市泾县");
		areaCodes.put("341824", "安徽省宣城市绩溪县");
		areaCodes.put("341825", "安徽省宣城市旌德县");
		areaCodes.put("341881", "安徽省宣城市宁国市");
		areaCodes.put("350000", "福建省");
		areaCodes.put("350100", "福建省福州市");
		areaCodes.put("350101", "福建省福州市");
		areaCodes.put("350102", "福建省福州市鼓楼区");
		areaCodes.put("350103", "福建省福州市台江区");
		areaCodes.put("350104", "福建省福州市仓山区");
		areaCodes.put("350105", "福建省福州市马尾区");
		areaCodes.put("370882", "山东省济宁市兖州市");
		areaCodes.put("350628", "福建省漳州市平和县");
		areaCodes.put("360926", "江西省宜春市铜鼓县");
		areaCodes.put("360981", "江西省宜春市丰城市");
		areaCodes.put("360982", "江西省宜春市樟树市");
		areaCodes.put("360983", "江西省宜春市高安市");
		areaCodes.put("361000", "江西省抚州市");
		areaCodes.put("361001", "江西省抚州市");
		areaCodes.put("361002", "江西省抚州市临川区");
		areaCodes.put("361021", "江西省抚州市南城县");
		areaCodes.put("361022", "江西省抚州市黎川县");
		areaCodes.put("361023", "江西省抚州市南丰县");
		areaCodes.put("361024", "江西省抚州市崇仁县");
		areaCodes.put("361025", "江西省抚州市乐安县");
		areaCodes.put("361026", "江西省抚州市宜黄县");
		areaCodes.put("361027", "江西省抚州市金溪县");
		areaCodes.put("361028", "江西省抚州市资溪县");
		areaCodes.put("361029", "江西省抚州市东乡县");
		areaCodes.put("361030", "江西省抚州市广昌县");
		areaCodes.put("361100", "江西省上饶市");
		areaCodes.put("361101", "江西省上饶市");
		areaCodes.put("361102", "江西省上饶市信州区");
		areaCodes.put("361121", "江西省上饶市上饶县");
		areaCodes.put("361122", "江西省上饶市广丰县");
		areaCodes.put("361123", "江西省上饶市玉山县");
		areaCodes.put("361124", "江西省上饶市铅山县");
		areaCodes.put("361125", "江西省上饶市横峰县");
		areaCodes.put("361126", "江西省上饶市弋阳县");
		areaCodes.put("361127", "江西省上饶市余干县");
		areaCodes.put("361128", "江西省上饶市鄱阳县");
		areaCodes.put("361129", "江西省上饶市万年县");
		areaCodes.put("361130", "江西省上饶市婺源县");
		areaCodes.put("361181", "江西省上饶市德兴市");
		areaCodes.put("370000", "山东省");
		areaCodes.put("370100", "山东省济南市");
		areaCodes.put("370101", "山东省济南市");
		areaCodes.put("370102", "山东省济南市历下区");
		areaCodes.put("370103", "山东省济南市市中区");
		areaCodes.put("370104", "山东省济南市槐荫区");
		areaCodes.put("370105", "山东省济南市天桥区");
		areaCodes.put("370112", "山东省济南市历城区");
		areaCodes.put("370113", "山东省济南市长清区");
		areaCodes.put("370124", "山东省济南市平阴县");
		areaCodes.put("370125", "山东省济南市济阳县");
		areaCodes.put("370126", "山东省济南市商河县");
		areaCodes.put("370181", "山东省济南市章丘市");
		areaCodes.put("370200", "山东省青岛市");
		areaCodes.put("370201", "山东省青岛市");
		areaCodes.put("370202", "山东省青岛市市南区");
		areaCodes.put("370203", "山东省青岛市市北区");
		areaCodes.put("370205", "山东省青岛市四方区");
		areaCodes.put("370211", "山东省青岛市黄岛区");
		areaCodes.put("370212", "山东省青岛市崂山区");
		areaCodes.put("370213", "山东省青岛市李沧区");
		areaCodes.put("370214", "山东省青岛市城阳区");
		areaCodes.put("370281", "山东省青岛市胶州市");
		areaCodes.put("370282", "山东省青岛市即墨市");
		areaCodes.put("370283", "山东省青岛市平度市");
		areaCodes.put("370284", "山东省青岛市胶南市");
		areaCodes.put("370285", "山东省青岛市莱西市");
		areaCodes.put("370300", "山东省淄博市");
		areaCodes.put("370301", "山东省淄博市");
		areaCodes.put("370302", "山东省淄博市淄川区");
		areaCodes.put("370883", "山东省济宁市邹城市");
		areaCodes.put("360423", "江西省九江市武宁县");
		areaCodes.put("360424", "江西省九江市修水县");
		areaCodes.put("360425", "江西省九江市永修县");
		areaCodes.put("360426", "江西省九江市德安县");
		areaCodes.put("360427", "江西省九江市星子县");
		areaCodes.put("360428", "江西省九江市都昌县");
		areaCodes.put("360429", "江西省九江市湖口县");
		areaCodes.put("360430", "江西省九江市彭泽县");
		areaCodes.put("360481", "江西省九江市瑞昌市");
		areaCodes.put("360500", "江西省新余市");
		areaCodes.put("360501", "江西省新余市");
		areaCodes.put("360502", "江西省新余市渝水区");
		areaCodes.put("360521", "江西省新余市分宜县");
		areaCodes.put("360600", "江西省鹰潭市");
		areaCodes.put("360601", "江西省鹰潭市");
		areaCodes.put("360602", "江西省鹰潭市月湖区");
		areaCodes.put("360622", "江西省鹰潭市余江县");
		areaCodes.put("360681", "江西省鹰潭市贵溪市");
		areaCodes.put("360700", "江西省赣州市");
		areaCodes.put("360701", "江西省赣州市");
		areaCodes.put("360702", "江西省赣州市章贡区");
		areaCodes.put("360721", "江西省赣州市赣县");
		areaCodes.put("360722", "江西省赣州市信丰县");
		areaCodes.put("360723", "江西省赣州市大余县");
		areaCodes.put("360724", "江西省赣州市上犹县");
		areaCodes.put("360725", "江西省赣州市崇义县");
		areaCodes.put("360726", "江西省赣州市安远县");
		areaCodes.put("360727", "江西省赣州市龙南县");
		areaCodes.put("360728", "江西省赣州市定南县");
		areaCodes.put("360729", "江西省赣州市全南县");
		areaCodes.put("360730", "江西省赣州市宁都县");
		areaCodes.put("360731", "江西省赣州市于都县");
		areaCodes.put("360732", "江西省赣州市兴国县");
		areaCodes.put("360733", "江西省赣州市会昌县");
		areaCodes.put("360734", "江西省赣州市寻乌县");
		areaCodes.put("360735", "江西省赣州市石城县");
		areaCodes.put("360781", "江西省赣州市瑞金市");
		areaCodes.put("360782", "江西省赣州市南康市");
		areaCodes.put("360800", "江西省吉安市");
		areaCodes.put("360801", "江西省吉安市");
		areaCodes.put("360802", "江西省吉安市吉州区");
		areaCodes.put("360803", "江西省吉安市青原区");
		areaCodes.put("360821", "江西省吉安市吉安县");
		areaCodes.put("360822", "江西省吉安市吉水县");
		areaCodes.put("360823", "江西省吉安市峡江县");
		areaCodes.put("360824", "江西省吉安市新干县");
		areaCodes.put("360825", "江西省吉安市永丰县");
		areaCodes.put("360826", "江西省吉安市泰和县");
		areaCodes.put("360827", "江西省吉安市遂川县");
		areaCodes.put("360828", "江西省吉安市万安县");
		areaCodes.put("360829", "江西省吉安市安福县");
		areaCodes.put("360830", "江西省吉安市永新县");
		areaCodes.put("360881", "江西省吉安市井冈山市");
		areaCodes.put("360900", "江西省宜春市");
		areaCodes.put("360901", "江西省宜春市");
		areaCodes.put("360902", "江西省宜春市袁州区");
		areaCodes.put("360921", "江西省宜春市奉新县");
		areaCodes.put("360922", "江西省宜春市万载县");
		areaCodes.put("360923", "江西省宜春市上高县");
		areaCodes.put("360924", "江西省宜春市宜丰县");
		areaCodes.put("371600", "山东省滨州市");
		areaCodes.put("360925", "江西省宜春市靖安县");
		areaCodes.put("410926", "河南省濮阳市范县");
		areaCodes.put("420503", "湖北省宜昌市伍家岗区");
		areaCodes.put("420504", "湖北省宜昌市点军区");
		areaCodes.put("420505", "湖北省宜昌市猇亭区");
		areaCodes.put("420506", "湖北省宜昌市夷陵区");
		areaCodes.put("420525", "湖北省宜昌市远安县");
		areaCodes.put("420526", "湖北省宜昌市兴山县");
		areaCodes.put("420527", "湖北省宜昌市秭归县");
		areaCodes.put("420528", "湖北省宜昌市长阳土家族自治县");
		areaCodes.put("420529", "湖北省宜昌市五峰土家族自治县");
		areaCodes.put("420581", "湖北省宜昌市宜都市");
		areaCodes.put("420582", "湖北省宜昌市当阳市");
		areaCodes.put("420583", "湖北省宜昌市枝江市");
		areaCodes.put("420600", "湖北省襄樊市");
		areaCodes.put("420601", "湖北省襄樊市");
		areaCodes.put("420602", "湖北省襄樊市襄城区");
		areaCodes.put("420606", "湖北省襄樊市樊城区");
		areaCodes.put("420607", "湖北省襄樊市襄阳区");
		areaCodes.put("420624", "湖北省襄樊市南漳县");
		areaCodes.put("420625", "湖北省襄樊市谷城县");
		areaCodes.put("420626", "湖北省襄樊市保康县");
		areaCodes.put("420682", "湖北省襄樊市老河口市");
		areaCodes.put("420683", "湖北省襄樊市枣阳市");
		areaCodes.put("420684", "湖北省襄樊市宜城市");
		areaCodes.put("420700", "湖北省鄂州市");
		areaCodes.put("420701", "湖北省鄂州市");
		areaCodes.put("420702", "湖北省鄂州市梁子湖区");
		areaCodes.put("420703", "湖北省鄂州市华容区");
		areaCodes.put("420704", "湖北省鄂州市鄂城区");
		areaCodes.put("420800", "湖北省荆门市");
		areaCodes.put("420801", "湖北省荆门市");
		areaCodes.put("420802", "湖北省荆门市东宝区");
		areaCodes.put("420804", "湖北省荆门市掇刀区");
		areaCodes.put("420821", "湖北省荆门市京山县");
		areaCodes.put("420822", "湖北省荆门市沙洋县");
		areaCodes.put("420881", "湖北省荆门市钟祥市");
		areaCodes.put("420900", "湖北省孝感市");
		areaCodes.put("420901", "湖北省孝感市");
		areaCodes.put("420902", "湖北省孝感市孝南区");
		areaCodes.put("420921", "湖北省孝感市孝昌县");
		areaCodes.put("420922", "湖北省孝感市大悟县");
		areaCodes.put("420923", "湖北省孝感市云梦县");
		areaCodes.put("420981", "湖北省孝感市应城市");
		areaCodes.put("420982", "湖北省孝感市安陆市");
		areaCodes.put("420984", "湖北省孝感市汉川市");
		areaCodes.put("421000", "湖北省荆州市");
		areaCodes.put("421001", "湖北省荆州市");
		areaCodes.put("421002", "湖北省荆州市沙市区");
		areaCodes.put("421003", "湖北省荆州市荆州区");
		areaCodes.put("421022", "湖北省荆州市公安县");
		areaCodes.put("421023", "湖北省荆州市监利县");
		areaCodes.put("421024", "湖北省荆州市江陵县");
		areaCodes.put("421081", "湖北省荆州市石首市");
		areaCodes.put("421083", "湖北省荆州市洪湖市");
		areaCodes.put("421087", "湖北省荆州市松滋市");
		areaCodes.put("421100", "湖北省黄冈市");
		areaCodes.put("421101", "湖北省黄冈市");
		areaCodes.put("421102", "湖北省黄冈市黄州区");
		areaCodes.put("421121", "湖北省黄冈市团风县");
		areaCodes.put("421122", "湖北省黄冈市红安县");
		areaCodes.put("430381", "湖南省湘潭市湘乡市");
		areaCodes.put("430922", "湖南省益阳市桃江县");
		areaCodes.put("430923", "湖南省益阳市安化县");
		areaCodes.put("430401", "湖南省衡阳市");
		areaCodes.put("430405", "湖南省衡阳市珠晖区");
		areaCodes.put("430406", "湖南省衡阳市雁峰区");
		areaCodes.put("430407", "湖南省衡阳市石鼓区");
		areaCodes.put("430408", "湖南省衡阳市蒸湘区");
		areaCodes.put("430412", "湖南省衡阳市南岳区");
		areaCodes.put("430421", "湖南省衡阳市衡阳县");
		areaCodes.put("430422", "湖南省衡阳市衡南县");
		areaCodes.put("430423", "湖南省衡阳市衡山县");
		areaCodes.put("430424", "湖南省衡阳市衡东县");
		areaCodes.put("430426", "湖南省衡阳市祁东县");
		areaCodes.put("430481", "湖南省衡阳市耒阳市");
		areaCodes.put("430482", "湖南省衡阳市常宁市");
		areaCodes.put("430500", "湖南省邵阳市");
		areaCodes.put("430501", "湖南省邵阳市");
		areaCodes.put("430502", "湖南省邵阳市双清区");
		areaCodes.put("430503", "湖南省邵阳市大祥区");
		areaCodes.put("430511", "湖南省邵阳市北塔区");
		areaCodes.put("430521", "湖南省邵阳市邵东县");
		areaCodes.put("430522", "湖南省邵阳市新邵县");
		areaCodes.put("430523", "湖南省邵阳市邵阳县");
		areaCodes.put("430524", "湖南省邵阳市隆回县");
		areaCodes.put("430525", "湖南省邵阳市洞口县");
		areaCodes.put("430527", "湖南省邵阳市绥宁县");
		areaCodes.put("430528", "湖南省邵阳市新宁县");
		areaCodes.put("430529", "湖南省邵阳市城步苗族自治县");
		areaCodes.put("430581", "湖南省邵阳市武冈市");
		areaCodes.put("430600", "湖南省岳阳市");
		areaCodes.put("430601", "湖南省岳阳市");
		areaCodes.put("430602", "湖南省岳阳市岳阳楼区");
		areaCodes.put("430603", "湖南省岳阳市云溪区");
		areaCodes.put("430611", "湖南省岳阳市君山区");
		areaCodes.put("430621", "湖南省岳阳市岳阳县");
		areaCodes.put("430623", "湖南省岳阳市华容县");
		areaCodes.put("430624", "湖南省岳阳市湘阴县");
		areaCodes.put("430626", "湖南省岳阳市平江县");
		areaCodes.put("430681", "湖南省岳阳市汨罗市");
		areaCodes.put("430682", "湖南省岳阳市临湘市");
		areaCodes.put("430700", "湖南省常德市");
		areaCodes.put("430701", "湖南省常德市");
		areaCodes.put("430702", "湖南省常德市武陵区");
		areaCodes.put("430703", "湖南省常德市鼎城区");
		areaCodes.put("430721", "湖南省常德市安乡县");
		areaCodes.put("430722", "湖南省常德市汉寿县");
		areaCodes.put("430723", "湖南省常德市澧县");
		areaCodes.put("430724", "湖南省常德市临澧县");
		areaCodes.put("430725", "湖南省常德市桃源县");
		areaCodes.put("430726", "湖南省常德市石门县");
		areaCodes.put("430781", "湖南省常德市津市市");
		areaCodes.put("430800", "湖南省张家界市");
		areaCodes.put("320723", "江苏省连云港市灌云县");
		areaCodes.put("320724", "江苏省连云港市灌南县");
		areaCodes.put("320800", "江苏省淮安市");
		areaCodes.put("320801", "江苏省淮安市");
		areaCodes.put("330781", "浙江省金华市兰溪市");
		areaCodes.put("230622", "黑龙江省大庆市肇源县");
		areaCodes.put("230623", "黑龙江省大庆市林甸县");
		areaCodes.put("230624", "黑龙江省大庆市杜尔伯特蒙古族自治县");
		areaCodes.put("230700", "黑龙江省伊春市");
		areaCodes.put("230701", "黑龙江省伊春市");
		areaCodes.put("230702", "黑龙江省伊春市伊春区");
		areaCodes.put("230703", "黑龙江省伊春市南岔区");
		areaCodes.put("230704", "黑龙江省伊春市友好区");
		areaCodes.put("230705", "黑龙江省伊春市西林区");
		areaCodes.put("230706", "黑龙江省伊春市翠峦区");
		areaCodes.put("230707", "黑龙江省伊春市新青区");
		areaCodes.put("230708", "黑龙江省伊春市美溪区");
		areaCodes.put("230709", "黑龙江省伊春市金山屯区");
		areaCodes.put("230710", "黑龙江省伊春市五营区");
		areaCodes.put("230711", "黑龙江省伊春市乌马河区");
		areaCodes.put("230712", "黑龙江省伊春市汤旺河区");
		areaCodes.put("230713", "黑龙江省伊春市带岭区");
		areaCodes.put("230714", "黑龙江省伊春市乌伊岭区");
		areaCodes.put("230715", "黑龙江省伊春市红星区");
		areaCodes.put("230716", "黑龙江省伊春市上甘岭区");
		areaCodes.put("230722", "黑龙江省伊春市嘉荫县");
		areaCodes.put("230781", "黑龙江省伊春市铁力市");
		areaCodes.put("230800", "黑龙江省佳木斯市");
		areaCodes.put("230801", "黑龙江省佳木斯市");
		areaCodes.put("230802", "黑龙江省佳木斯市永红区");
		areaCodes.put("230803", "黑龙江省佳木斯市向阳区");
		areaCodes.put("230804", "黑龙江省佳木斯市前进区");
		areaCodes.put("230805", "黑龙江省佳木斯市东风区");
		areaCodes.put("230811", "黑龙江省佳木斯市郊区");
		areaCodes.put("230822", "黑龙江省佳木斯市桦南县");
		areaCodes.put("230826", "黑龙江省佳木斯市桦川县");
		areaCodes.put("230828", "黑龙江省佳木斯市汤原县");
		areaCodes.put("230833", "黑龙江省佳木斯市抚远县");
		areaCodes.put("230881", "黑龙江省佳木斯市同江市");
		areaCodes.put("230882", "黑龙江省佳木斯市富锦市");
		areaCodes.put("230900", "黑龙江省七台河市");
		areaCodes.put("230901", "黑龙江省七台河市");
		areaCodes.put("230902", "黑龙江省七台河市新兴区");
		areaCodes.put("230903", "黑龙江省七台河市桃山区");
		areaCodes.put("230904", "黑龙江省七台河市茄子河区");
		areaCodes.put("230921", "黑龙江省七台河市勃利县");
		areaCodes.put("231000", "黑龙江省牡丹江市");
		areaCodes.put("231001", "黑龙江省牡丹江市");
		areaCodes.put("231002", "黑龙江省牡丹江市东安区");
		areaCodes.put("231003", "黑龙江省牡丹江市阳明区");
		areaCodes.put("231004", "黑龙江省牡丹江市爱民区");
		areaCodes.put("231005", "黑龙江省牡丹江市西安区");
		areaCodes.put("231024", "黑龙江省牡丹江市东宁县");
		areaCodes.put("231025", "黑龙江省牡丹江市林口县");
		areaCodes.put("231081", "黑龙江省牡丹江市绥芬河市");
		areaCodes.put("231083", "黑龙江省牡丹江市海林市");
		areaCodes.put("231084", "黑龙江省牡丹江市宁安市");
		areaCodes.put("231085", "黑龙江省牡丹江市穆棱市");
		areaCodes.put("231100", "黑龙江省黑河市");
		areaCodes.put("231101", "黑龙江省黑河市");
		areaCodes.put("231102", "黑龙江省黑河市爱辉区");
		areaCodes.put("231121", "黑龙江省黑河市嫩江县");
		areaCodes.put("360403", "江西省九江市浔阳区");
		areaCodes.put("231124", "黑龙江省黑河市孙吴县");
		areaCodes.put("231181", "黑龙江省黑河市北安市");
		areaCodes.put("231182", "黑龙江省黑河市五大连池市");
		areaCodes.put("231200", "黑龙江省绥化市");
		areaCodes.put("231201", "黑龙江省绥化市");
		areaCodes.put("231202", "黑龙江省绥化市北林区");
		areaCodes.put("231221", "黑龙江省绥化市望奎县");
		areaCodes.put("231222", "黑龙江省绥化市兰西县");
		areaCodes.put("231223", "黑龙江省绥化市青冈县");
		areaCodes.put("231224", "黑龙江省绥化市庆安县");
		areaCodes.put("231225", "黑龙江省绥化市明水县");
		areaCodes.put("231226", "黑龙江省绥化市绥棱县");
		areaCodes.put("231281", "黑龙江省绥化市安达市");
		areaCodes.put("231282", "黑龙江省绥化市肇东市");
		areaCodes.put("231283", "黑龙江省绥化市海伦市");
		areaCodes.put("232700", "黑龙江省大兴安岭地区");
		areaCodes.put("232721", "黑龙江省大兴安岭地区呼玛县");
		areaCodes.put("232722", "黑龙江省大兴安岭地区塔河县");
		areaCodes.put("232723", "黑龙江省大兴安岭地区漠河县");
		areaCodes.put("310000", "上海市");
		areaCodes.put("310100", "上海市");
		areaCodes.put("310101", "上海市黄浦区");
		areaCodes.put("310103", "上海市卢湾区");
		areaCodes.put("310104", "上海市徐汇区");
		areaCodes.put("310105", "上海市长宁区");
		areaCodes.put("310106", "上海市静安区");
		areaCodes.put("310107", "上海市普陀区");
		areaCodes.put("310108", "上海市闸北区");
		areaCodes.put("310109", "上海市虹口区");
		areaCodes.put("310110", "上海市杨浦区");
		areaCodes.put("310112", "上海市闵行区");
		areaCodes.put("310113", "上海市宝山区");
		areaCodes.put("310114", "上海市嘉定区");
		areaCodes.put("310115", "上海市浦东新区");
		areaCodes.put("310116", "上海市金山区");
		areaCodes.put("310117", "上海市松江区");
		areaCodes.put("310118", "上海市青浦区");
		areaCodes.put("310119", "上海市南汇区");
		areaCodes.put("310120", "上海市奉贤区");
		areaCodes.put("310200", "上海市");
		areaCodes.put("310230", "上海市崇明县");
		areaCodes.put("320000", "江苏省");
		areaCodes.put("429021", "湖北省神农架林区");
		areaCodes.put("430000", "湖南省");
		areaCodes.put("430100", "湖南省长沙市");
		areaCodes.put("430101", "湖南省长沙市");
		areaCodes.put("430102", "湖南省长沙市芙蓉区");
		areaCodes.put("430103", "湖南省长沙市天心区");
		areaCodes.put("430104", "湖南省长沙市岳麓区");
		areaCodes.put("430105", "湖南省长沙市开福区");
		areaCodes.put("430111", "湖南省长沙市雨花区");
		areaCodes.put("430121", "湖南省长沙市长沙县");
		areaCodes.put("430122", "湖南省长沙市望城县");
		areaCodes.put("430124", "湖南省长沙市宁乡县");
		areaCodes.put("430181", "湖南省长沙市浏阳市");
		areaCodes.put("430200", "湖南省株洲市");
		areaCodes.put("430201", "湖南省株洲市");
		areaCodes.put("430202", "湖南省株洲市荷塘区");
		areaCodes.put("430203", "湖南省株洲市芦淞区");
		areaCodes.put("430204", "湖南省株洲市石峰区");
		areaCodes.put("430211", "湖南省株洲市天元区");
		areaCodes.put("430221", "湖南省株洲市株洲县");
		areaCodes.put("430223", "湖南省株洲市攸县");
		areaCodes.put("430224", "湖南省株洲市茶陵县");
		areaCodes.put("430225", "湖南省株洲市炎陵县");
		areaCodes.put("430281", "湖南省株洲市醴陵市");
		areaCodes.put("430300", "湖南省湘潭市");
		areaCodes.put("430301", "湖南省湘潭市");
		areaCodes.put("430302", "湖南省湘潭市雨湖区");
		areaCodes.put("430304", "湖南省湘潭市岳塘区");
		areaCodes.put("430321", "湖南省湘潭市湘潭县");
		areaCodes.put("420500", "湖北省宜昌市");
		areaCodes.put("411525", "河南省信阳市固始县");
		areaCodes.put("411526", "河南省信阳市潢川县");
		areaCodes.put("411527", "河南省信阳市淮滨县");
		areaCodes.put("411528", "河南省信阳市息县");
		areaCodes.put("411600", "河南省周口市");
		areaCodes.put("411601", "河南省周口市");
		areaCodes.put("411602", "河南省周口市川汇区");
		areaCodes.put("411621", "河南省周口市扶沟县");
		areaCodes.put("411622", "河南省周口市西华县");
		areaCodes.put("411623", "河南省周口市商水县");
		areaCodes.put("411624", "河南省周口市沈丘县");
		areaCodes.put("411625", "河南省周口市郸城县");
		areaCodes.put("411626", "河南省周口市淮阳县");
		areaCodes.put("411627", "河南省周口市太康县");
		areaCodes.put("411628", "河南省周口市鹿邑县");
		areaCodes.put("411681", "河南省周口市项城市");
		areaCodes.put("411700", "河南省驻马店市");
		areaCodes.put("411701", "河南省驻马店市");
		areaCodes.put("411702", "河南省驻马店市驿城区");
		areaCodes.put("411721", "河南省驻马店市西平县");
		areaCodes.put("411722", "河南省驻马店市上蔡县");
		areaCodes.put("411723", "河南省驻马店市平舆县");
		areaCodes.put("411724", "河南省驻马店市正阳县");
		areaCodes.put("411725", "河南省驻马店市确山县");
		areaCodes.put("411726", "河南省驻马店市泌阳县");
		areaCodes.put("411727", "河南省驻马店市汝南县");
		areaCodes.put("411728", "河南省驻马店市遂平县");
		areaCodes.put("411729", "河南省驻马店市新蔡县");
		areaCodes.put("420000", "湖北省");
		areaCodes.put("420100", "湖北省武汉市");
		areaCodes.put("420101", "湖北省武汉市");
		areaCodes.put("420102", "湖北省武汉市江岸区");
		areaCodes.put("420103", "湖北省武汉市江汉区");
		areaCodes.put("420104", "湖北省武汉市乔口区");
		areaCodes.put("420105", "湖北省武汉市汉阳区");
		areaCodes.put("420106", "湖北省武汉市武昌区");
		areaCodes.put("420107", "湖北省武汉市青山区");
		areaCodes.put("420111", "湖北省武汉市洪山区");
		areaCodes.put("420112", "湖北省武汉市东西湖区");
		areaCodes.put("420113", "湖北省武汉市汉南区");
		areaCodes.put("420114", "湖北省武汉市蔡甸区");
		areaCodes.put("420115", "湖北省武汉市江夏区");
		areaCodes.put("420116", "湖北省武汉市黄陂区");
		areaCodes.put("420117", "湖北省武汉市新洲区");
		areaCodes.put("420200", "湖北省黄石市");
		areaCodes.put("420201", "湖北省黄石市");
		areaCodes.put("420202", "湖北省黄石市黄石港区");
		areaCodes.put("420203", "湖北省黄石市西塞山区");
		areaCodes.put("420204", "湖北省黄石市下陆区");
		areaCodes.put("420205", "湖北省黄石市铁山区");
		areaCodes.put("420222", "湖北省黄石市阳新县");
		areaCodes.put("420281", "湖北省黄石市大冶市");
		areaCodes.put("420300", "湖北省十堰市");
		areaCodes.put("420301", "湖北省十堰市");
		areaCodes.put("420302", "湖北省十堰市茅箭区");
		areaCodes.put("420303", "湖北省十堰市张湾区");
		areaCodes.put("420321", "湖北省十堰市郧县");
		areaCodes.put("420322", "湖北省十堰市郧西县");
		areaCodes.put("420323", "湖北省十堰市竹山县");
		areaCodes.put("420324", "湖北省十堰市竹溪县");
		areaCodes.put("420325", "湖北省十堰市房县");
		areaCodes.put("420381", "湖北省十堰市丹江口市");
		areaCodes.put("420501", "湖北省宜昌市");
		areaCodes.put("430400", "湖南省衡阳市");
		areaCodes.put("410401", "河南省平顶山市");
		areaCodes.put("410402", "河南省平顶山市新华区");
		areaCodes.put("410403", "河南省平顶山市卫东区");
		areaCodes.put("410404", "河南省平顶山市石龙区");
		areaCodes.put("410411", "河南省平顶山市湛河区");
		areaCodes.put("410421", "河南省平顶山市宝丰县");
		areaCodes.put("410422", "河南省平顶山市叶县");
		areaCodes.put("410423", "河南省平顶山市鲁山县");
		areaCodes.put("410425", "河南省平顶山市郏县");
		areaCodes.put("410481", "河南省平顶山市舞钢市");
		areaCodes.put("410482", "河南省平顶山市汝州市");
		areaCodes.put("410500", "河南省安阳市");
		areaCodes.put("410501", "河南省安阳市");
		areaCodes.put("410502", "河南省安阳市文峰区");
		areaCodes.put("410503", "河南省安阳市北关区");
		areaCodes.put("410505", "河南省安阳市殷都区");
		areaCodes.put("410506", "河南省安阳市龙安区");
		areaCodes.put("410522", "河南省安阳市安阳县");
		areaCodes.put("410523", "河南省安阳市汤阴县");
		areaCodes.put("410526", "河南省安阳市滑县");
		areaCodes.put("410527", "河南省安阳市内黄县");
		areaCodes.put("410581", "河南省安阳市林州市");
		areaCodes.put("410600", "河南省鹤壁市");
		areaCodes.put("410601", "河南省鹤壁市");
		areaCodes.put("410602", "河南省鹤壁市鹤山区");
		areaCodes.put("410603", "河南省鹤壁市山城区");
		areaCodes.put("410611", "河南省鹤壁市淇滨区");
		areaCodes.put("410621", "河南省鹤壁市浚县");
		areaCodes.put("410622", "河南省鹤壁市淇县");
		areaCodes.put("410700", "河南省新乡市");
		areaCodes.put("410701", "河南省新乡市");
		areaCodes.put("410702", "河南省新乡市红旗区");
		areaCodes.put("410703", "河南省新乡市卫滨区");
		areaCodes.put("410704", "河南省新乡市凤泉区");
		areaCodes.put("410711", "河南省新乡市牧野区");
		areaCodes.put("410721", "河南省新乡市新乡县");
		areaCodes.put("410724", "河南省新乡市获嘉县");
		areaCodes.put("410725", "河南省新乡市原阳县");
		areaCodes.put("410726", "河南省新乡市延津县");
		areaCodes.put("410727", "河南省新乡市封丘县");
		areaCodes.put("410728", "河南省新乡市长垣县");
		areaCodes.put("410781", "河南省新乡市卫辉市");
		areaCodes.put("410782", "河南省新乡市辉县市");
		areaCodes.put("410800", "河南省焦作市");
		areaCodes.put("410801", "河南省焦作市");
		areaCodes.put("410802", "河南省焦作市解放区");
		areaCodes.put("410803", "河南省焦作市中站区");
		areaCodes.put("410804", "河南省焦作市马村区");
		areaCodes.put("410811", "河南省焦作市山阳区");
		areaCodes.put("410821", "河南省焦作市修武县");
		areaCodes.put("410822", "河南省焦作市博爱县");
		areaCodes.put("410823", "河南省焦作市武陟县");
		areaCodes.put("410825", "河南省焦作市温县");
		areaCodes.put("410881", "河南省焦作市济源市");
		areaCodes.put("410882", "河南省焦作市沁阳市");
		areaCodes.put("410883", "河南省焦作市孟州市");
		areaCodes.put("410900", "河南省濮阳市");
		areaCodes.put("410901", "河南省濮阳市");
		areaCodes.put("410902", "河南省濮阳市华龙区");
		areaCodes.put("410922", "河南省濮阳市清丰县");
		areaCodes.put("430382", "湖南省湘潭市韶山市");
		areaCodes.put("421124", "湖北省黄冈市英山县");
		areaCodes.put("410927", "河南省濮阳市台前县");
		areaCodes.put("410928", "河南省濮阳市濮阳县");
		areaCodes.put("411000", "河南省许昌市");
		areaCodes.put("411001", "河南省许昌市");
		areaCodes.put("411002", "河南省许昌市魏都区");
		areaCodes.put("411023", "河南省许昌市许昌县");
		areaCodes.put("411024", "河南省许昌市鄢陵县");
		areaCodes.put("411025", "河南省许昌市襄城县");
		areaCodes.put("411081", "河南省许昌市禹州市");
		areaCodes.put("411082", "河南省许昌市长葛市");
		areaCodes.put("411100", "河南省漯河市");
		areaCodes.put("411101", "河南省漯河市");
		areaCodes.put("411102", "河南省漯河市源汇区");
		areaCodes.put("411103", "河南省漯河市郾城区");
		areaCodes.put("411104", "河南省漯河市召陵区");
		areaCodes.put("411121", "河南省漯河市舞阳县");
		areaCodes.put("411122", "河南省漯河市临颍县");
		areaCodes.put("411200", "河南省三门峡市");
		areaCodes.put("411201", "河南省三门峡市");
		areaCodes.put("411202", "河南省三门峡市湖滨区");
		areaCodes.put("411221", "河南省三门峡市渑池县");
		areaCodes.put("411222", "河南省三门峡市陕县");
		areaCodes.put("411224", "河南省三门峡市卢氏县");
		areaCodes.put("411281", "河南省三门峡市义马市");
		areaCodes.put("411282", "河南省三门峡市灵宝市");
		areaCodes.put("411300", "河南省南阳市");
		areaCodes.put("411301", "河南省南阳市");
		areaCodes.put("411302", "河南省南阳市宛城区");
		areaCodes.put("411303", "河南省南阳市卧龙区");
		areaCodes.put("411321", "河南省南阳市南召县");
		areaCodes.put("411322", "河南省南阳市方城县");
		areaCodes.put("411323", "河南省南阳市西峡县");
		areaCodes.put("411324", "河南省南阳市镇平县");
		areaCodes.put("411325", "河南省南阳市内乡县");
		areaCodes.put("411326", "河南省南阳市淅川县");
		areaCodes.put("411327", "河南省南阳市社旗县");
		areaCodes.put("411328", "河南省南阳市唐河县");
		areaCodes.put("411329", "河南省南阳市新野县");
		areaCodes.put("411330", "河南省南阳市桐柏县");
		areaCodes.put("411381", "河南省南阳市邓州市");
		areaCodes.put("411400", "河南省商丘市");
		areaCodes.put("411401", "河南省商丘市");
		areaCodes.put("411402", "河南省商丘市梁园区");
		areaCodes.put("411403", "河南省商丘市睢阳区");
		areaCodes.put("411421", "河南省商丘市民权县");
		areaCodes.put("411422", "河南省商丘市睢县");
		areaCodes.put("411423", "河南省商丘市宁陵县");
		areaCodes.put("411424", "河南省商丘市柘城县");
		areaCodes.put("411425", "河南省商丘市虞城县");
		areaCodes.put("411426", "河南省商丘市夏邑县");
		areaCodes.put("411481", "河南省商丘市永城市");
		areaCodes.put("411500", "河南省信阳市");
		areaCodes.put("411501", "河南省信阳市");
		areaCodes.put("411502", "河南省信阳市师河区");
		areaCodes.put("411503", "河南省信阳市平桥区");
		areaCodes.put("411521", "河南省信阳市罗山县");
		areaCodes.put("411522", "河南省信阳市光山县");
		areaCodes.put("411523", "河南省信阳市新县");
		areaCodes.put("411524", "河南省信阳市商城县");
		areaCodes.put("440100", "广东省广州市");
		areaCodes.put("440101", "广东省广州市");
		areaCodes.put("410381", "河南省洛阳市偃师市");
		areaCodes.put("410400", "河南省平顶山市");
		areaCodes.put("510704", "四川省绵阳市游仙区");
		areaCodes.put("510722", "四川省绵阳市三台县");
		areaCodes.put("510723", "四川省绵阳市盐亭县");
		areaCodes.put("510724", "四川省绵阳市安县");
		areaCodes.put("510725", "四川省绵阳市梓潼县");
		areaCodes.put("510726", "四川省绵阳市北川羌族自治县");
		areaCodes.put("510727", "四川省绵阳市平武县");
		areaCodes.put("510781", "四川省绵阳市江油市");
		areaCodes.put("510800", "四川省广元市");
		areaCodes.put("510801", "四川省广元市");
		areaCodes.put("510802", "四川省广元市市中区");
		areaCodes.put("510811", "四川省广元市元坝区");
		areaCodes.put("510812", "四川省广元市朝天区");
		areaCodes.put("510821", "四川省广元市旺苍县");
		areaCodes.put("510822", "四川省广元市青川县");
		areaCodes.put("510823", "四川省广元市剑阁县");
		areaCodes.put("510824", "四川省广元市苍溪县");
		areaCodes.put("510900", "四川省遂宁市");
		areaCodes.put("510901", "四川省遂宁市");
		areaCodes.put("510903", "四川省遂宁市船山区");
		areaCodes.put("510904", "四川省遂宁市安居区");
		areaCodes.put("510921", "四川省遂宁市蓬溪县");
		areaCodes.put("510922", "四川省遂宁市射洪县");
		areaCodes.put("510923", "四川省遂宁市大英县");
		areaCodes.put("511000", "四川省内江市");
		areaCodes.put("511001", "四川省内江市");
		areaCodes.put("511002", "四川省内江市市中区");
		areaCodes.put("511011", "四川省内江市东兴区");
		areaCodes.put("511024", "四川省内江市威远县");
		areaCodes.put("511025", "四川省内江市资中县");
		areaCodes.put("511028", "四川省内江市隆昌县");
		areaCodes.put("511100", "四川省乐山市");
		areaCodes.put("511101", "四川省乐山市");
		areaCodes.put("511102", "四川省乐山市市中区");
		areaCodes.put("511111", "四川省乐山市沙湾区");
		areaCodes.put("511112", "四川省乐山市五通桥区");
		areaCodes.put("511113", "四川省乐山市金口河区");
		areaCodes.put("511123", "四川省乐山市犍为县");
		areaCodes.put("511124", "四川省乐山市井研县");
		areaCodes.put("511126", "四川省乐山市夹江县");
		areaCodes.put("511129", "四川省乐山市沐川县");
		areaCodes.put("511132", "四川省乐山市峨边彝族自治县");
		areaCodes.put("511133", "四川省乐山市马边彝族自治县");
		areaCodes.put("511181", "四川省乐山市峨眉山市");
		areaCodes.put("511300", "四川省南充市");
		areaCodes.put("370826", "山东省济宁市微山县");
		areaCodes.put("370827", "山东省济宁市鱼台县");
		areaCodes.put("370828", "山东省济宁市金乡县");
		areaCodes.put("370829", "山东省济宁市嘉祥县");
		areaCodes.put("370830", "山东省济宁市汶上县");
		areaCodes.put("370831", "山东省济宁市泗水县");
		areaCodes.put("370832", "山东省济宁市梁山县");
		areaCodes.put("370881", "山东省济宁市曲阜市");
		areaCodes.put("350111", "福建省福州市晋安区");
		areaCodes.put("350121", "福建省福州市闽侯县");
		areaCodes.put("350122", "福建省福州市连江县");
		areaCodes.put("350123", "福建省福州市罗源县");
		areaCodes.put("350124", "福建省福州市闽清县");
		areaCodes.put("350125", "福建省福州市永泰县");
		areaCodes.put("350128", "福建省福州市平潭县");
		areaCodes.put("350181", "福建省福州市福清市");
		areaCodes.put("350182", "福建省福州市长乐市");
		areaCodes.put("350200", "福建省厦门市");
		areaCodes.put("350201", "福建省厦门市");
		areaCodes.put("350203", "福建省厦门市思明区");
		areaCodes.put("350205", "福建省厦门市海沧区");
		areaCodes.put("350206", "福建省厦门市湖里区");
		areaCodes.put("350211", "福建省厦门市集美区");
		areaCodes.put("350212", "福建省厦门市同安区");
		areaCodes.put("350213", "福建省厦门市翔安区");
		areaCodes.put("350300", "福建省莆田市");
		areaCodes.put("350301", "福建省莆田市");
		areaCodes.put("350302", "福建省莆田市城厢区");
		areaCodes.put("350303", "福建省莆田市涵江区");
		areaCodes.put("350304", "福建省莆田市荔城区");
		areaCodes.put("350305", "福建省莆田市秀屿区");
		areaCodes.put("350322", "福建省莆田市仙游县");
		areaCodes.put("350400", "福建省三明市");
		areaCodes.put("350401", "福建省三明市");
		areaCodes.put("350402", "福建省三明市梅列区");
		areaCodes.put("350403", "福建省三明市三元区");
		areaCodes.put("350421", "福建省三明市明溪县");
		areaCodes.put("350423", "福建省三明市清流县");
		areaCodes.put("350424", "福建省三明市宁化县");
		areaCodes.put("350425", "福建省三明市大田县");
		areaCodes.put("350426", "福建省三明市尤溪县");
		areaCodes.put("350427", "福建省三明市沙县");
		areaCodes.put("350428", "福建省三明市将乐县");
		areaCodes.put("350429", "福建省三明市泰宁县");
		areaCodes.put("350430", "福建省三明市建宁县");
		areaCodes.put("350481", "福建省三明市永安市");
		areaCodes.put("350500", "福建省泉州市");
		areaCodes.put("350501", "福建省泉州市");
		areaCodes.put("350502", "福建省泉州市鲤城区");
		areaCodes.put("350503", "福建省泉州市丰泽区");
		areaCodes.put("350504", "福建省泉州市洛江区");
		areaCodes.put("350505", "福建省泉州市泉港区");
		areaCodes.put("350521", "福建省泉州市惠安县");
		areaCodes.put("350524", "福建省泉州市安溪县");
		areaCodes.put("350525", "福建省泉州市永春县");
		areaCodes.put("350526", "福建省泉州市德化县");
		areaCodes.put("350527", "福建省泉州市金门县");
		areaCodes.put("350581", "福建省泉州市石狮市");
		areaCodes.put("350582", "福建省泉州市晋江市");
		areaCodes.put("350583", "福建省泉州市南安市");
		areaCodes.put("350600", "福建省漳州市");
		areaCodes.put("350601", "福建省漳州市");
		areaCodes.put("350602", "福建省漳州市芗城区");
		areaCodes.put("350603", "福建省漳州市龙文区");
		areaCodes.put("350622", "福建省漳州市云霄县");
		areaCodes.put("350623", "福建省漳州市漳浦县");
		areaCodes.put("350624", "福建省漳州市诏安县");
		areaCodes.put("350625", "福建省漳州市长泰县");
		areaCodes.put("350626", "福建省漳州市东山县");
		areaCodes.put("350627", "福建省漳州市南靖县");
		areaCodes.put("341200", "安徽省阜阳市");
		areaCodes.put("341201", "安徽省阜阳市");
		areaCodes.put("341202", "安徽省阜阳市颍州区");
		areaCodes.put("341203", "安徽省阜阳市颍东区");
		areaCodes.put("341204", "安徽省阜阳市颍泉区");
		areaCodes.put("341221", "安徽省阜阳市临泉县");
		areaCodes.put("341222", "安徽省阜阳市太和县");
		areaCodes.put("341225", "安徽省阜阳市阜南县");
		areaCodes.put("341226", "安徽省阜阳市颍上县");
		areaCodes.put("341282", "安徽省阜阳市界首市");
		areaCodes.put("341300", "安徽省宿州市");
		areaCodes.put("341301", "安徽省宿州市");
		areaCodes.put("341302", "安徽省宿州市墉桥区");
		areaCodes.put("341321", "安徽省宿州市砀山县");
		areaCodes.put("341322", "安徽省宿州市萧县");
		areaCodes.put("341323", "安徽省宿州市灵璧县");
		areaCodes.put("341324", "安徽省宿州市泗县");
		areaCodes.put("341400", "安徽省巢湖市");
		areaCodes.put("341401", "安徽省巢湖市");
		areaCodes.put("341402", "安徽省巢湖市居巢区");
		areaCodes.put("341421", "安徽省巢湖市庐江县");
		areaCodes.put("341422", "安徽省巢湖市无为县");
		areaCodes.put("341423", "安徽省巢湖市含山县");
		areaCodes.put("341424", "安徽省巢湖市和县");
		areaCodes.put("341500", "安徽省六安市");
		areaCodes.put("341501", "安徽省六安市");
		areaCodes.put("341502", "安徽省六安市金安区");
		areaCodes.put("341503", "安徽省六安市裕安区");
		areaCodes.put("341521", "安徽省六安市寿县");
		areaCodes.put("341522", "安徽省六安市霍邱县");
		areaCodes.put("341523", "安徽省六安市舒城县");
		areaCodes.put("341524", "安徽省六安市金寨县");
		areaCodes.put("341525", "安徽省六安市霍山县");
		areaCodes.put("341600", "安徽省亳州市");
		areaCodes.put("341601", "安徽省亳州市");
		areaCodes.put("341602", "安徽省亳州市谯城区");
		areaCodes.put("445301", "广东省云浮市");
		areaCodes.put("445302", "广东省云浮市云城区");
		areaCodes.put("445321", "广东省云浮市新兴县");
		areaCodes.put("445322", "广东省云浮市郁南县");
		areaCodes.put("445323", "广东省云浮市云安县");
		areaCodes.put("445381", "广东省云浮市罗定市");
		areaCodes.put("450000", "广西壮族自治区");
		areaCodes.put("450100", "广西壮族自治区南宁市");
		areaCodes.put("450101", "广西壮族自治区南宁市");
		areaCodes.put("450102", "广西壮族自治区南宁市兴宁区");
		areaCodes.put("450103", "广西壮族自治区南宁市青秀区");
		areaCodes.put("450105", "广西壮族自治区南宁市江南区");
		areaCodes.put("450107", "广西壮族自治区南宁市西乡塘区");
		areaCodes.put("450108", "广西壮族自治区南宁市良庆区");
		areaCodes.put("450109", "广西壮族自治区南宁市邕宁区");
		areaCodes.put("450122", "广西壮族自治区南宁市武鸣县");
		areaCodes.put("450123", "广西壮族自治区南宁市隆安县");
		areaCodes.put("450124", "广西壮族自治区南宁市马山县");
		areaCodes.put("450125", "广西壮族自治区南宁市上林县");
		areaCodes.put("450126", "广西壮族自治区南宁市宾阳县");
		areaCodes.put("450127", "广西壮族自治区南宁市横县");
		areaCodes.put("450200", "广西壮族自治区柳州市");
		areaCodes.put("450201", "广西壮族自治区柳州市");
		areaCodes.put("450202", "广西壮族自治区柳州市城中区");
		areaCodes.put("450203", "广西壮族自治区柳州市鱼峰区");
		areaCodes.put("450204", "广西壮族自治区柳州市柳南区");
		areaCodes.put("450205", "广西壮族自治区柳州市柳北区");
		areaCodes.put("450221", "广西壮族自治区柳州市柳江县");
		areaCodes.put("450222", "广西壮族自治区柳州市柳城县");
		areaCodes.put("450223", "广西壮族自治区柳州市鹿寨县");
		areaCodes.put("460105", "海南省海口市秀英区");
		areaCodes.put("522625", "贵州省黔东南苗族侗族自治州镇远县");
		areaCodes.put("460107", "海南省海口市琼山区");
		areaCodes.put("460108", "海南省海口市美兰区");
		areaCodes.put("460200", "海南省三亚市");
		areaCodes.put("460201", "海南省三亚市");
		areaCodes.put("469000", "海南省省直辖县级行政单位");
		areaCodes.put("469001", "海南省五指山市");
		areaCodes.put("469002", "海南省琼海市");
		areaCodes.put("469003", "海南省儋州市");
		areaCodes.put("469005", "海南省文昌市");
		areaCodes.put("469006", "海南省万宁市");
		areaCodes.put("469007", "海南省东方市");
		areaCodes.put("469025", "海南省定安县");
		areaCodes.put("469026", "海南省屯昌县");
		areaCodes.put("469027", "海南省澄迈县");
		areaCodes.put("469028", "海南省临高县");
		areaCodes.put("469030", "海南省白沙黎族自治县");
		areaCodes.put("469031", "海南省昌江黎族自治县");
		areaCodes.put("469033", "海南省乐东黎族自治县");
		areaCodes.put("469034", "海南省陵水黎族自治县");
		areaCodes.put("469035", "海南省保亭黎族苗族自治县");
		areaCodes.put("469036", "海南省琼中黎族苗族自治县");
		areaCodes.put("469037", "海南省西沙群岛");
		areaCodes.put("469038", "海南省南沙群岛");
		areaCodes.put("469039", "海南省中沙群岛的岛礁及其海域");
		areaCodes.put("500000", "重庆市");
		areaCodes.put("500100", "重庆市");
		areaCodes.put("500101", "重庆市万州区");
		areaCodes.put("500102", "重庆市涪陵区");
		areaCodes.put("500103", "重庆市渝中区");
		areaCodes.put("500104", "重庆市大渡口区");
		areaCodes.put("500105", "重庆市江北区");
		areaCodes.put("500106", "重庆市沙坪坝区");
		areaCodes.put("500107", "重庆市九龙坡区");
		areaCodes.put("500108", "重庆市南岸区");
		areaCodes.put("500109", "重庆市北碚区");
		areaCodes.put("500110", "重庆市万盛区");
		areaCodes.put("500111", "重庆市双桥区");
		areaCodes.put("500112", "重庆市渝北区");
		areaCodes.put("500113", "重庆市巴南区");
		areaCodes.put("500114", "重庆市黔江区");
		areaCodes.put("500115", "重庆市长寿区");
		areaCodes.put("500200", "重庆市");
		areaCodes.put("500222", "重庆市綦江县");
		areaCodes.put("500223", "重庆市潼南县");
		areaCodes.put("500224", "重庆市铜梁县");
		areaCodes.put("500225", "重庆市大足县");
		areaCodes.put("500226", "重庆市荣昌县");
		areaCodes.put("500227", "重庆市璧山县");
		areaCodes.put("500228", "重庆市梁平县");
		areaCodes.put("500229", "重庆市城口县");
		areaCodes.put("500230", "重庆市丰都县");
		areaCodes.put("500231", "重庆市垫江县");
		areaCodes.put("500232", "重庆市武隆县");
		areaCodes.put("500233", "重庆市忠县");
		areaCodes.put("500234", "重庆市开县");
		areaCodes.put("500235", "重庆市云阳县");
		areaCodes.put("500236", "重庆市奉节县");
		areaCodes.put("500237", "重庆市巫山县");
		areaCodes.put("500238", "重庆市巫溪县");
		areaCodes.put("500240", "重庆市石柱土家族自治县");
		areaCodes.put("440783", "广东省江门市开平市");
		areaCodes.put("450901", "广西壮族自治区玉林市");
		areaCodes.put("440785", "广东省江门市恩平市");
		areaCodes.put("440800", "广东省湛江市");
		areaCodes.put("440801", "广东省湛江市");
		areaCodes.put("440802", "广东省湛江市赤坎区");
		areaCodes.put("440803", "广东省湛江市霞山区");
		areaCodes.put("440804", "广东省湛江市坡头区");
		areaCodes.put("440811", "广东省湛江市麻章区");
		areaCodes.put("440823", "广东省湛江市遂溪县");
		areaCodes.put("440825", "广东省湛江市徐闻县");
		areaCodes.put("440881", "广东省湛江市廉江市");
		areaCodes.put("440882", "广东省湛江市雷州市");
		areaCodes.put("440883", "广东省湛江市吴川市");
		areaCodes.put("440900", "广东省茂名市");
		areaCodes.put("440901", "广东省茂名市");
		areaCodes.put("440902", "广东省茂名市茂南区");
		areaCodes.put("440903", "广东省茂名市茂港区");
		areaCodes.put("440923", "广东省茂名市电白县");
		areaCodes.put("440981", "广东省茂名市高州市");
		areaCodes.put("440982", "广东省茂名市化州市");
		areaCodes.put("440983", "广东省茂名市信宜市");
		areaCodes.put("441200", "广东省肇庆市");
		areaCodes.put("441201", "广东省肇庆市");
		areaCodes.put("441202", "广东省肇庆市端州区");
		areaCodes.put("441203", "广东省肇庆市鼎湖区");
		areaCodes.put("441223", "广东省肇庆市广宁县");
		areaCodes.put("441224", "广东省肇庆市怀集县");
		areaCodes.put("441225", "广东省肇庆市封开县");
		areaCodes.put("441226", "广东省肇庆市德庆县");
		areaCodes.put("441283", "广东省肇庆市高要市");
		areaCodes.put("441284", "广东省肇庆市四会市");
		areaCodes.put("441300", "广东省惠州市");
		areaCodes.put("441301", "广东省惠州市");
		areaCodes.put("441302", "广东省惠州市惠城区");
		areaCodes.put("441303", "广东省惠州市惠阳区");
		areaCodes.put("441322", "广东省惠州市博罗县");
		areaCodes.put("441323", "广东省惠州市惠东县");
		areaCodes.put("441324", "广东省惠州市龙门县");
		areaCodes.put("441400", "广东省梅州市");
		areaCodes.put("441401", "广东省梅州市");
		areaCodes.put("441402", "广东省梅州市梅江区");
		areaCodes.put("441421", "广东省梅州市梅县");
		areaCodes.put("441422", "广东省梅州市大埔县");
		areaCodes.put("441423", "广东省梅州市丰顺县");
		areaCodes.put("441424", "广东省梅州市五华县");
		areaCodes.put("441426", "广东省梅州市平远县");
		areaCodes.put("441427", "广东省梅州市蕉岭县");
		areaCodes.put("441481", "广东省梅州市兴宁市");
		areaCodes.put("441500", "广东省汕尾市");
		areaCodes.put("441501", "广东省汕尾市");
		areaCodes.put("441502", "广东省汕尾市城区");
		areaCodes.put("441521", "广东省汕尾市海丰县");
		areaCodes.put("441523", "广东省汕尾市陆河县");
		areaCodes.put("441581", "广东省汕尾市陆丰市");
		areaCodes.put("441600", "广东省河源市");
		areaCodes.put("441601", "广东省河源市");
		areaCodes.put("441602", "广东省河源市源城区");
		areaCodes.put("441621", "广东省河源市紫金县");
		areaCodes.put("441622", "广东省河源市龙川县");
		areaCodes.put("441623", "广东省河源市连平县");
		areaCodes.put("441624", "广东省河源市和平县");
		areaCodes.put("441625", "广东省河源市东源县");
		areaCodes.put("450224", "广西壮族自治区柳州市融安县");
		areaCodes.put("500243", "重庆市彭水苗族土家族自治县");
		areaCodes.put("500300", "重庆市");
		areaCodes.put("440105", "广东省广州市海珠区");
		areaCodes.put("440106", "广东省广州市天河区");
		areaCodes.put("440107", "广东省广州市芳村区");
		areaCodes.put("440111", "广东省广州市白云区");
		areaCodes.put("440112", "广东省广州市黄埔区");
		areaCodes.put("440113", "广东省广州市番禺区");
		areaCodes.put("440114", "广东省广州市花都区");
		areaCodes.put("440183", "广东省广州市增城市");
		areaCodes.put("440184", "广东省广州市从化市");
		areaCodes.put("440200", "广东省韶关市");
		areaCodes.put("440201", "广东省韶关市");
		areaCodes.put("440203", "广东省韶关市武江区");
		areaCodes.put("440204", "广东省韶关市浈江区");
		areaCodes.put("440205", "广东省韶关市曲江区");
		areaCodes.put("440222", "广东省韶关市始兴县");
		areaCodes.put("440224", "广东省韶关市仁化县");
		areaCodes.put("440229", "广东省韶关市翁源县");
		areaCodes.put("440232", "广东省韶关市乳源瑶族自治县");
		areaCodes.put("440233", "广东省韶关市新丰县");
		areaCodes.put("440281", "广东省韶关市乐昌市");
		areaCodes.put("440282", "广东省韶关市南雄市");
		areaCodes.put("440300", "广东省深圳市");
		areaCodes.put("440301", "广东省深圳市");
		areaCodes.put("440303", "广东省深圳市罗湖区");
		areaCodes.put("440304", "广东省深圳市福田区");
		areaCodes.put("440305", "广东省深圳市南山区");
		areaCodes.put("440306", "广东省深圳市宝安区");
		areaCodes.put("440307", "广东省深圳市龙岗区");
		areaCodes.put("440308", "广东省深圳市盐田区");
		areaCodes.put("440400", "广东省珠海市");
		areaCodes.put("440401", "广东省珠海市");
		areaCodes.put("440402", "广东省珠海市香洲区");
		areaCodes.put("440403", "广东省珠海市斗门区");
		areaCodes.put("440404", "广东省珠海市金湾区");
		areaCodes.put("440500", "广东省汕头市");
		areaCodes.put("440501", "广东省汕头市");
		areaCodes.put("440507", "广东省汕头市龙湖区");
		areaCodes.put("440511", "广东省汕头市金平区");
		areaCodes.put("440512", "广东省汕头市濠江区");
		areaCodes.put("440513", "广东省汕头市潮阳区");
		areaCodes.put("440514", "广东省汕头市潮南区");
		areaCodes.put("440515", "广东省汕头市澄海区");
		areaCodes.put("440523", "广东省汕头市南澳县");
		areaCodes.put("440600", "广东省佛山市");
		areaCodes.put("440601", "广东省佛山市");
		areaCodes.put("440604", "广东省佛山市禅城区");
		areaCodes.put("440605", "广东省佛山市南海区");
		areaCodes.put("440606", "广东省佛山市顺德区");
		areaCodes.put("440607", "广东省佛山市三水区");
		areaCodes.put("440608", "广东省佛山市高明区");
		areaCodes.put("440700", "广东省江门市");
		areaCodes.put("440701", "广东省江门市");
		areaCodes.put("440703", "广东省江门市蓬江区");
		areaCodes.put("440704", "广东省江门市江海区");
		areaCodes.put("440705", "广东省江门市新会区");
		areaCodes.put("440781", "广东省江门市台山市");
		areaCodes.put("460100", "海南省海口市");
		areaCodes.put("460101", "海南省海口市");
		areaCodes.put("500241", "重庆市秀山土家族苗族自治县");
		areaCodes.put("500242", "重庆市酉阳土家族苗族自治县");
		areaCodes.put("441701", "广东省阳江市");
		areaCodes.put("450921", "广西壮族自治区玉林市容县");
		areaCodes.put("450922", "广西壮族自治区玉林市陆川县");
		areaCodes.put("450923", "广西壮族自治区玉林市博白县");
		areaCodes.put("450924", "广西壮族自治区玉林市兴业县");
		areaCodes.put("450981", "广西壮族自治区玉林市北流市");
		areaCodes.put("451000", "广西壮族自治区百色市");
		areaCodes.put("451001", "广西壮族自治区百色市");
		areaCodes.put("451002", "广西壮族自治区百色市右江区");
		areaCodes.put("451021", "广西壮族自治区百色市田阳县");
		areaCodes.put("451022", "广西壮族自治区百色市田东县");
		areaCodes.put("451023", "广西壮族自治区百色市平果县");
		areaCodes.put("451024", "广西壮族自治区百色市德保县");
		areaCodes.put("451025", "广西壮族自治区百色市靖西县");
		areaCodes.put("451026", "广西壮族自治区百色市那坡县");
		areaCodes.put("451027", "广西壮族自治区百色市凌云县");
		areaCodes.put("451028", "广西壮族自治区百色市乐业县");
		areaCodes.put("451029", "广西壮族自治区百色市田林县");
		areaCodes.put("451030", "广西壮族自治区百色市西林县");
		areaCodes.put("451031", "广西壮族自治区百色市隆林各族自治县");
		areaCodes.put("451100", "广西壮族自治区贺州市");
		areaCodes.put("451101", "广西壮族自治区贺州市");
		areaCodes.put("451102", "广西壮族自治区贺州市八步区");
		areaCodes.put("451121", "广西壮族自治区贺州市昭平县");
		areaCodes.put("451122", "广西壮族自治区贺州市钟山县");
		areaCodes.put("451123", "广西壮族自治区贺州市富川瑶族自治县");
		areaCodes.put("451200", "广西壮族自治区河池市");
		areaCodes.put("451201", "广西壮族自治区河池市");
		areaCodes.put("451202", "广西壮族自治区河池市金城江区");
		areaCodes.put("451221", "广西壮族自治区河池市南丹县");
		areaCodes.put("451222", "广西壮族自治区河池市天峨县");
		areaCodes.put("451223", "广西壮族自治区河池市凤山县");
		areaCodes.put("451224", "广西壮族自治区河池市东兰县");
		areaCodes.put("451225", "广西壮族自治区河池市罗城仫佬族自治县");
		areaCodes.put("451226", "广西壮族自治区河池市环江毛南族自治县");
		areaCodes.put("451227", "广西壮族自治区河池市巴马瑶族自治县");
		areaCodes.put("451228", "广西壮族自治区河池市都安瑶族自治县");
		areaCodes.put("451229", "广西壮族自治区河池市大化瑶族自治县");
		areaCodes.put("451281", "广西壮族自治区河池市宜州市");
		areaCodes.put("451300", "广西壮族自治区来宾市");
		areaCodes.put("451301", "广西壮族自治区来宾市");
		areaCodes.put("451302", "广西壮族自治区来宾市兴宾区");
		areaCodes.put("451321", "广西壮族自治区来宾市忻城县");
		areaCodes.put("430801", "湖南省张家界市");
		areaCodes.put("430802", "湖南省张家界市永定区");
		areaCodes.put("430811", "湖南省张家界市武陵源区");
		areaCodes.put("430821", "湖南省张家界市慈利县");
		areaCodes.put("430822", "湖南省张家界市桑植县");
		areaCodes.put("430900", "湖南省益阳市");
		areaCodes.put("430901", "湖南省益阳市");
		areaCodes.put("430902", "湖南省益阳市资阳区");
		areaCodes.put("430903", "湖南省益阳市赫山区");
		areaCodes.put("430921", "湖南省益阳市南县");
		areaCodes.put("441700", "广东省阳江市");
		areaCodes.put("420502", "湖北省宜昌市西陵区");
		areaCodes.put("430981", "湖南省益阳市沅江市");
		areaCodes.put("431000", "湖南省郴州市");
		areaCodes.put("431001", "湖南省郴州市");
		areaCodes.put("431002", "湖南省郴州市北湖区");
		areaCodes.put("431003", "湖南省郴州市苏仙区");
		areaCodes.put("431021", "湖南省郴州市桂阳县");
		areaCodes.put("431022", "湖南省郴州市宜章县");
		areaCodes.put("431023", "湖南省郴州市永兴县");
		areaCodes.put("431024", "湖南省郴州市嘉禾县");
		areaCodes.put("431025", "湖南省郴州市临武县");
		areaCodes.put("431026", "湖南省郴州市汝城县");
		areaCodes.put("431027", "湖南省郴州市桂东县");
		areaCodes.put("431028", "湖南省郴州市安仁县");
		areaCodes.put("431081", "湖南省郴州市资兴市");
		areaCodes.put("431100", "湖南省永州市");
		areaCodes.put("431101", "湖南省永州市");
		areaCodes.put("431102", "湖南省永州市芝山区");
		areaCodes.put("431103", "湖南省永州市冷水滩区");
		areaCodes.put("431121", "湖南省永州市祁阳县");
		areaCodes.put("431122", "湖南省永州市东安县");
		areaCodes.put("431123", "湖南省永州市双牌县");
		areaCodes.put("431124", "湖南省永州市道县");
		areaCodes.put("431125", "湖南省永州市江永县");
		areaCodes.put("431126", "湖南省永州市宁远县");
		areaCodes.put("431127", "湖南省永州市蓝山县");
		areaCodes.put("431128", "湖南省永州市新田县");
		areaCodes.put("431129", "湖南省永州市江华瑶族自治县");
		areaCodes.put("431200", "湖南省怀化市");
		areaCodes.put("431201", "湖南省怀化市");
		areaCodes.put("431202", "湖南省怀化市鹤城区");
		areaCodes.put("431221", "湖南省怀化市中方县");
		areaCodes.put("431222", "湖南省怀化市沅陵县");
		areaCodes.put("431223", "湖南省怀化市辰溪县");
		areaCodes.put("431224", "湖南省怀化市溆浦县");
		areaCodes.put("431225", "湖南省怀化市会同县");
		areaCodes.put("431226", "湖南省怀化市麻阳苗族自治县");
		areaCodes.put("431227", "湖南省怀化市新晃侗族自治县");
		areaCodes.put("431228", "湖南省怀化市芷江侗族自治县");
		areaCodes.put("431229", "湖南省怀化市靖州苗族侗族自治县");
		areaCodes.put("431230", "湖南省怀化市通道侗族自治县");
		areaCodes.put("431281", "湖南省怀化市洪江市");
		areaCodes.put("431300", "湖南省娄底市");
		areaCodes.put("431301", "湖南省娄底市");
		areaCodes.put("431302", "湖南省娄底市娄星区");
		areaCodes.put("431321", "湖南省娄底市双峰县");
		areaCodes.put("431322", "湖南省娄底市新化县");
		areaCodes.put("431381", "湖南省娄底市冷水江市");
		areaCodes.put("431382", "湖南省娄底市涟源市");
		areaCodes.put("433100", "湖南省湘西土家族苗族自治州");
		areaCodes.put("433101", "湖南省湘西土家族苗族自治州吉首市");
		areaCodes.put("433122", "湖南省湘西土家族苗族自治州泸溪县");
		areaCodes.put("433123", "湖南省湘西土家族苗族自治州凤凰县");
		areaCodes.put("433124", "湖南省湘西土家族苗族自治州花垣县");
		areaCodes.put("433125", "湖南省湘西土家族苗族自治州保靖县");
		areaCodes.put("433126", "湖南省湘西土家族苗族自治州古丈县");
		areaCodes.put("433127", "湖南省湘西土家族苗族自治州永顺县");
		areaCodes.put("433130", "湖南省湘西土家族苗族自治州龙山县");
		areaCodes.put("440000", "广东省");
		areaCodes.put("410329", "河南省洛阳市伊川县");
		areaCodes.put("410923", "河南省濮阳市南乐县");
		areaCodes.put("450902", "广西壮族自治区玉林市玉州区");
		areaCodes.put("421125", "湖北省黄冈市浠水县");
		areaCodes.put("421126", "湖北省黄冈市蕲春县");
		areaCodes.put("421127", "湖北省黄冈市黄梅县");
		areaCodes.put("421181", "湖北省黄冈市麻城市");
		areaCodes.put("421182", "湖北省黄冈市武穴市");
		areaCodes.put("421200", "湖北省咸宁市");
		areaCodes.put("421201", "湖北省咸宁市");
		areaCodes.put("421202", "湖北省咸宁市咸安区");
		areaCodes.put("421221", "湖北省咸宁市嘉鱼县");
		areaCodes.put("421222", "湖北省咸宁市通城县");
		areaCodes.put("421223", "湖北省咸宁市崇阳县");
		areaCodes.put("421224", "湖北省咸宁市通山县");
		areaCodes.put("421281", "湖北省咸宁市赤壁市");
		areaCodes.put("421300", "湖北省随州市");
		areaCodes.put("421301", "湖北省随州市");
		areaCodes.put("421302", "湖北省随州市曾都区");
		areaCodes.put("421381", "湖北省随州市广水市");
		areaCodes.put("422800", "湖北省恩施土家族苗族自治州");
		areaCodes.put("422801", "湖北省恩施土家族苗族自治州恩施市");
		areaCodes.put("422802", "湖北省恩施土家族苗族自治州利川市");
		areaCodes.put("422822", "湖北省恩施土家族苗族自治州建始县");
		areaCodes.put("422823", "湖北省恩施土家族苗族自治州巴东县");
		areaCodes.put("422825", "湖北省恩施土家族苗族自治州宣恩县");
		areaCodes.put("422826", "湖北省恩施土家族苗族自治州咸丰县");
		areaCodes.put("422827", "湖北省恩施土家族苗族自治州来凤县");
		areaCodes.put("422828", "湖北省恩施土家族苗族自治州鹤峰县");
		areaCodes.put("429000", "湖北省省直辖行政单位");
		areaCodes.put("429004", "湖北省仙桃市");
		areaCodes.put("429005", "湖北省潜江市");
		areaCodes.put("429006", "湖北省天门市");
		areaCodes.put("530111", "云南省昆明市官渡区");
		areaCodes.put("530112", "云南省昆明市西山区");
		areaCodes.put("530113", "云南省昆明市东川区");
		areaCodes.put("530121", "云南省昆明市呈贡县");
		areaCodes.put("530122", "云南省昆明市晋宁县");
		areaCodes.put("530124", "云南省昆明市富民县");
		areaCodes.put("530125", "云南省昆明市宜良县");
		areaCodes.put("530126", "云南省昆明市石林彝族自治县");
		areaCodes.put("530127", "云南省昆明市嵩明县");
		areaCodes.put("530128", "云南省昆明市禄劝彝族苗族自治县");
		areaCodes.put("530129", "云南省昆明市寻甸回族彝族自治县");
		areaCodes.put("530181", "云南省昆明市安宁市");
		areaCodes.put("530300", "云南省曲靖市");
		areaCodes.put("530301", "云南省曲靖市");
		areaCodes.put("530302", "云南省曲靖市麒麟区");
		areaCodes.put("530321", "云南省曲靖市马龙县");
		areaCodes.put("530322", "云南省曲靖市陆良县");
		areaCodes.put("530323", "云南省曲靖市师宗县");
		areaCodes.put("530324", "云南省曲靖市罗平县");
		areaCodes.put("530325", "云南省曲靖市富源县");
		areaCodes.put("530326", "云南省曲靖市会泽县");
		areaCodes.put("530328", "云南省曲靖市沾益县");
		areaCodes.put("530381", "云南省曲靖市宣威市");
		areaCodes.put("530400", "云南省玉溪市");
		areaCodes.put("530401", "云南省玉溪市");
		areaCodes.put("620500", "甘肃省天水市");
		areaCodes.put("533124", "云南省德宏傣族景颇族自治州陇川县");
		areaCodes.put("533300", "云南省怒江傈僳族自治州");
		areaCodes.put("533321", "云南省怒江傈僳族自治州泸水县");
		areaCodes.put("532323", "云南省楚雄彝族自治州牟定县");
		areaCodes.put("532324", "云南省楚雄彝族自治州南华县");
		areaCodes.put("532325", "云南省楚雄彝族自治州姚安县");
		areaCodes.put("532326", "云南省楚雄彝族自治州大姚县");
		areaCodes.put("532327", "云南省楚雄彝族自治州永仁县");
		areaCodes.put("532328", "云南省楚雄彝族自治州元谋县");
		areaCodes.put("532329", "云南省楚雄彝族自治州武定县");
		areaCodes.put("532331", "云南省楚雄彝族自治州禄丰县");
		areaCodes.put("532500", "云南省红河哈尼族彝族自治州");
		areaCodes.put("532501", "云南省红河哈尼族彝族自治州个旧市");
		areaCodes.put("532502", "云南省红河哈尼族彝族自治州开远市");
		areaCodes.put("532522", "云南省红河哈尼族彝族自治州蒙自县");
		areaCodes.put("532523", "云南省红河哈尼族彝族自治州屏边苗族自治县");
		areaCodes.put("532524", "云南省红河哈尼族彝族自治州建水县");
		areaCodes.put("532525", "云南省红河哈尼族彝族自治州石屏县");
		areaCodes.put("532526", "云南省红河哈尼族彝族自治州弥勒县");
		areaCodes.put("532527", "云南省红河哈尼族彝族自治州泸西县");
		areaCodes.put("532528", "云南省红河哈尼族彝族自治州元阳县");
		areaCodes.put("532529", "云南省红河哈尼族彝族自治州红河县");
		areaCodes.put("532530", "云南省红河哈尼族彝族自治州金平苗族瑶族傣族自治县");
		areaCodes.put("532531", "云南省红河哈尼族彝族自治州绿春县");
		areaCodes.put("532532", "云南省红河哈尼族彝族自治州河口瑶族自治县");
		areaCodes.put("532600", "云南省文山壮族苗族自治州");
		areaCodes.put("532621", "云南省文山壮族苗族自治州文山县");
		areaCodes.put("532622", "云南省文山壮族苗族自治州砚山县");
		areaCodes.put("532623", "云南省文山壮族苗族自治州西畴县");
		areaCodes.put("532624", "云南省文山壮族苗族自治州麻栗坡县");
		areaCodes.put("532625", "云南省文山壮族苗族自治州马关县");
		areaCodes.put("532626", "云南省文山壮族苗族自治州丘北县");
		areaCodes.put("532627", "云南省文山壮族苗族自治州广南县");
		areaCodes.put("532628", "云南省文山壮族苗族自治州富宁县");
		areaCodes.put("532800", "云南省西双版纳傣族自治州");
		areaCodes.put("532801", "云南省西双版纳傣族自治州景洪市");
		areaCodes.put("532822", "云南省西双版纳傣族自治州勐海县");
		areaCodes.put("532823", "云南省西双版纳傣族自治州勐腊县");
		areaCodes.put("532900", "云南省大理白族自治州");
		areaCodes.put("532901", "云南省大理白族自治州大理市");
		areaCodes.put("532922", "云南省大理白族自治州漾濞彝族自治县");
		areaCodes.put("532923", "云南省大理白族自治州祥云县");
		areaCodes.put("532924", "云南省大理白族自治州宾川县");
		areaCodes.put("532925", "云南省大理白族自治州弥渡县");
		areaCodes.put("532926", "云南省大理白族自治州南涧彝族自治县");
		areaCodes.put("532927", "云南省大理白族自治州巍山彝族回族自治县");
		areaCodes.put("532928", "云南省大理白族自治州永平县");
		areaCodes.put("532929", "云南省大理白族自治州云龙县");
		areaCodes.put("532930", "云南省大理白族自治州洱源县");
		areaCodes.put("532931", "云南省大理白族自治州剑川县");
		areaCodes.put("532932", "云南省大理白族自治州鹤庆县");
		areaCodes.put("533100", "云南省德宏傣族景颇族自治州");
		areaCodes.put("533102", "云南省德宏傣族景颇族自治州瑞丽市");
		areaCodes.put("533103", "云南省德宏傣族景颇族自治州潞西市");
		areaCodes.put("533122", "云南省德宏傣族景颇族自治州梁河县");
		areaCodes.put("533123", "云南省德宏傣族景颇族自治州盈江县");
		areaCodes.put("610727", "陕西省汉中市略阳县");
		areaCodes.put("610728", "陕西省汉中市镇巴县");
		areaCodes.put("610729", "陕西省汉中市留坝县");
		areaCodes.put("533323", "云南省怒江傈僳族自治州福贡县");
		areaCodes.put("533324", "云南省怒江傈僳族自治州贡山独龙族怒族自治县");
		areaCodes.put("533325", "云南省怒江傈僳族自治州兰坪白族普米族自治县");
		areaCodes.put("533400", "云南省迪庆藏族自治州");
		areaCodes.put("533421", "云南省迪庆藏族自治州香格里拉县");
		areaCodes.put("533422", "云南省迪庆藏族自治州德钦县");
		areaCodes.put("533423", "云南省迪庆藏族自治州维西傈僳族自治县");
		areaCodes.put("540000", "西藏自治区");
		areaCodes.put("540100", "西藏自治区拉萨市");
		areaCodes.put("540101", "西藏自治区拉萨市");
		areaCodes.put("540102", "西藏自治区拉萨市城关区");
		areaCodes.put("540121", "西藏自治区拉萨市林周县");
		areaCodes.put("540122", "西藏自治区拉萨市当雄县");
		areaCodes.put("540123", "西藏自治区拉萨市尼木县");
		areaCodes.put("540124", "西藏自治区拉萨市曲水县");
		areaCodes.put("540125", "西藏自治区拉萨市堆龙德庆县");
		areaCodes.put("540126", "西藏自治区拉萨市达孜县");
		areaCodes.put("540127", "西藏自治区拉萨市墨竹工卡县");
		areaCodes.put("542100", "西藏自治区昌都地区");
		areaCodes.put("542121", "西藏自治区昌都地区昌都县");
		areaCodes.put("542122", "西藏自治区昌都地区江达县");
		areaCodes.put("542123", "西藏自治区昌都地区贡觉县");
		areaCodes.put("542124", "西藏自治区昌都地区类乌齐县");
		areaCodes.put("542125", "西藏自治区昌都地区丁青县");
		areaCodes.put("542126", "西藏自治区昌都地区察雅县");
		areaCodes.put("542127", "西藏自治区昌都地区八宿县");
		areaCodes.put("542128", "西藏自治区昌都地区左贡县");
		areaCodes.put("542129", "西藏自治区昌都地区芒康县");
		areaCodes.put("542132", "西藏自治区昌都地区洛隆县");
		areaCodes.put("542133", "西藏自治区昌都地区边坝县");
		areaCodes.put("542200", "西藏自治区山南地区");
		areaCodes.put("542221", "西藏自治区山南地区乃东县");
		areaCodes.put("542222", "西藏自治区山南地区扎囊县");
		areaCodes.put("542223", "西藏自治区山南地区贡嘎县");
		areaCodes.put("542224", "西藏自治区山南地区桑日县");
		areaCodes.put("542225", "西藏自治区山南地区琼结县");
		areaCodes.put("542226", "西藏自治区山南地区曲松县");
		areaCodes.put("542227", "西藏自治区山南地区措美县");
		areaCodes.put("542228", "西藏自治区山南地区洛扎县");
		areaCodes.put("542229", "西藏自治区山南地区加查县");
		areaCodes.put("542231", "西藏自治区山南地区隆子县");
		areaCodes.put("542232", "西藏自治区山南地区错那县");
		areaCodes.put("542233", "西藏自治区山南地区浪卡子县");
		areaCodes.put("542300", "西藏自治区日喀则地区");
		areaCodes.put("542301", "西藏自治区日喀则地区日喀则市");
		areaCodes.put("542322", "西藏自治区日喀则地区南木林县");
		areaCodes.put("542323", "西藏自治区日喀则地区江孜县");
		areaCodes.put("542324", "西藏自治区日喀则地区定日县");
		areaCodes.put("542325", "西藏自治区日喀则地区萨迦县");
		areaCodes.put("542326", "西藏自治区日喀则地区拉孜县");
		areaCodes.put("542327", "西藏自治区日喀则地区昂仁县");
		areaCodes.put("542328", "西藏自治区日喀则地区谢通门县");
		areaCodes.put("542329", "西藏自治区日喀则地区白朗县");
		areaCodes.put("542330", "西藏自治区日喀则地区仁布县");
		areaCodes.put("542331", "西藏自治区日喀则地区康马县");
		areaCodes.put("513227", "四川省阿坝藏族羌族自治州小金县");
		areaCodes.put("513228", "四川省阿坝藏族羌族自治州黑水县");
		areaCodes.put("513229", "四川省阿坝藏族羌族自治州马尔康县");
		areaCodes.put("511424", "四川省眉山市丹棱县");
		areaCodes.put("530424", "云南省玉溪市华宁县");
		areaCodes.put("530425", "云南省玉溪市易门县");
		areaCodes.put("530426", "云南省玉溪市峨山彝族自治县");
		areaCodes.put("530427", "云南省玉溪市新平彝族傣族自治县");
		areaCodes.put("530428", "云南省玉溪市元江哈尼族彝族傣族自治县");
		areaCodes.put("530500", "云南省保山市");
		areaCodes.put("530501", "云南省保山市");
		areaCodes.put("530502", "云南省保山市隆阳区");
		areaCodes.put("530521", "云南省保山市施甸县");
		areaCodes.put("530522", "云南省保山市腾冲县");
		areaCodes.put("530523", "云南省保山市龙陵县");
		areaCodes.put("530524", "云南省保山市昌宁县");
		areaCodes.put("530600", "云南省昭通市");
		areaCodes.put("530601", "云南省昭通市");
		areaCodes.put("530602", "云南省昭通市昭阳区");
		areaCodes.put("530621", "云南省昭通市鲁甸县");
		areaCodes.put("530622", "云南省昭通市巧家县");
		areaCodes.put("530623", "云南省昭通市盐津县");
		areaCodes.put("530624", "云南省昭通市大关县");
		areaCodes.put("530625", "云南省昭通市永善县");
		areaCodes.put("530626", "云南省昭通市绥江县");
		areaCodes.put("530627", "云南省昭通市镇雄县");
		areaCodes.put("530628", "云南省昭通市彝良县");
		areaCodes.put("530629", "云南省昭通市威信县");
		areaCodes.put("530630", "云南省昭通市水富县");
		areaCodes.put("530700", "云南省丽江市");
		areaCodes.put("530701", "云南省丽江市");
		areaCodes.put("530702", "云南省丽江市古城区");
		areaCodes.put("530721", "云南省丽江市玉龙纳西族自治县");
		areaCodes.put("530722", "云南省丽江市永胜县");
		areaCodes.put("530723", "云南省丽江市华坪县");
		areaCodes.put("530724", "云南省丽江市宁蒗彝族自治县");
		areaCodes.put("530800", "云南省思茅市");
		areaCodes.put("530801", "云南省思茅市");
		areaCodes.put("530802", "云南省思茅市翠云区");
		areaCodes.put("530821", "云南省思茅市普洱哈尼族彝族自治县");
		areaCodes.put("530822", "云南省思茅市墨江哈尼族自治县");
		areaCodes.put("530823", "云南省思茅市景东彝族自治县");
		areaCodes.put("530824", "云南省思茅市景谷傣族彝族自治县");
		areaCodes.put("530825", "云南省思茅市镇沅彝族哈尼族拉祜族自治县");
		areaCodes.put("530826", "云南省思茅市江城哈尼族彝族自治县");
		areaCodes.put("530827", "云南省思茅市孟连傣族拉祜族佤族自治县");
		areaCodes.put("530828", "云南省思茅市澜沧拉祜族自治县");
		areaCodes.put("530829", "云南省思茅市西盟佤族自治县");
		areaCodes.put("530900", "云南省临沧市");
		areaCodes.put("530901", "云南省临沧市");
		areaCodes.put("530902", "云南省临沧市临翔区");
		areaCodes.put("530921", "云南省临沧市凤庆县");
		areaCodes.put("530922", "云南省临沧市云县");
		areaCodes.put("530923", "云南省临沧市永德县");
		areaCodes.put("530924", "云南省临沧市镇康县");
		areaCodes.put("530925", "云南省临沧市双江拉祜族佤族布朗族傣族自治县");
		areaCodes.put("530926", "云南省临沧市耿马傣族佤族自治县");
		areaCodes.put("530927", "云南省临沧市沧源佤族自治县");
		areaCodes.put("520203", "贵州省六盘水市六枝特区");
		areaCodes.put("520221", "贵州省六盘水市水城县");
		areaCodes.put("520222", "贵州省六盘水市盘县");
		areaCodes.put("520300", "贵州省遵义市");
		areaCodes.put("520301", "贵州省遵义市");
		areaCodes.put("520302", "贵州省遵义市红花岗区");
		areaCodes.put("520303", "贵州省遵义市汇川区");
		areaCodes.put("520321", "贵州省遵义市遵义县");
		areaCodes.put("520322", "贵州省遵义市桐梓县");
		areaCodes.put("520323", "贵州省遵义市绥阳县");
		areaCodes.put("520324", "贵州省遵义市正安县");
		areaCodes.put("520325", "贵州省遵义市道真仡佬族苗族自治县");
		areaCodes.put("520326", "贵州省遵义市务川仡佬族苗族自治县");
		areaCodes.put("520327", "贵州省遵义市凤冈县");
		areaCodes.put("520328", "贵州省遵义市湄潭县");
		areaCodes.put("520329", "贵州省遵义市余庆县");
		areaCodes.put("520330", "贵州省遵义市习水县");
		areaCodes.put("520381", "贵州省遵义市赤水市");
		areaCodes.put("520382", "贵州省遵义市仁怀市");
		areaCodes.put("520400", "贵州省安顺市");
		areaCodes.put("520401", "贵州省安顺市");
		areaCodes.put("520402", "贵州省安顺市西秀区");
		areaCodes.put("520421", "贵州省安顺市平坝县");
		areaCodes.put("520422", "贵州省安顺市普定县");
		areaCodes.put("520423", "贵州省安顺市镇宁布依族苗族自治县");
		areaCodes.put("520424", "贵州省安顺市关岭布依族苗族自治县");
		areaCodes.put("520425", "贵州省安顺市紫云苗族布依族自治县");
		areaCodes.put("522200", "贵州省铜仁地区");
		areaCodes.put("522201", "贵州省铜仁地区铜仁市");
		areaCodes.put("522222", "贵州省铜仁地区江口县");
		areaCodes.put("522223", "贵州省铜仁地区玉屏侗族自治县");
		areaCodes.put("522224", "贵州省铜仁地区石阡县");
		areaCodes.put("522225", "贵州省铜仁地区思南县");
		areaCodes.put("522226", "贵州省铜仁地区印江土家族苗族自治县");
		areaCodes.put("522227", "贵州省铜仁地区德江县");
		areaCodes.put("522228", "贵州省铜仁地区沿河土家族自治县");
		areaCodes.put("522229", "贵州省铜仁地区松桃苗族自治县");
		areaCodes.put("522230", "贵州省铜仁地区万山特区");
		areaCodes.put("522300", "贵州省黔西南布依族苗族自治州");
		areaCodes.put("522301", "贵州省黔西南布依族苗族自治州兴义市");
		areaCodes.put("522322", "贵州省黔西南布依族苗族自治州兴仁县");
		areaCodes.put("522323", "贵州省黔西南布依族苗族自治州普安县");
		areaCodes.put("522324", "贵州省黔西南布依族苗族自治州晴隆县");
		areaCodes.put("522325", "贵州省黔西南布依族苗族自治州贞丰县");
		areaCodes.put("522326", "贵州省黔西南布依族苗族自治州望谟县");
		areaCodes.put("522327", "贵州省黔西南布依族苗族自治州册亨县");
		areaCodes.put("522328", "贵州省黔西南布依族苗族自治州安龙县");
		areaCodes.put("522400", "贵州省毕节地区");
		areaCodes.put("522401", "贵州省毕节地区毕节市");
		areaCodes.put("522422", "贵州省毕节地区大方县");
		areaCodes.put("522423", "贵州省毕节地区黔西县");
		areaCodes.put("522424", "贵州省毕节地区金沙县");
		areaCodes.put("511301", "四川省南充市");
		areaCodes.put("511302", "四川省南充市顺庆区");
		areaCodes.put("511303", "四川省南充市高坪区");
		areaCodes.put("511304", "四川省南充市嘉陵区");
		areaCodes.put("511321", "四川省南充市南部县");
		areaCodes.put("511322", "四川省南充市营山县");
		areaCodes.put("511323", "四川省南充市蓬安县");
		areaCodes.put("511324", "四川省南充市仪陇县");
		areaCodes.put("511325", "四川省南充市西充县");
		areaCodes.put("511381", "四川省南充市阆中市");
		areaCodes.put("511400", "四川省眉山市");
		areaCodes.put("511401", "四川省眉山市");
		areaCodes.put("511402", "四川省眉山市东坡区");
		areaCodes.put("440102", "广东省广州市东山区");
		areaCodes.put("440103", "广东省广州市荔湾区");
		areaCodes.put("440104", "广东省广州市越秀区");
		areaCodes.put("500381", "重庆市江津市");
		areaCodes.put("500382", "重庆市合川市");
		areaCodes.put("500383", "重庆市永川市");
		areaCodes.put("500384", "重庆市南川市");
		areaCodes.put("510000", "四川省");
		areaCodes.put("510100", "四川省成都市");
		areaCodes.put("510101", "四川省成都市");
		areaCodes.put("510104", "四川省成都市锦江区");
		areaCodes.put("510105", "四川省成都市青羊区");
		areaCodes.put("510106", "四川省成都市金牛区");
		areaCodes.put("510107", "四川省成都市武侯区");
		areaCodes.put("510108", "四川省成都市成华区");
		areaCodes.put("510112", "四川省成都市龙泉驿区");
		areaCodes.put("510113", "四川省成都市青白江区");
		areaCodes.put("510114", "四川省成都市新都区");
		areaCodes.put("510115", "四川省成都市温江区");
		areaCodes.put("510121", "四川省成都市金堂县");
		areaCodes.put("510122", "四川省成都市双流县");
		areaCodes.put("510124", "四川省成都市郫县");
		areaCodes.put("510129", "四川省成都市大邑县");
		areaCodes.put("510131", "四川省成都市蒲江县");
		areaCodes.put("510132", "四川省成都市新津县");
		areaCodes.put("510181", "四川省成都市都江堰市");
		areaCodes.put("510182", "四川省成都市彭州市");
		areaCodes.put("510183", "四川省成都市邛崃市");
		areaCodes.put("510184", "四川省成都市崇州市");
		areaCodes.put("510300", "四川省自贡市");
		areaCodes.put("510301", "四川省自贡市");
		areaCodes.put("510302", "四川省自贡市自流井区");
		areaCodes.put("510303", "四川省自贡市贡井区");
		areaCodes.put("510304", "四川省自贡市大安区");
		areaCodes.put("510311", "四川省自贡市沿滩区");
		areaCodes.put("510321", "四川省自贡市荣县");
		areaCodes.put("510322", "四川省自贡市富顺县");
		areaCodes.put("510400", "四川省攀枝花市");
		areaCodes.put("510401", "四川省攀枝花市");
		areaCodes.put("510402", "四川省攀枝花市东区");
		areaCodes.put("510403", "四川省攀枝花市西区");
		areaCodes.put("510411", "四川省攀枝花市仁和区");
		areaCodes.put("510421", "四川省攀枝花市米易县");
		areaCodes.put("510422", "四川省攀枝花市盐边县");
		areaCodes.put("510500", "四川省泸州市");
		areaCodes.put("510501", "四川省泸州市");
		areaCodes.put("510502", "四川省泸州市江阳区");
		areaCodes.put("510503", "四川省泸州市纳溪区");
		areaCodes.put("510504", "四川省泸州市龙马潭区");
		areaCodes.put("510521", "四川省泸州市泸县");
		areaCodes.put("510522", "四川省泸州市合江县");
		areaCodes.put("510524", "四川省泸州市叙永县");
		areaCodes.put("510525", "四川省泸州市古蔺县");
		areaCodes.put("510600", "四川省德阳市");
		areaCodes.put("510601", "四川省德阳市");
		areaCodes.put("510603", "四川省德阳市旌阳区");
		areaCodes.put("510623", "四川省德阳市中江县");
		areaCodes.put("510626", "四川省德阳市罗江县");
		areaCodes.put("510681", "四川省德阳市广汉市");
		areaCodes.put("510682", "四川省德阳市什邡市");
		areaCodes.put("530402", "云南省玉溪市红塔区");
		areaCodes.put("530421", "云南省玉溪市江川县");
		areaCodes.put("530422", "云南省玉溪市澄江县");
		areaCodes.put("510701", "四川省绵阳市");
		areaCodes.put("510703", "四川省绵阳市涪城区");
		areaCodes.put("441702", "广东省阳江市江城区");
		areaCodes.put("441721", "广东省阳江市阳西县");
		areaCodes.put("441723", "广东省阳江市阳东县");
		areaCodes.put("441781", "广东省阳江市阳春市");
		areaCodes.put("441800", "广东省清远市");
		areaCodes.put("441801", "广东省清远市");
		areaCodes.put("441802", "广东省清远市清城区");
		areaCodes.put("441821", "广东省清远市佛冈县");
		areaCodes.put("441823", "广东省清远市阳山县");
		areaCodes.put("441825", "广东省清远市连山壮族瑶族自治县");
		areaCodes.put("441826", "广东省清远市连南瑶族自治县");
		areaCodes.put("441827", "广东省清远市清新县");
		areaCodes.put("441881", "广东省清远市英德市");
		areaCodes.put("441882", "广东省清远市连州市");
		areaCodes.put("441900", "广东省东莞市");
		areaCodes.put("442000", "广东省中山市");
		areaCodes.put("445100", "广东省潮州市");
		areaCodes.put("445101", "广东省潮州市");
		areaCodes.put("445102", "广东省潮州市湘桥区");
		areaCodes.put("445121", "广东省潮州市潮安县");
		areaCodes.put("445122", "广东省潮州市饶平县");
		areaCodes.put("445200", "广东省揭阳市");
		areaCodes.put("445201", "广东省揭阳市");
		areaCodes.put("445202", "广东省揭阳市榕城区");
		areaCodes.put("445221", "广东省揭阳市揭东县");
		areaCodes.put("445222", "广东省揭阳市揭西县");
		areaCodes.put("445224", "广东省揭阳市惠来县");
		areaCodes.put("445281", "广东省揭阳市普宁市");
		areaCodes.put("445300", "广东省云浮市");
		areaCodes.put("513433", "四川省凉山彝族自治州冕宁县");
		areaCodes.put("513434", "四川省凉山彝族自治州越西县");
		areaCodes.put("513435", "四川省凉山彝族自治州甘洛县");
		areaCodes.put("513436", "四川省凉山彝族自治州美姑县");
		areaCodes.put("513437", "四川省凉山彝族自治州雷波县");
		areaCodes.put("520000", "贵州省");
		areaCodes.put("520100", "贵州省贵阳市");
		areaCodes.put("520101", "贵州省贵阳市");
		areaCodes.put("520102", "贵州省贵阳市南明区");
		areaCodes.put("520103", "贵州省贵阳市云岩区");
		areaCodes.put("520111", "贵州省贵阳市花溪区");
		areaCodes.put("520112", "贵州省贵阳市乌当区");
		areaCodes.put("520113", "贵州省贵阳市白云区");
		areaCodes.put("520114", "贵州省贵阳市小河区");
		areaCodes.put("520121", "贵州省贵阳市开阳县");
		areaCodes.put("520122", "贵州省贵阳市息烽县");
		areaCodes.put("520123", "贵州省贵阳市修文县");
		areaCodes.put("520181", "贵州省贵阳市清镇市");
		areaCodes.put("520200", "贵州省六盘水市");
		areaCodes.put("520201", "贵州省六盘水市钟山区");
		areaCodes.put("511421", "四川省眉山市仁寿县");
		areaCodes.put("511422", "四川省眉山市彭山县");
		areaCodes.put("511423", "四川省眉山市洪雅县");
		areaCodes.put("620523", "甘肃省天水市甘谷县");
		areaCodes.put("653024", "新疆维吾尔自治区克孜勒苏柯尔克孜自治州乌恰县");
		areaCodes.put("653100", "新疆维吾尔自治区喀什地区");
		areaCodes.put("653101", "新疆维吾尔自治区喀什地区喀什市");
		areaCodes.put("653121", "新疆维吾尔自治区喀什地区疏附县");
		areaCodes.put("653122", "新疆维吾尔自治区喀什地区疏勒县");
		areaCodes.put("653123", "新疆维吾尔自治区喀什地区英吉沙县");
		areaCodes.put("653124", "新疆维吾尔自治区喀什地区泽普县");
		areaCodes.put("653125", "新疆维吾尔自治区喀什地区莎车县");
		areaCodes.put("653126", "新疆维吾尔自治区喀什地区叶城县");
		areaCodes.put("653127", "新疆维吾尔自治区喀什地区麦盖提县");
		areaCodes.put("653128", "新疆维吾尔自治区喀什地区岳普湖县");
		areaCodes.put("653129", "新疆维吾尔自治区喀什地区伽师县");
		areaCodes.put("653130", "新疆维吾尔自治区喀什地区巴楚县");
		areaCodes.put("653131", "新疆维吾尔自治区喀什地区塔什库尔干塔吉克自治县");
		areaCodes.put("653200", "新疆维吾尔自治区和田地区");
		areaCodes.put("653201", "新疆维吾尔自治区和田地区和田市");
		areaCodes.put("653221", "新疆维吾尔自治区和田地区和田县");
		areaCodes.put("653222", "新疆维吾尔自治区和田地区墨玉县");
		areaCodes.put("653223", "新疆维吾尔自治区和田地区皮山县");
		areaCodes.put("653224", "新疆维吾尔自治区和田地区洛浦县");
		areaCodes.put("653225", "新疆维吾尔自治区和田地区策勒县");
		areaCodes.put("653226", "新疆维吾尔自治区和田地区于田县");
		areaCodes.put("653227", "新疆维吾尔自治区和田地区民丰县");
		areaCodes.put("654000", "新疆维吾尔自治区伊犁哈萨克自治州");
		areaCodes.put("654002", "新疆维吾尔自治区伊犁哈萨克自治州伊宁市");
		areaCodes.put("654003", "新疆维吾尔自治区伊犁哈萨克自治州奎屯市");
		areaCodes.put("654021", "新疆维吾尔自治区伊犁哈萨克自治州伊宁县");
		areaCodes.put("654022", "新疆维吾尔自治区伊犁哈萨克自治州察布查尔锡伯自治县");
		areaCodes.put("654023", "新疆维吾尔自治区伊犁哈萨克自治州霍城县");
		areaCodes.put("654024", "新疆维吾尔自治区伊犁哈萨克自治州巩留县");
		areaCodes.put("654025", "新疆维吾尔自治区伊犁哈萨克自治州新源县");
		areaCodes.put("654026", "新疆维吾尔自治区伊犁哈萨克自治州昭苏县");
		areaCodes.put("654027", "新疆维吾尔自治区伊犁哈萨克自治州特克斯县");
		areaCodes.put("654028", "新疆维吾尔自治区伊犁哈萨克自治州尼勒克县");
		areaCodes.put("654200", "新疆维吾尔自治区塔城地区");
		areaCodes.put("654201", "新疆维吾尔自治区塔城地区塔城市");
		areaCodes.put("654202", "新疆维吾尔自治区塔城地区乌苏市");
		areaCodes.put("654221", "新疆维吾尔自治区塔城地区额敏县");
		areaCodes.put("654223", "新疆维吾尔自治区塔城地区沙湾县");
		areaCodes.put("654224", "新疆维吾尔自治区塔城地区托里县");
		areaCodes.put("654225", "新疆维吾尔自治区塔城地区裕民县");
		areaCodes.put("654226", "新疆维吾尔自治区塔城地区和布克赛尔蒙古自治县");
		areaCodes.put("654300", "新疆维吾尔自治区阿勒泰地区");
		areaCodes.put("654301", "新疆维吾尔自治区阿勒泰地区阿勒泰市");
		areaCodes.put("654321", "新疆维吾尔自治区阿勒泰地区布尔津县");
		areaCodes.put("654322", "新疆维吾尔自治区阿勒泰地区富蕴县");
		areaCodes.put("654323", "新疆维吾尔自治区阿勒泰地区福海县");
		areaCodes.put("654324", "新疆维吾尔自治区阿勒泰地区哈巴河县");
		areaCodes.put("654325", "新疆维吾尔自治区阿勒泰地区青河县");
		areaCodes.put("542333", "西藏自治区日喀则地区仲巴县");
		areaCodes.put("542334", "西藏自治区日喀则地区亚东县");
		areaCodes.put("542335", "西藏自治区日喀则地区吉隆县");
		areaCodes.put("542336", "西藏自治区日喀则地区聂拉木县");
		areaCodes.put("632623", "青海省果洛藏族自治州甘德县");
		areaCodes.put("632624", "青海省果洛藏族自治州达日县");
		areaCodes.put("650108", "新疆维吾尔自治区乌鲁木齐市东山区");
		areaCodes.put("650121", "新疆维吾尔自治区乌鲁木齐市乌鲁木齐县");
		areaCodes.put("650200", "新疆维吾尔自治区克拉玛依市");
		areaCodes.put("650201", "新疆维吾尔自治区克拉玛依市");
		areaCodes.put("650202", "新疆维吾尔自治区克拉玛依市独山子区");
		areaCodes.put("650203", "新疆维吾尔自治区克拉玛依市克拉玛依区");
		areaCodes.put("650204", "新疆维吾尔自治区克拉玛依市白碱滩区");
		areaCodes.put("650205", "新疆维吾尔自治区克拉玛依市乌尔禾区");
		areaCodes.put("652100", "新疆维吾尔自治区吐鲁番地区");
		areaCodes.put("652101", "新疆维吾尔自治区吐鲁番地区吐鲁番市");
		areaCodes.put("652122", "新疆维吾尔自治区吐鲁番地区鄯善县");
		areaCodes.put("652123", "新疆维吾尔自治区吐鲁番地区托克逊县");
		areaCodes.put("652200", "新疆维吾尔自治区哈密地区");
		areaCodes.put("652201", "新疆维吾尔自治区哈密地区哈密市");
		areaCodes.put("652222", "新疆维吾尔自治区哈密地区巴里坤哈萨克自治县");
		areaCodes.put("652223", "新疆维吾尔自治区哈密地区伊吾县");
		areaCodes.put("652300", "新疆维吾尔自治区昌吉回族自治州");
		areaCodes.put("652301", "新疆维吾尔自治区昌吉回族自治州昌吉市");
		areaCodes.put("652302", "新疆维吾尔自治区昌吉回族自治州阜康市");
		areaCodes.put("652303", "新疆维吾尔自治区昌吉回族自治州米泉市");
		areaCodes.put("652323", "新疆维吾尔自治区昌吉回族自治州呼图壁县");
		areaCodes.put("652324", "新疆维吾尔自治区昌吉回族自治州玛纳斯县");
		areaCodes.put("652325", "新疆维吾尔自治区昌吉回族自治州奇台县");
		areaCodes.put("652327", "新疆维吾尔自治区昌吉回族自治州吉木萨尔县");
		areaCodes.put("652328", "新疆维吾尔自治区昌吉回族自治州木垒哈萨克自治县");
		areaCodes.put("652700", "新疆维吾尔自治区博尔塔拉蒙古自治州");
		areaCodes.put("652701", "新疆维吾尔自治区博尔塔拉蒙古自治州博乐市");
		areaCodes.put("652722", "新疆维吾尔自治区博尔塔拉蒙古自治州精河县");
		areaCodes.put("652723", "新疆维吾尔自治区博尔塔拉蒙古自治州温泉县");
		areaCodes.put("652800", "新疆维吾尔自治区巴音郭楞蒙古自治州");
		areaCodes.put("652801", "新疆维吾尔自治区巴音郭楞蒙古自治州库尔勒市");
		areaCodes.put("652822", "新疆维吾尔自治区巴音郭楞蒙古自治州轮台县");
		areaCodes.put("652823", "新疆维吾尔自治区巴音郭楞蒙古自治州尉犁县");
		areaCodes.put("652824", "新疆维吾尔自治区巴音郭楞蒙古自治州若羌县");
		areaCodes.put("652825", "新疆维吾尔自治区巴音郭楞蒙古自治州且末县");
		areaCodes.put("652826", "新疆维吾尔自治区巴音郭楞蒙古自治州焉耆回族自治县");
		areaCodes.put("652827", "新疆维吾尔自治区巴音郭楞蒙古自治州和静县");
		areaCodes.put("652828", "新疆维吾尔自治区巴音郭楞蒙古自治州和硕县");
		areaCodes.put("652829", "新疆维吾尔自治区巴音郭楞蒙古自治州博湖县");
		areaCodes.put("652900", "新疆维吾尔自治区阿克苏地区");
		areaCodes.put("652901", "新疆维吾尔自治区阿克苏地区阿克苏市");
		areaCodes.put("652922", "新疆维吾尔自治区阿克苏地区温宿县");
		areaCodes.put("652923", "新疆维吾尔自治区阿克苏地区库车县");
		areaCodes.put("652924", "新疆维吾尔自治区阿克苏地区沙雅县");
		areaCodes.put("652925", "新疆维吾尔自治区阿克苏地区新和县");
		areaCodes.put("652926", "新疆维吾尔自治区阿克苏地区拜城县");
		areaCodes.put("652927", "新疆维吾尔自治区阿克苏地区乌什县");
		areaCodes.put("652928", "新疆维吾尔自治区阿克苏地区阿瓦提县");
		areaCodes.put("542332", "西藏自治区日喀则地区定结县");
		areaCodes.put("659000", "新疆维吾尔自治区省直辖行政单位");
		areaCodes.put("659001", "新疆维吾尔自治区石河子市");
		areaCodes.put("659002", "新疆维吾尔自治区阿拉尔市");
		areaCodes.put("610730", "陕西省汉中市佛坪县");
		areaCodes.put("610800", "陕西省榆林市");
		areaCodes.put("610801", "陕西省榆林市");
		areaCodes.put("610802", "陕西省榆林市榆阳区");
		areaCodes.put("610821", "陕西省榆林市神木县");
		areaCodes.put("610822", "陕西省榆林市府谷县");
		areaCodes.put("610823", "陕西省榆林市横山县");
		areaCodes.put("610824", "陕西省榆林市靖边县");
		areaCodes.put("610825", "陕西省榆林市定边县");
		areaCodes.put("610826", "陕西省榆林市绥德县");
		areaCodes.put("610827", "陕西省榆林市米脂县");
		areaCodes.put("610828", "陕西省榆林市佳县");
		areaCodes.put("610829", "陕西省榆林市吴堡县");
		areaCodes.put("610830", "陕西省榆林市清涧县");
		areaCodes.put("610831", "陕西省榆林市子洲县");
		areaCodes.put("610900", "陕西省安康市");
		areaCodes.put("610901", "陕西省安康市");
		areaCodes.put("610902", "陕西省安康市汉滨区");
		areaCodes.put("610921", "陕西省安康市汉阴县");
		areaCodes.put("610922", "陕西省安康市石泉县");
		areaCodes.put("610923", "陕西省安康市宁陕县");
		areaCodes.put("610924", "陕西省安康市紫阳县");
		areaCodes.put("610925", "陕西省安康市岚皋县");
		areaCodes.put("610926", "陕西省安康市平利县");
		areaCodes.put("610927", "陕西省安康市镇坪县");
		areaCodes.put("610928", "陕西省安康市旬阳县");
		areaCodes.put("610929", "陕西省安康市白河县");
		areaCodes.put("611000", "陕西省商洛市");
		areaCodes.put("611001", "陕西省商洛市");
		areaCodes.put("611002", "陕西省商洛市商州区");
		areaCodes.put("611021", "陕西省商洛市洛南县");
		areaCodes.put("611022", "陕西省商洛市丹凤县");
		areaCodes.put("611023", "陕西省商洛市商南县");
		areaCodes.put("611024", "陕西省商洛市山阳县");
		areaCodes.put("611025", "陕西省商洛市镇安县");
		areaCodes.put("611026", "陕西省商洛市柞水县");
		areaCodes.put("620000", "甘肃省");
		areaCodes.put("620100", "甘肃省兰州市");
		areaCodes.put("620101", "甘肃省兰州市");
		areaCodes.put("620102", "甘肃省兰州市城关区");
		areaCodes.put("620103", "甘肃省兰州市七里河区");
		areaCodes.put("620104", "甘肃省兰州市西固区");
		areaCodes.put("620105", "甘肃省兰州市安宁区");
		areaCodes.put("620111", "甘肃省兰州市红古区");
		areaCodes.put("620121", "甘肃省兰州市永登县");
		areaCodes.put("620122", "甘肃省兰州市皋兰县");
		areaCodes.put("620123", "甘肃省兰州市榆中县");
		areaCodes.put("620200", "甘肃省嘉峪关市");
		areaCodes.put("620201", "甘肃省嘉峪关市");
		areaCodes.put("620300", "甘肃省金昌市");
		areaCodes.put("620301", "甘肃省金昌市");
		areaCodes.put("620302", "甘肃省金昌市金川区");
		areaCodes.put("620321", "甘肃省金昌市永昌县");
		areaCodes.put("620400", "甘肃省白银市");
		areaCodes.put("620401", "甘肃省白银市");
		areaCodes.put("620402", "甘肃省白银市白银区");
		areaCodes.put("620403", "甘肃省白银市平川区");
		areaCodes.put("620421", "甘肃省白银市靖远县");
		areaCodes.put("620422", "甘肃省白银市会宁县");
		areaCodes.put("620423", "甘肃省白银市景泰县");
		areaCodes.put("632600", "青海省果洛藏族自治州");
		areaCodes.put("632621", "青海省果洛藏族自治州玛沁县");
		areaCodes.put("632622", "青海省果洛藏族自治州班玛县");
		areaCodes.put("620524", "甘肃省天水市武山县");
		areaCodes.put("632625", "青海省果洛藏族自治州久治县");
		areaCodes.put("632626", "青海省果洛藏族自治州玛多县");
		areaCodes.put("632700", "青海省玉树藏族自治州");
		areaCodes.put("632721", "青海省玉树藏族自治州玉树县");
		areaCodes.put("632722", "青海省玉树藏族自治州杂多县");
		areaCodes.put("632723", "青海省玉树藏族自治州称多县");
		areaCodes.put("632724", "青海省玉树藏族自治州治多县");
		areaCodes.put("632725", "青海省玉树藏族自治州囊谦县");
		areaCodes.put("632726", "青海省玉树藏族自治州曲麻莱县");
		areaCodes.put("632800", "青海省海西蒙古族藏族自治州");
		areaCodes.put("632801", "青海省海西蒙古族藏族自治州格尔木市");
		areaCodes.put("632802", "青海省海西蒙古族藏族自治州德令哈市");
		areaCodes.put("632821", "青海省海西蒙古族藏族自治州乌兰县");
		areaCodes.put("632822", "青海省海西蒙古族藏族自治州都兰县");
		areaCodes.put("632823", "青海省海西蒙古族藏族自治州天峻县");
		areaCodes.put("640000", "宁夏回族自治区");
		areaCodes.put("640100", "宁夏回族自治区银川市");
		areaCodes.put("640101", "宁夏回族自治区银川市");
		areaCodes.put("640104", "宁夏回族自治区银川市兴庆区");
		areaCodes.put("640105", "宁夏回族自治区银川市西夏区");
		areaCodes.put("640106", "宁夏回族自治区银川市金凤区");
		areaCodes.put("640121", "宁夏回族自治区银川市永宁县");
		areaCodes.put("640122", "宁夏回族自治区银川市贺兰县");
		areaCodes.put("640181", "宁夏回族自治区银川市灵武市");
		areaCodes.put("640200", "宁夏回族自治区石嘴山市");
		areaCodes.put("640201", "宁夏回族自治区石嘴山市");
		areaCodes.put("640202", "宁夏回族自治区石嘴山市大武口区");
		areaCodes.put("640205", "宁夏回族自治区石嘴山市惠农区");
		areaCodes.put("640221", "宁夏回族自治区石嘴山市平罗县");
		areaCodes.put("640300", "宁夏回族自治区吴忠市");
		areaCodes.put("640301", "宁夏回族自治区吴忠市");
		areaCodes.put("640302", "宁夏回族自治区吴忠市利通区");
		areaCodes.put("640323", "宁夏回族自治区吴忠市盐池县");
		areaCodes.put("640324", "宁夏回族自治区吴忠市同心县");
		areaCodes.put("640381", "宁夏回族自治区吴忠市青铜峡市");
		areaCodes.put("640400", "宁夏回族自治区固原市");
		areaCodes.put("640401", "宁夏回族自治区固原市");
		areaCodes.put("640402", "宁夏回族自治区固原市原州区");
		areaCodes.put("640422", "宁夏回族自治区固原市西吉县");
		areaCodes.put("640423", "宁夏回族自治区固原市隆德县");
		areaCodes.put("640424", "宁夏回族自治区固原市泾源县");
		areaCodes.put("640425", "宁夏回族自治区固原市彭阳县");
		areaCodes.put("640500", "宁夏回族自治区中卫市");
		areaCodes.put("640501", "宁夏回族自治区中卫市");
		areaCodes.put("640502", "宁夏回族自治区中卫市沙坡头区");
		areaCodes.put("640521", "宁夏回族自治区中卫市中宁县");
		areaCodes.put("640522", "宁夏回族自治区中卫市海原县");
		areaCodes.put("650000", "新疆维吾尔自治区");
		areaCodes.put("650100", "新疆维吾尔自治区乌鲁木齐市");
		areaCodes.put("650101", "新疆维吾尔自治区乌鲁木齐市");
		areaCodes.put("650102", "新疆维吾尔自治区乌鲁木齐市天山区");
		areaCodes.put("650103", "新疆维吾尔自治区乌鲁木齐市沙依巴克区");
		areaCodes.put("610304", "陕西省宝鸡市陈仓区");
		areaCodes.put("451322", "广西壮族自治区来宾市象州县");
		areaCodes.put("451323", "广西壮族自治区来宾市武宣县");
		areaCodes.put("451324", "广西壮族自治区来宾市金秀瑶族自治县");
		areaCodes.put("451381", "广西壮族自治区来宾市合山市");
		areaCodes.put("451400", "广西壮族自治区崇左市");
		areaCodes.put("451401", "广西壮族自治区崇左市");
		areaCodes.put("451402", "广西壮族自治区崇左市江洲区");
		areaCodes.put("451421", "广西壮族自治区崇左市扶绥县");
		areaCodes.put("451422", "广西壮族自治区崇左市宁明县");
		areaCodes.put("451423", "广西壮族自治区崇左市龙州县");
		areaCodes.put("451424", "广西壮族自治区崇左市大新县");
		areaCodes.put("451425", "广西壮族自治区崇左市天等县");
		areaCodes.put("451481", "广西壮族自治区崇左市凭祥市");
		areaCodes.put("460000", "海南省");
		areaCodes.put("440784", "广东省江门市鹤山市");
		areaCodes.put("460106", "海南省海口市龙华区");
		areaCodes.put("450225", "广西壮族自治区柳州市融水苗族自治县");
		areaCodes.put("450226", "广西壮族自治区柳州市三江侗族自治县");
		areaCodes.put("450300", "广西壮族自治区桂林市");
		areaCodes.put("450301", "广西壮族自治区桂林市");
		areaCodes.put("450302", "广西壮族自治区桂林市秀峰区");
		areaCodes.put("450303", "广西壮族自治区桂林市叠彩区");
		areaCodes.put("450304", "广西壮族自治区桂林市象山区");
		areaCodes.put("450305", "广西壮族自治区桂林市七星区");
		areaCodes.put("450311", "广西壮族自治区桂林市雁山区");
		areaCodes.put("450321", "广西壮族自治区桂林市阳朔县");
		areaCodes.put("450322", "广西壮族自治区桂林市临桂县");
		areaCodes.put("450323", "广西壮族自治区桂林市灵川县");
		areaCodes.put("450324", "广西壮族自治区桂林市全州县");
		areaCodes.put("450325", "广西壮族自治区桂林市兴安县");
		areaCodes.put("450326", "广西壮族自治区桂林市永福县");
		areaCodes.put("450327", "广西壮族自治区桂林市灌阳县");
		areaCodes.put("450328", "广西壮族自治区桂林市龙胜各族自治县");
		areaCodes.put("450329", "广西壮族自治区桂林市资源县");
		areaCodes.put("450330", "广西壮族自治区桂林市平乐县");
		areaCodes.put("450331", "广西壮族自治区桂林市荔蒲县");
		areaCodes.put("450332", "广西壮族自治区桂林市恭城瑶族自治县");
		areaCodes.put("450400", "广西壮族自治区梧州市");
		areaCodes.put("450401", "广西壮族自治区梧州市");
		areaCodes.put("450403", "广西壮族自治区梧州市万秀区");
		areaCodes.put("450404", "广西壮族自治区梧州市蝶山区");
		areaCodes.put("450405", "广西壮族自治区梧州市长洲区");
		areaCodes.put("450421", "广西壮族自治区梧州市苍梧县");
		areaCodes.put("450422", "广西壮族自治区梧州市藤县");
		areaCodes.put("450423", "广西壮族自治区梧州市蒙山县");
		areaCodes.put("450481", "广西壮族自治区梧州市岑溪市");
		areaCodes.put("450500", "广西壮族自治区北海市");
		areaCodes.put("450501", "广西壮族自治区北海市");
		areaCodes.put("450502", "广西壮族自治区北海市海城区");
		areaCodes.put("450503", "广西壮族自治区北海市银海区");
		areaCodes.put("450512", "广西壮族自治区北海市铁山港区");
		areaCodes.put("450521", "广西壮族自治区北海市合浦县");
		areaCodes.put("450600", "广西壮族自治区防城港市");
		areaCodes.put("450601", "广西壮族自治区防城港市");
		areaCodes.put("450602", "广西壮族自治区防城港市港口区");
		areaCodes.put("450603", "广西壮族自治区防城港市防城区");
		areaCodes.put("450621", "广西壮族自治区防城港市上思县");
		areaCodes.put("450681", "广西壮族自治区防城港市东兴市");
		areaCodes.put("450700", "广西壮族自治区钦州市");
		areaCodes.put("450701", "广西壮族自治区钦州市");
		areaCodes.put("450702", "广西壮族自治区钦州市钦南区");
		areaCodes.put("450703", "广西壮族自治区钦州市钦北区");
		areaCodes.put("450721", "广西壮族自治区钦州市灵山县");
		areaCodes.put("450722", "广西壮族自治区钦州市浦北县");
		areaCodes.put("450800", "广西壮族自治区贵港市");
		areaCodes.put("450801", "广西壮族自治区贵港市");
		areaCodes.put("450802", "广西壮族自治区贵港市港北区");
		areaCodes.put("450803", "广西壮族自治区贵港市港南区");
		areaCodes.put("450804", "广西壮族自治区贵港市覃塘区");
		areaCodes.put("450821", "广西壮族自治区贵港市平南县");
		areaCodes.put("450881", "广西壮族自治区贵港市桂平市");
		areaCodes.put("450900", "广西壮族自治区玉林市");
		areaCodes.put("510683", "四川省德阳市绵竹市");
		areaCodes.put("510700", "四川省绵阳市");
		areaCodes.put("522626", "贵州省黔东南苗族侗族自治州岑巩县");
		areaCodes.put("522627", "贵州省黔东南苗族侗族自治州天柱县");
		areaCodes.put("522628", "贵州省黔东南苗族侗族自治州锦屏县");
		areaCodes.put("522629", "贵州省黔东南苗族侗族自治州剑河县");
		areaCodes.put("522630", "贵州省黔东南苗族侗族自治州台江县");
		areaCodes.put("522631", "贵州省黔东南苗族侗族自治州黎平县");
		areaCodes.put("522632", "贵州省黔东南苗族侗族自治州榕江县");
		areaCodes.put("522633", "贵州省黔东南苗族侗族自治州从江县");
		areaCodes.put("522634", "贵州省黔东南苗族侗族自治州雷山县");
		areaCodes.put("522635", "贵州省黔东南苗族侗族自治州麻江县");
		areaCodes.put("522636", "贵州省黔东南苗族侗族自治州丹寨县");
		areaCodes.put("522700", "贵州省黔南布依族苗族自治州");
		areaCodes.put("522701", "贵州省黔南布依族苗族自治州都匀市");
		areaCodes.put("522702", "贵州省黔南布依族苗族自治州福泉市");
		areaCodes.put("522722", "贵州省黔南布依族苗族自治州荔波县");
		areaCodes.put("522723", "贵州省黔南布依族苗族自治州贵定县");
		areaCodes.put("522725", "贵州省黔南布依族苗族自治州瓮安县");
		areaCodes.put("522726", "贵州省黔南布依族苗族自治州独山县");
		areaCodes.put("522727", "贵州省黔南布依族苗族自治州平塘县");
		areaCodes.put("522728", "贵州省黔南布依族苗族自治州罗甸县");
		areaCodes.put("522729", "贵州省黔南布依族苗族自治州长顺县");
		areaCodes.put("522730", "贵州省黔南布依族苗族自治州龙里县");
		areaCodes.put("522731", "贵州省黔南布依族苗族自治州惠水县");
		areaCodes.put("522732", "贵州省黔南布依族苗族自治州三都水族自治县");
		areaCodes.put("530000", "云南省");
		areaCodes.put("530100", "云南省昆明市");
		areaCodes.put("530101", "云南省昆明市");
		areaCodes.put("530102", "云南省昆明市五华区");
		areaCodes.put("530103", "云南省昆明市盘龙区");
		areaCodes.put("621222", "甘肃省陇南市文县");
		areaCodes.put("621223", "甘肃省陇南市宕昌县");
		areaCodes.put("621224", "甘肃省陇南市康县");
		areaCodes.put("542337", "西藏自治区日喀则地区萨嘎县");
		areaCodes.put("542338", "西藏自治区日喀则地区岗巴县");
		areaCodes.put("610328", "陕西省宝鸡市千阳县");
		areaCodes.put("610329", "陕西省宝鸡市麟游县");
		areaCodes.put("610330", "陕西省宝鸡市凤县");
		areaCodes.put("610331", "陕西省宝鸡市太白县");
		areaCodes.put("610400", "陕西省咸阳市");
		areaCodes.put("610401", "陕西省咸阳市");
		areaCodes.put("610402", "陕西省咸阳市秦都区");
		areaCodes.put("610403", "陕西省咸阳市杨凌区");
		areaCodes.put("610404", "陕西省咸阳市渭城区");
		areaCodes.put("610422", "陕西省咸阳市三原县");
		areaCodes.put("610423", "陕西省咸阳市泾阳县");
		areaCodes.put("610424", "陕西省咸阳市乾县");
		areaCodes.put("610425", "陕西省咸阳市礼泉县");
		areaCodes.put("610426", "陕西省咸阳市永寿县");
		areaCodes.put("610427", "陕西省咸阳市彬县");
		areaCodes.put("610428", "陕西省咸阳市长武县");
		areaCodes.put("610429", "陕西省咸阳市旬邑县");
		areaCodes.put("610430", "陕西省咸阳市淳化县");
		areaCodes.put("610431", "陕西省咸阳市武功县");
		areaCodes.put("610481", "陕西省咸阳市兴平市");
		areaCodes.put("610500", "陕西省渭南市");
		areaCodes.put("610501", "陕西省渭南市");
		areaCodes.put("610502", "陕西省渭南市临渭区");
		areaCodes.put("610521", "陕西省渭南市华县");
		areaCodes.put("610522", "陕西省渭南市潼关县");
		areaCodes.put("610523", "陕西省渭南市大荔县");
		areaCodes.put("610524", "陕西省渭南市合阳县");
		areaCodes.put("610525", "陕西省渭南市澄城县");
		areaCodes.put("610526", "陕西省渭南市蒲城县");
		areaCodes.put("610527", "陕西省渭南市白水县");
		areaCodes.put("610528", "陕西省渭南市富平县");
		areaCodes.put("610581", "陕西省渭南市韩城市");
		areaCodes.put("610582", "陕西省渭南市华阴市");
		areaCodes.put("610600", "陕西省延安市");
		areaCodes.put("610601", "陕西省延安市");
		areaCodes.put("610602", "陕西省延安市宝塔区");
		areaCodes.put("610621", "陕西省延安市延长县");
		areaCodes.put("610622", "陕西省延安市延川县");
		areaCodes.put("610623", "陕西省延安市子长县");
		areaCodes.put("610624", "陕西省延安市安塞县");
		areaCodes.put("610625", "陕西省延安市志丹县");
		areaCodes.put("610626", "陕西省延安市吴旗县");
		areaCodes.put("610627", "陕西省延安市甘泉县");
		areaCodes.put("610628", "陕西省延安市富县");
		areaCodes.put("610629", "陕西省延安市洛川县");
		areaCodes.put("610630", "陕西省延安市宜川县");
		areaCodes.put("610631", "陕西省延安市黄龙县");
		areaCodes.put("610632", "陕西省延安市黄陵县");
		areaCodes.put("610700", "陕西省汉中市");
		areaCodes.put("610701", "陕西省汉中市");
		areaCodes.put("610702", "陕西省汉中市汉台区");
		areaCodes.put("610721", "陕西省汉中市南郑县");
		areaCodes.put("610722", "陕西省汉中市城固县");
		areaCodes.put("610723", "陕西省汉中市洋县");
		areaCodes.put("610724", "陕西省汉中市西乡县");
		areaCodes.put("610725", "陕西省汉中市勉县");
		areaCodes.put("610726", "陕西省汉中市宁强县");
		areaCodes.put("620501", "甘肃省天水市");
		areaCodes.put("620502", "甘肃省天水市秦城区");
		areaCodes.put("620503", "甘肃省天水市北道区");
		areaCodes.put("620521", "甘肃省天水市清水县");
		areaCodes.put("620522", "甘肃省天水市秦安县");
		areaCodes.put("542400", "西藏自治区那曲地区");
		areaCodes.put("542421", "西藏自治区那曲地区那曲县");
		areaCodes.put("542422", "西藏自治区那曲地区嘉黎县");
		areaCodes.put("542423", "西藏自治区那曲地区比如县");
		areaCodes.put("542424", "西藏自治区那曲地区聂荣县");
		areaCodes.put("542425", "西藏自治区那曲地区安多县");
		areaCodes.put("542426", "西藏自治区那曲地区申扎县");
		areaCodes.put("542427", "西藏自治区那曲地区索县");
		areaCodes.put("542428", "西藏自治区那曲地区班戈县");
		areaCodes.put("542429", "西藏自治区那曲地区巴青县");
		areaCodes.put("542430", "西藏自治区那曲地区尼玛县");
		areaCodes.put("542500", "西藏自治区阿里地区");
		areaCodes.put("542521", "西藏自治区阿里地区普兰县");
		areaCodes.put("542522", "西藏自治区阿里地区札达县");
		areaCodes.put("542523", "西藏自治区阿里地区噶尔县");
		areaCodes.put("542524", "西藏自治区阿里地区日土县");
		areaCodes.put("542525", "西藏自治区阿里地区革吉县");
		areaCodes.put("542526", "西藏自治区阿里地区改则县");
		areaCodes.put("542527", "西藏自治区阿里地区措勤县");
		areaCodes.put("542600", "西藏自治区林芝地区");
		areaCodes.put("542621", "西藏自治区林芝地区林芝县");
		areaCodes.put("542622", "西藏自治区林芝地区工布江达县");
		areaCodes.put("542623", "西藏自治区林芝地区米林县");
		areaCodes.put("542624", "西藏自治区林芝地区墨脱县");
		areaCodes.put("542625", "西藏自治区林芝地区波密县");
		areaCodes.put("542626", "西藏自治区林芝地区察隅县");
		areaCodes.put("542627", "西藏自治区林芝地区朗县");
		areaCodes.put("610000", "陕西省");
		areaCodes.put("610100", "陕西省西安市");
		areaCodes.put("610101", "陕西省西安市");
		areaCodes.put("610102", "陕西省西安市新城区");
		areaCodes.put("610103", "陕西省西安市碑林区");
		areaCodes.put("610104", "陕西省西安市莲湖区");
		areaCodes.put("610111", "陕西省西安市灞桥区");
		areaCodes.put("610112", "陕西省西安市未央区");
		areaCodes.put("610113", "陕西省西安市雁塔区");
		areaCodes.put("610114", "陕西省西安市阎良区");
		areaCodes.put("610115", "陕西省西安市临潼区");
		areaCodes.put("610116", "陕西省西安市长安区");
		areaCodes.put("610122", "陕西省西安市蓝田县");
		areaCodes.put("610124", "陕西省西安市周至县");
		areaCodes.put("610125", "陕西省西安市户县");
		areaCodes.put("610126", "陕西省西安市高陵县");
		areaCodes.put("610200", "陕西省铜川市");
		areaCodes.put("610201", "陕西省铜川市");
		areaCodes.put("610202", "陕西省铜川市王益区");
		areaCodes.put("610203", "陕西省铜川市印台区");
		areaCodes.put("610204", "陕西省铜川市耀州区");
		areaCodes.put("610222", "陕西省铜川市宜君县");
		areaCodes.put("610300", "陕西省宝鸡市");
		areaCodes.put("610301", "陕西省宝鸡市");
		areaCodes.put("610302", "陕西省宝鸡市渭滨区");
		areaCodes.put("610303", "陕西省宝鸡市金台区");
		areaCodes.put("654326", "新疆维吾尔自治区阿勒泰地区吉木乃县");
		areaCodes.put("632525", "青海省海南藏族自治州贵南县");
		areaCodes.put("650104", "新疆维吾尔自治区乌鲁木齐市新市区");
		areaCodes.put("650105", "新疆维吾尔自治区乌鲁木齐市水磨沟区");
		areaCodes.put("650106", "新疆维吾尔自治区乌鲁木齐市头屯河区");
		areaCodes.put("621225", "甘肃省陇南市西和县");
		areaCodes.put("621226", "甘肃省陇南市礼县");
		areaCodes.put("621227", "甘肃省陇南市徽县");
		areaCodes.put("621228", "甘肃省陇南市两当县");
		areaCodes.put("622900", "甘肃省临夏回族自治州");
		areaCodes.put("622901", "甘肃省临夏回族自治州临夏市");
		areaCodes.put("622921", "甘肃省临夏回族自治州临夏县");
		areaCodes.put("622922", "甘肃省临夏回族自治州康乐县");
		areaCodes.put("622923", "甘肃省临夏回族自治州永靖县");
		areaCodes.put("622924", "甘肃省临夏回族自治州广河县");
		areaCodes.put("622925", "甘肃省临夏回族自治州和政县");
		areaCodes.put("622926", "甘肃省临夏回族自治州东乡族自治县");
		areaCodes.put("622927", "甘肃省临夏回族自治州积石山保安族东乡族撒拉族自治县");
		areaCodes.put("623000", "甘肃省甘南藏族自治州");
		areaCodes.put("623001", "甘肃省甘南藏族自治州合作市");
		areaCodes.put("623021", "甘肃省甘南藏族自治州临潭县");
		areaCodes.put("623022", "甘肃省甘南藏族自治州卓尼县");
		areaCodes.put("623023", "甘肃省甘南藏族自治州舟曲县");
		areaCodes.put("623024", "甘肃省甘南藏族自治州迭部县");
		areaCodes.put("623025", "甘肃省甘南藏族自治州玛曲县");
		areaCodes.put("623026", "甘肃省甘南藏族自治州碌曲县");
		areaCodes.put("623027", "甘肃省甘南藏族自治州夏河县");
		areaCodes.put("630000", "青海省");
		areaCodes.put("630100", "青海省西宁市");
		areaCodes.put("630101", "青海省西宁市");
		areaCodes.put("630102", "青海省西宁市城东区");
		areaCodes.put("630103", "青海省西宁市城中区");
		areaCodes.put("630104", "青海省西宁市城西区");
		areaCodes.put("630105", "青海省西宁市城北区");
		areaCodes.put("630121", "青海省西宁市大通回族土族自治县");
		areaCodes.put("630122", "青海省西宁市湟中县");
		areaCodes.put("630123", "青海省西宁市湟源县");
		areaCodes.put("632100", "青海省海东地区");
		areaCodes.put("632121", "青海省海东地区平安县");
		areaCodes.put("632122", "青海省海东地区民和回族土族自治县");
		areaCodes.put("632123", "青海省海东地区乐都县");
		areaCodes.put("632126", "青海省海东地区互助土族自治县");
		areaCodes.put("632127", "青海省海东地区化隆回族自治县");
		areaCodes.put("632128", "青海省海东地区循化撒拉族自治县");
		areaCodes.put("632200", "青海省海北藏族自治州");
		areaCodes.put("632221", "青海省海北藏族自治州门源回族自治县");
		areaCodes.put("632222", "青海省海北藏族自治州祁连县");
		areaCodes.put("632223", "青海省海北藏族自治州海晏县");
		areaCodes.put("632224", "青海省海北藏族自治州刚察县");
		areaCodes.put("632300", "青海省黄南藏族自治州");
		areaCodes.put("632321", "青海省黄南藏族自治州同仁县");
		areaCodes.put("632322", "青海省黄南藏族自治州尖扎县");
		areaCodes.put("632323", "青海省黄南藏族自治州泽库县");
		areaCodes.put("632324", "青海省黄南藏族自治州河南蒙古族自治县");
		areaCodes.put("632500", "青海省海南藏族自治州");
		areaCodes.put("632521", "青海省海南藏族自治州共和县");
		areaCodes.put("632522", "青海省海南藏族自治州同德县");
		areaCodes.put("632523", "青海省海南藏族自治州贵德县");
		areaCodes.put("632524", "青海省海南藏族自治州兴海县");
		areaCodes.put("652929", "新疆维吾尔自治区阿克苏地区柯坪县");
		areaCodes.put("610322", "陕西省宝鸡市凤翔县");
		areaCodes.put("610323", "陕西省宝鸡市岐山县");
		areaCodes.put("610324", "陕西省宝鸡市扶风县");
		areaCodes.put("610326", "陕西省宝鸡市眉县");
		areaCodes.put("610327", "陕西省宝鸡市陇县");
		areaCodes.put("650107", "新疆维吾尔自治区乌鲁木齐市达坂城区");
		areaCodes.put("620525", "甘肃省天水市张家川回族自治县");
		areaCodes.put("620600", "甘肃省武威市");
		areaCodes.put("620601", "甘肃省武威市");
		areaCodes.put("620602", "甘肃省武威市凉州区");
		areaCodes.put("620621", "甘肃省武威市民勤县");
		areaCodes.put("620622", "甘肃省武威市古浪县");
		areaCodes.put("620623", "甘肃省武威市天祝藏族自治县");
		areaCodes.put("620700", "甘肃省张掖市");
		areaCodes.put("620701", "甘肃省张掖市");
		areaCodes.put("620702", "甘肃省张掖市甘州区");
		areaCodes.put("620721", "甘肃省张掖市肃南裕固族自治县");
		areaCodes.put("620722", "甘肃省张掖市民乐县");
		areaCodes.put("620723", "甘肃省张掖市临泽县");
		areaCodes.put("620724", "甘肃省张掖市高台县");
		areaCodes.put("620725", "甘肃省张掖市山丹县");
		areaCodes.put("620800", "甘肃省平凉市");
		areaCodes.put("620801", "甘肃省平凉市");
		areaCodes.put("620802", "甘肃省平凉市崆峒区");
		areaCodes.put("620821", "甘肃省平凉市泾川县");
		areaCodes.put("620822", "甘肃省平凉市灵台县");
		areaCodes.put("620823", "甘肃省平凉市崇信县");
		areaCodes.put("620824", "甘肃省平凉市华亭县");
		areaCodes.put("620825", "甘肃省平凉市庄浪县");
		areaCodes.put("620826", "甘肃省平凉市静宁县");
		areaCodes.put("620900", "甘肃省酒泉市");
		areaCodes.put("620901", "甘肃省酒泉市");
		areaCodes.put("620902", "甘肃省酒泉市肃州区");
		areaCodes.put("620921", "甘肃省酒泉市金塔县");
		areaCodes.put("620922", "甘肃省酒泉市安西县");
		areaCodes.put("620923", "甘肃省酒泉市肃北蒙古族自治县");
		areaCodes.put("620924", "甘肃省酒泉市阿克塞哈萨克族自治县");
		areaCodes.put("620981", "甘肃省酒泉市玉门市");
		areaCodes.put("620982", "甘肃省酒泉市敦煌市");
		areaCodes.put("621000", "甘肃省庆阳市");
		areaCodes.put("621001", "甘肃省庆阳市");
		areaCodes.put("621002", "甘肃省庆阳市西峰区");
		areaCodes.put("621021", "甘肃省庆阳市庆城县");
		areaCodes.put("621022", "甘肃省庆阳市环县");
		areaCodes.put("621023", "甘肃省庆阳市华池县");
		areaCodes.put("621024", "甘肃省庆阳市合水县");
		areaCodes.put("621025", "甘肃省庆阳市正宁县");
		areaCodes.put("621026", "甘肃省庆阳市宁县");
		areaCodes.put("621027", "甘肃省庆阳市镇原县");
		areaCodes.put("621100", "甘肃省定西市");
		areaCodes.put("621101", "甘肃省定西市");
		areaCodes.put("621102", "甘肃省定西市安定区");
		areaCodes.put("621121", "甘肃省定西市通渭县");
		areaCodes.put("621122", "甘肃省定西市陇西县");
		areaCodes.put("621123", "甘肃省定西市渭源县");
		areaCodes.put("621124", "甘肃省定西市临洮县");
		areaCodes.put("621125", "甘肃省定西市漳县");
		areaCodes.put("621126", "甘肃省定西市岷县");
		areaCodes.put("621200", "甘肃省陇南市");
		areaCodes.put("621201", "甘肃省陇南市");
		areaCodes.put("621202", "甘肃省陇南市武都区");
		areaCodes.put("621221", "甘肃省陇南市成县");
		areaCodes.put("653000", "新疆维吾尔自治区克孜勒苏柯尔克孜自治州");
		areaCodes.put("653001", "新疆维吾尔自治区克孜勒苏柯尔克孜自治州阿图什市");
		areaCodes.put("653022", "新疆维吾尔自治区克孜勒苏柯尔克孜自治州阿克陶县");
		areaCodes.put("653023", "新疆维吾尔自治区克孜勒苏柯尔克孜自治州阿合奇县");
		areaCodes.put("659003", "新疆维吾尔自治区图木舒克市");
		areaCodes.put("659004", "新疆维吾尔自治区五家渠市");
		areaCodes.put("710000", "台湾省");
		areaCodes.put("810000", "香港特别行政区");
		areaCodes.put("820000", "澳门特别行政区");
		areaCodes.put("A00000", "亚洲");
		areaCodes.put("B00000", "非洲");
		areaCodes.put("C00000", "欧洲");
		areaCodes.put("D00000", "美洲");
		areaCodes.put("E00000", "大洋洲");
		areaCodes.put("F01000", "南极洲");
		areaCodes.put("ZZZZZZ", "其它");
		areaCodes.put("522425", "贵州省毕节地区织金县");
		areaCodes.put("522426", "贵州省毕节地区纳雍县");
		areaCodes.put("522427", "贵州省毕节地区威宁彝族回族苗族自治县");
		areaCodes.put("522428", "贵州省毕节地区赫章县");
		areaCodes.put("522600", "贵州省黔东南苗族侗族自治州");
		areaCodes.put("522601", "贵州省黔东南苗族侗族自治州凯里市");
		areaCodes.put("522622", "贵州省黔东南苗族侗族自治州黄平县");
		areaCodes.put("522623", "贵州省黔东南苗族侗族自治州施秉县");
		areaCodes.put("522624", "贵州省黔东南苗族侗族自治州三穗县");
		areaCodes.put("513230", "四川省阿坝藏族羌族自治州壤塘县");
		areaCodes.put("511425", "四川省眉山市青神县");
		areaCodes.put("511500", "四川省宜宾市");
		areaCodes.put("511501", "四川省宜宾市");
		areaCodes.put("511502", "四川省宜宾市翠屏区");
		areaCodes.put("511521", "四川省宜宾市宜宾县");
		areaCodes.put("511522", "四川省宜宾市南溪县");
		areaCodes.put("511523", "四川省宜宾市江安县");
		areaCodes.put("511524", "四川省宜宾市长宁县");
		areaCodes.put("511525", "四川省宜宾市高县");
		areaCodes.put("511526", "四川省宜宾市珙县");
		areaCodes.put("511527", "四川省宜宾市筠连县");
		areaCodes.put("511528", "四川省宜宾市兴文县");
		areaCodes.put("511529", "四川省宜宾市屏山县");
		areaCodes.put("511600", "四川省广安市");
		areaCodes.put("511601", "四川省广安市");
		areaCodes.put("511602", "四川省广安市广安区");
		areaCodes.put("511621", "四川省广安市岳池县");
		areaCodes.put("511622", "四川省广安市武胜县");
		areaCodes.put("511623", "四川省广安市邻水县");
		areaCodes.put("511681", "四川省广安市华莹市");
		areaCodes.put("511700", "四川省达州市");
		areaCodes.put("511701", "四川省达州市");
		areaCodes.put("511702", "四川省达州市通川区");
		areaCodes.put("511721", "四川省达州市达县");
		areaCodes.put("511722", "四川省达州市宣汉县");
		areaCodes.put("511723", "四川省达州市开江县");
		areaCodes.put("511724", "四川省达州市大竹县");
		areaCodes.put("511725", "四川省达州市渠县");
		areaCodes.put("511781", "四川省达州市万源市");
		areaCodes.put("511800", "四川省雅安市");
		areaCodes.put("511801", "四川省雅安市");
		areaCodes.put("511802", "四川省雅安市雨城区");
		areaCodes.put("511821", "四川省雅安市名山县");
		areaCodes.put("511822", "四川省雅安市荥经县");
		areaCodes.put("511823", "四川省雅安市汉源县");
		areaCodes.put("511824", "四川省雅安市石棉县");
		areaCodes.put("511825", "四川省雅安市天全县");
		areaCodes.put("511826", "四川省雅安市芦山县");
		areaCodes.put("511827", "四川省雅安市宝兴县");
		areaCodes.put("511900", "四川省巴中市");
		areaCodes.put("511901", "四川省巴中市");
		areaCodes.put("511902", "四川省巴中市巴州区");
		areaCodes.put("511921", "四川省巴中市通江县");
		areaCodes.put("511922", "四川省巴中市南江县");
		areaCodes.put("511923", "四川省巴中市平昌县");
		areaCodes.put("512000", "四川省资阳市");
		areaCodes.put("512001", "四川省资阳市");
		areaCodes.put("512002", "四川省资阳市雁江区");
		areaCodes.put("512021", "四川省资阳市安岳县");
		areaCodes.put("512022", "四川省资阳市乐至县");
		areaCodes.put("512081", "四川省资阳市简阳市");
		areaCodes.put("513200", "四川省阿坝藏族羌族自治州");
		areaCodes.put("513221", "四川省阿坝藏族羌族自治州汶川县");
		areaCodes.put("513222", "四川省阿坝藏族羌族自治州理县");
		areaCodes.put("513223", "四川省阿坝藏族羌族自治州茂县");
		areaCodes.put("513224", "四川省阿坝藏族羌族自治州松潘县");
		areaCodes.put("513225", "四川省阿坝藏族羌族自治州九寨沟县");
		areaCodes.put("513226", "四川省阿坝藏族羌族自治州金川县");
		areaCodes.put("532300", "云南省楚雄彝族自治州");
		areaCodes.put("532301", "云南省楚雄彝族自治州楚雄市");
		areaCodes.put("532322", "云南省楚雄彝族自治州双柏县");
		areaCodes.put("530423", "云南省玉溪市通海县");
		areaCodes.put("513231", "四川省阿坝藏族羌族自治州阿坝县");
		areaCodes.put("513232", "四川省阿坝藏族羌族自治州若尔盖县");
		areaCodes.put("513233", "四川省阿坝藏族羌族自治州红原县");
		areaCodes.put("513300", "四川省甘孜藏族自治州");
		areaCodes.put("513321", "四川省甘孜藏族自治州康定县");
		areaCodes.put("513322", "四川省甘孜藏族自治州泸定县");
		areaCodes.put("513323", "四川省甘孜藏族自治州丹巴县");
		areaCodes.put("513324", "四川省甘孜藏族自治州九龙县");
		areaCodes.put("513325", "四川省甘孜藏族自治州雅江县");
		areaCodes.put("513326", "四川省甘孜藏族自治州道孚县");
		areaCodes.put("513327", "四川省甘孜藏族自治州炉霍县");
		areaCodes.put("513328", "四川省甘孜藏族自治州甘孜县");
		areaCodes.put("513329", "四川省甘孜藏族自治州新龙县");
		areaCodes.put("513330", "四川省甘孜藏族自治州德格县");
		areaCodes.put("513331", "四川省甘孜藏族自治州白玉县");
		areaCodes.put("513332", "四川省甘孜藏族自治州石渠县");
		areaCodes.put("513333", "四川省甘孜藏族自治州色达县");
		areaCodes.put("513334", "四川省甘孜藏族自治州理塘县");
		areaCodes.put("513335", "四川省甘孜藏族自治州巴塘县");
		areaCodes.put("513336", "四川省甘孜藏族自治州乡城县");
		areaCodes.put("513337", "四川省甘孜藏族自治州稻城县");
		areaCodes.put("513338", "四川省甘孜藏族自治州得荣县");
		areaCodes.put("513400", "四川省凉山彝族自治州");
		areaCodes.put("513401", "四川省凉山彝族自治州西昌市");
		areaCodes.put("513422", "四川省凉山彝族自治州木里藏族自治县");
		areaCodes.put("513423", "四川省凉山彝族自治州盐源县");
		areaCodes.put("513424", "四川省凉山彝族自治州德昌县");
		areaCodes.put("513425", "四川省凉山彝族自治州会理县");
		areaCodes.put("513426", "四川省凉山彝族自治州会东县");
		areaCodes.put("513427", "四川省凉山彝族自治州宁南县");
		areaCodes.put("513428", "四川省凉山彝族自治州普格县");
		areaCodes.put("513429", "四川省凉山彝族自治州布拖县");
		areaCodes.put("513430", "四川省凉山彝族自治州金阳县");
		areaCodes.put("513431", "四川省凉山彝族自治州昭觉县");
		areaCodes.put("513432", "四川省凉山彝族自治州喜德县");

		twFirstCode.put("A", 10);
		twFirstCode.put("B", 11);
		twFirstCode.put("C", 12);
		twFirstCode.put("D", 13);
		twFirstCode.put("E", 14);
		twFirstCode.put("F", 15);
		twFirstCode.put("G", 16);
		twFirstCode.put("H", 17);
		twFirstCode.put("J", 18);
		twFirstCode.put("K", 19);
		twFirstCode.put("L", 20);
		twFirstCode.put("M", 21);
		twFirstCode.put("N", 22);
		twFirstCode.put("P", 23);
		twFirstCode.put("Q", 24);
		twFirstCode.put("R", 25);
		twFirstCode.put("S", 26);
		twFirstCode.put("T", 27);
		twFirstCode.put("U", 28);
		twFirstCode.put("V", 29);
		twFirstCode.put("X", 30);
		twFirstCode.put("Y", 31);
		twFirstCode.put("W", 32);
		twFirstCode.put("Z", 33);
		twFirstCode.put("I", 34);
		twFirstCode.put("O", 35);
		hkFirstCode.put("A", 1);
		hkFirstCode.put("B", 2);
		hkFirstCode.put("C", 3);
		hkFirstCode.put("R", 18);
		hkFirstCode.put("U", 21);
		hkFirstCode.put("Z", 26);
		hkFirstCode.put("X", 24);
		hkFirstCode.put("W", 23);
		hkFirstCode.put("O", 15);
		hkFirstCode.put("N", 14);
	}

	/**
	 * 将15位身份证号码转换为18位
	 * 
	 * @param idCard
	 *            15位身份编码
	 * @return 18位身份编码
	 */
	private static String conver15CardTo18(String idCard) {
		String idCard18 = "";
		if (idCard.length() != CHINA_ID_MIN_LENGTH) {
			return null;
		}
		if (isNum(idCard)) {
			// 获取出生年月日
			String birthday = idCard.substring(6, 12);
			Date birthDate = null;
			try {
				birthDate = new SimpleDateFormat("yyMMdd").parse(birthday);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Calendar cal = Calendar.getInstance();
			if (birthDate != null)
				cal.setTime(birthDate);
			// 获取出生年(完全表现形式,如：2010)
			String sYear = String.valueOf(cal.get(Calendar.YEAR));
			idCard18 = idCard.substring(0, 6) + sYear + idCard.substring(8);
			// 转换字符数组
			char[] cArr = idCard18.toCharArray();
			if (cArr != null) {
				int[] iCard = converCharToInt(cArr);
				int iSum17 = getPowerSum(iCard);
				// 获取校验位
				String sVal = getCheckCode18(iSum17);
				if (sVal.length() > 0) {
					idCard18 += sVal;
				} else {
					return null;
				}
			}
		} else {
			return null;
		}
		return idCard18;
	}

	/**
	 * 验证身份证是否合法
	 */
	public static boolean validateCard(String idCard) {
		String card = idCard.trim();
		if (validateIdCard18(card)) {
			return true;
		}
		if (validateIdCard15(card)) {
			return true;
		}
		String[] cardval = validateIdCard10(card);
		if (cardval != null) {
			if (cardval[2].equals("true")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 验证18位身份编码是否合法
	 * 
	 * @param idCard
	 *            身份编码
	 * @return 是否合法
	 */
	private static boolean validateIdCard18(String idCard) {
		boolean bTrue = false;
		if (idCard.length() == CHINA_ID_MAX_LENGTH) {
			// 前17位
			String code17 = idCard.substring(0, 17);
			// 第18位
			String code18 = idCard.substring(17, CHINA_ID_MAX_LENGTH);
			if (isNum(code17)) {
				char[] cArr = code17.toCharArray();
				if (cArr != null) {
					int[] iCard = converCharToInt(cArr);
					int iSum17 = getPowerSum(iCard);
					// 获取校验位
					String val = getCheckCode18(iSum17);
					if (val.length() > 0) {
						if (val.equalsIgnoreCase(code18)) {
							bTrue = true;
						}
					}
				}
			}
		}
		return bTrue;
	}

	/**
	 * 验证15位身份编码是否合法
	 * 
	 * @param idCard
	 *            身份编码
	 * @return 是否合法
	 */
	private static boolean validateIdCard15(String idCard) {
		if (idCard.length() != CHINA_ID_MIN_LENGTH) {
			return false;
		}
		if (isNum(idCard)) {
			String proCode = idCard.substring(0, 2);
			if (areaCodes.get(proCode) == null) {
				return false;
			}
			String birthCode = idCard.substring(6, 12);
			Date birthDate = null;
			try {
				birthDate = new SimpleDateFormat("yy").parse(birthCode
						.substring(0, 2));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Calendar cal = Calendar.getInstance();
			if (birthDate != null)
				cal.setTime(birthDate);
			if (!valiDate(cal.get(Calendar.YEAR),
					Integer.valueOf(birthCode.substring(2, 4)),
					Integer.valueOf(birthCode.substring(4, 6)))) {
				return false;
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * 验证10位身份编码是否合法
	 * 
	 * @param idCard
	 *            身份编码
	 * @return 身份证信息数组
	 *         <p>
	 *         [0] - 台湾、澳门、香港 [1] - 性别(男M,女F,未知N) [2] - 是否合法(合法true,不合法false)
	 *         若不是身份证件号码则返回null
	 *         </p>
	 */
	private static String[] validateIdCard10(String idCard) {
		String[] info = new String[3];
		String card = idCard.replaceAll("[\\(|\\)]", "");
		if (card.length() != 8 && card.length() != 9 && idCard.length() != 10) {
			return null;
		}
		if (idCard.matches("^[a-zA-Z][0-9]{9}{1}")) { // 台湾
			info[0] = "台湾";
			String char2 = idCard.substring(1, 2);
			if (char2.equals("1")) {
				info[1] = "M";
			} else if (char2.equals("2")) {
				info[1] = "F";
			} else {
				info[1] = "N";
				info[2] = "false";
				return info;
			}
			info[2] = validateTWCard(idCard) ? "true" : "false";
		} else if (idCard.matches("^[1|5|7][0-9]{6}\\(?[0-9A-Z]\\)?{1}")) { // 澳门
			info[0] = "澳门";
			info[1] = "N";
			// TODO
		} else if (idCard.matches("^[A-Z]{1,2}[0-9]{6}\\(?[0-9A]\\)?{1}")) { // 香港
			info[0] = "香港";
			info[1] = "N";
			info[2] = validateHKCard(idCard) ? "true" : "false";
		} else {
			return null;
		}
		return info;
	}

	/**
	 * 验证台湾身份证号码
	 * 
	 * @param idCard
	 *            身份证号码
	 * @return 验证码是否符合
	 */
	private static boolean validateTWCard(String idCard) {
		String start = idCard.substring(0, 1);
		String mid = idCard.substring(1, 9);
		String end = idCard.substring(9, 10);
		Integer iStart = twFirstCode.get(start);
		Integer sum = iStart / 10 + (iStart % 10) * 9;
		char[] chars = mid.toCharArray();
		Integer iflag = 8;
		for (char c : chars) {
			sum = sum + Integer.valueOf(c + "") * iflag;
			iflag--;
		}
		return (sum % 10 == 0 ? 0 : (10 - sum % 10)) == Integer.valueOf(end) ? true
				: false;
	}

	/**
	 * 验证香港身份证号码(存在Bug，部份特殊身份证无法检查)
	 * <p>
	 * 身份证前2位为英文字符，如果只出现一个英文字符则表示第一位是空格，对应数字58 前2位英文字符A-Z分别对应数字10-35
	 * 最后一位校验码为0-9的数字加上字符"A"，"A"代表10
	 * </p>
	 * <p>
	 * 将身份证号码全部转换为数字，分别对应乘9-1相加的总和，整除11则证件号码有效
	 * </p>
	 * 
	 * @param idCard
	 *            身份证号码
	 * @return 验证码是否符合
	 */
	private static boolean validateHKCard(String idCard) {
		String card = idCard.replaceAll("[\\(|\\)]", "");
		Integer sum = 0;
		if (card.length() == 9) {
			sum = (Integer.valueOf(card.substring(0, 1).toUpperCase()
					.toCharArray()[0]) - 55)
					* 9
					+ (Integer.valueOf(card.substring(1, 2).toUpperCase()
							.toCharArray()[0]) - 55) * 8;
			card = card.substring(1, 9);
		} else {
			sum = 522 + (Integer.valueOf(card.substring(0, 1).toUpperCase()
					.toCharArray()[0]) - 55) * 8;
		}
		String mid = card.substring(1, 7);
		String end = card.substring(7, 8);
		char[] chars = mid.toCharArray();
		Integer iflag = 7;
		for (char c : chars) {
			sum = sum + Integer.valueOf(c + "") * iflag;
			iflag--;
		}
		if (end.toUpperCase().equals("A")) {
			sum = sum + 10;
		} else {
			sum = sum + Integer.valueOf(end);
		}
		return (sum % 11 == 0) ? true : false;
	}

	/**
	 * 将字符数组转换成数字数组
	 * 
	 * @param ca
	 *            字符数组
	 * @return 数字数组
	 */
	private static int[] converCharToInt(char[] ca) {
		int len = ca.length;
		int[] iArr = new int[len];
		try {
			for (int i = 0; i < len; i++) {
				iArr[i] = Integer.parseInt(String.valueOf(ca[i]));
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return iArr;
	}

	/**
	 * 将身份证的每位和对应位的加权因子相乘之后，再得到和值
	 * 
	 * @param iArr
	 * @return 身份证编码。
	 */
	private static int getPowerSum(int[] iArr) {
		int iSum = 0;
		if (power.length == iArr.length) {
			for (int i = 0; i < iArr.length; i++) {
				for (int j = 0; j < power.length; j++) {
					if (i == j) {
						iSum = iSum + iArr[i] * power[j];
					}
				}
			}
		}
		return iSum;
	}

	/**
	 * 将power和值与11取模获得余数进行校验码判断
	 * 
	 * @param iSum
	 * @return 校验位
	 */
	private static String getCheckCode18(int iSum) {
		String sCode = "";
		switch (iSum % 11) {
		case 10:
			sCode = "2";
			break;
		case 9:
			sCode = "3";
			break;
		case 8:
			sCode = "4";
			break;
		case 7:
			sCode = "5";
			break;
		case 6:
			sCode = "6";
			break;
		case 5:
			sCode = "7";
			break;
		case 4:
			sCode = "8";
			break;
		case 3:
			sCode = "9";
			break;
		case 2:
			sCode = "x";
			break;
		case 1:
			sCode = "0";
			break;
		case 0:
			sCode = "1";
			break;
		}
		return sCode;
	}

	/**
	 * 根据身份编号获取年龄
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 年龄
	 */
	public static int getAge(String idCard) {
		int iAge = 0;
		if (idCard.length() == CHINA_ID_MIN_LENGTH) {
			idCard = conver15CardTo18(idCard);
		}
		String year = idCard.substring(6, 10);
		Calendar cal = Calendar.getInstance();
		int iCurrYear = cal.get(Calendar.YEAR);
		iAge = iCurrYear - Integer.valueOf(year);
		return iAge;
	}

	/**
	 * 根据身份编号获取生日
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 生日(yyyyMMdd)
	 */
	public static String getBirthday(String idCard) {
		Integer len = idCard.length();
		if (len < CHINA_ID_MIN_LENGTH) {
			return null;
		} else if (len == CHINA_ID_MIN_LENGTH) {
			idCard = conver15CardTo18(idCard);
		}
		return idCard.substring(6, 14);
	}

	/**
	 * 根据身份编号获取生日年
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 生日(yyyy)
	 */
	public static Short getYear(String idCard) {
		Integer len = idCard.length();
		if (len < CHINA_ID_MIN_LENGTH) {
			return null;
		} else if (len == CHINA_ID_MIN_LENGTH) {
			idCard = conver15CardTo18(idCard);
		}
		return Short.valueOf(idCard.substring(6, 10));
	}

	/**
	 * 根据身份编号获取生日月
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 生日(MM)
	 */
	public static Short getMonth(String idCard) {
		Integer len = idCard.length();
		if (len < CHINA_ID_MIN_LENGTH) {
			return null;
		} else if (len == CHINA_ID_MIN_LENGTH) {
			idCard = conver15CardTo18(idCard);
		}
		return Short.valueOf(idCard.substring(10, 12));
	}

	/**
	 * 根据身份编号获取生日天
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 生日(dd)
	 */
	public static Short getDate(String idCard) {
		Integer len = idCard.length();
		if (len < CHINA_ID_MIN_LENGTH) {
			return null;
		} else if (len == CHINA_ID_MIN_LENGTH) {
			idCard = conver15CardTo18(idCard);
		}
		return Short.valueOf(idCard.substring(12, 14));
	}

	/**
	 * 根据身份编号获取性别
	 * 
	 * @param idCard
	 *            身份编号
	 * @return 性别(M-男，F-女，N-未知)
	 */
	public static String getGender(String idCard) {
		String sGender = "N";
		if (idCard.length() == CHINA_ID_MIN_LENGTH) {
			idCard = conver15CardTo18(idCard);
		}
		String sCardNum = idCard.substring(16, 17);
		if (Integer.parseInt(sCardNum) % 2 != 0) {
			sGender = "M";
		} else {
			sGender = "F";
		}
		return sGender;
	}

	/**
	 * 根据身份编号获取户籍省份
	 * 
	 * @param idCard
	 *            身份编码
	 * @return 省级编码。
	 */
	public static String getProvince(String idCard) {
		int len = idCard.length();
		if (len == CHINA_ID_MIN_LENGTH || len == CHINA_ID_MAX_LENGTH)
			return areaCodes.get(idCard.substring(0, 2));
		return "";
	}

	/**
	 * 根据身份编号获取籍贯
	 * 
	 * @param idCard
	 * @return
	 */
	public static String getArea(String idCard) {
		int len = idCard.length();
		if (len == CHINA_ID_MIN_LENGTH || len == CHINA_ID_MAX_LENGTH)
			return areaCodes.get(idCard.substring(0, 6));
		return "";
	}

	/**
	 * 数字验证
	 * 
	 * @param val
	 * @return 提取的数字。
	 */
	private static boolean isNum(String val) {
		return val == null || "".equals(val) ? false : val
				.matches("^[0-9]*{1}");
	}

	/**
	 * 验证小于当前日期 是否有效
	 * 
	 * @param iYear
	 *            待验证日期(年)
	 * @param iMonth
	 *            待验证日期(月 1-12)
	 * @param iDate
	 *            待验证日期(日)
	 * @return 是否有效
	 */
	private static boolean valiDate(int iYear, int iMonth, int iDate) {
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int datePerMonth;
		if (iYear < MIN || iYear >= year) {
			return false;
		}
		if (iMonth < 1 || iMonth > 12) {
			return false;
		}
		switch (iMonth) {
		case 4:
		case 6:
		case 9:
		case 11:
			datePerMonth = 30;
			break;
		case 2:
			boolean dm = ((iYear % 4 == 0 && iYear % 100 != 0) || (iYear % 400 == 0))
					&& (iYear > MIN && iYear < year);
			datePerMonth = dm ? 29 : 28;
			break;
		default:
			datePerMonth = 31;
		}
		return (iDate >= 1) && (iDate <= datePerMonth);
	}

	/**
	 * 根据身份证号，自动获取对应的星座
	 * 
	 * @param idCard
	 *            身份证号码
	 * @return 星座
	 */
	public static String getConstellation(String idCard) {
		if (!validateCard(idCard))
			return "";
		int month = getMonth(idCard);
		int day = getDate(idCard);
		String strValue = "";

		if ((month == 1 && day >= 20) || (month == 2 && day <= 18)) {
			strValue = "水瓶座";
		} else if ((month == 2 && day >= 19) || (month == 3 && day <= 20)) {
			strValue = "双鱼座";
		} else if ((month == 3 && day > 20) || (month == 4 && day <= 19)) {
			strValue = "白羊座";
		} else if ((month == 4 && day >= 20) || (month == 5 && day <= 20)) {
			strValue = "金牛座";
		} else if ((month == 5 && day >= 21) || (month == 6 && day <= 21)) {
			strValue = "双子座";
		} else if ((month == 6 && day > 21) || (month == 7 && day <= 22)) {
			strValue = "巨蟹座";
		} else if ((month == 7 && day > 22) || (month == 8 && day <= 22)) {
			strValue = "狮子座";
		} else if ((month == 8 && day >= 23) || (month == 9 && day <= 22)) {
			strValue = "处女座";
		} else if ((month == 9 && day >= 23) || (month == 10 && day <= 23)) {
			strValue = "天秤座";
		} else if ((month == 10 && day > 23) || (month == 11 && day <= 22)) {
			strValue = "天蝎座";
		} else if ((month == 11 && day > 22) || (month == 12 && day <= 21)) {
			strValue = "射手座";
		} else if ((month == 12 && day > 21) || (month == 1 && day <= 19)) {
			strValue = "魔羯座";
		}

		return strValue;
	}

	/**
	 * 根据身份证号，自动获取对应的生肖
	 * 
	 * @param idCard
	 *            身份证号码
	 * @return 生肖
	 */
	public static String getZodiac(String idCard) { // 根据身份证号，自动返回对应的生肖
		if (!validateCard(idCard))
			return "";

		String sSX[] = { "猪", "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡",
				"狗" };
		int year = getYear(idCard);
		int end = 3;
		int x = (year - end) % 12;

		String retValue = "";
		retValue = sSX[x];

		return retValue;
	}

	/**
	 * 根据身份证号，自动获取对应的天干地支
	 * 
	 * @param idCard
	 *            身份证号码
	 * @return 天干地支
	 */
	public static String getChineseEra(String idCard) { // 根据身份证号，自动返回对应的生肖
		if (!validateCard(idCard))
			return "";
		String sTG[] = { "癸", "甲", "乙", "丙", "丁", "戊", "己", "庚", "辛", "任" };
		String sDZ[] = { "亥", "子", "丑", "寅", "卯", "辰", "巳", "午", "未", "申", "酉",
				"戌" };
		int year = getYear(idCard);
		int i = (year - 3) % 10;
		int j = (year - 3) % 12;
		String retValue = "";
		retValue = sTG[i] + sDZ[j];
		return retValue;
	}
}
