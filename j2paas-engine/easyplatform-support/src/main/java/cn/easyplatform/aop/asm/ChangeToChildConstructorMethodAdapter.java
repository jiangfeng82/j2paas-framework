/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.aop.asm;

import cn.easyplatform.org.objectweb.asm.MethodVisitor;

import static cn.easyplatform.org.objectweb.asm.Opcodes.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 */
class ChangeToChildConstructorMethodAdapter extends NormalMethodAdapter {

	private String superClassName;

	ChangeToChildConstructorMethodAdapter(MethodVisitor mv, String desc,
			int access, String superClassName) {
		super(mv, desc, access);
		this.superClassName = superClassName;
	}

	void visitCode() {
		mv.visitCode();
		mv.visitVarInsn(ALOAD, 0);
		loadArgs();
		mv.visitMethodInsn(INVOKESPECIAL, superClassName, "<init>", desc);
		mv.visitInsn(RETURN);
		mv.visitMaxs(2, 2);
		mv.visitEnd();
	}
}