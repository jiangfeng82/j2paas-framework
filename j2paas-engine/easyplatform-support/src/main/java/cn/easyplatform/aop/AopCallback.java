/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.aop;

/**
 * <b>不要实现这个接口</b><br/>
 * <b>不要实现这个接口</b><br/>
 * <b>不要实现这个接口</b><br/>
 * <b>不要实现这个接口</b><br/>
 * <b>不要实现这个接口</b><br/>
 * <b>不要实现这个接口</b><br/>
 * <b>不要实现这个接口</b><br/>
 * <b>不要实现这个接口</b><br/>
 * <b>这个接口仅供构建Aop类使用</b><br/>
 * <br/>
 * 这个接口将添加到被Aop改造过的类,如果你实现本接口,将导致不可预知的情况发生!!
 * 
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 * 
 */
public interface AopCallback {

	Object _aop_invoke(int methodIndex, Object[] args) throws Throwable;

}
