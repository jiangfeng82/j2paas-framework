/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.inject;

import cn.easyplatform.castor.Castors;
import cn.easyplatform.lang.Lang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

public class InjectByField implements Injecting {

	private static final Logger log = LoggerFactory.getLogger(InjectByField.class);

	private Field field;

	public InjectByField(Field field) {
		this.field = field;
		this.field.setAccessible(true);
	}

	public void inject(Object obj, Object value) {
		Object v = null;
		try {
			v = Castors.me().castTo(value, field.getType());
			field.set(obj, v);
		} catch (Exception e) {
			if (log.isInfoEnabled())
				log.info("Fail to set value by field", e);
			throw Lang.makeThrow(
					"Fail to set '%s'[ %s ] to field %s.'%s' because [%s]: %s",
					value, v, field.getDeclaringClass().getName(), field
							.getName(), Lang.unwrapThrow(e), Lang
							.unwrapThrow(e).getMessage());
		}
	}
}
