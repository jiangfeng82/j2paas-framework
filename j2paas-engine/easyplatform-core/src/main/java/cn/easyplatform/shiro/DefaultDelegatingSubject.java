/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.shiro;

import cn.easyplatform.services.ISubject;
import org.apache.shiro.subject.SubjectContext;
import org.apache.shiro.subject.support.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DefaultDelegatingSubject extends DelegatingSubject implements ISubject {

    private SubjectContext context;

    public DefaultDelegatingSubject(SubjectContext context) {
        super(context.resolvePrincipals(), context.resolveAuthenticated(), context.resolveHost(), context.resolveSession(), context.isSessionCreationEnabled(), context.resolveSecurityManager());
        this.context = context;
    }

    @Override
    public void setAuthenticate(boolean authenticated) {
        this.authenticated = authenticated;
        this.context.setAuthenticated(authenticated);
        this.session.setAttribute(DefaultSubjectContext.AUTHENTICATED_SESSION_KEY, authenticated);
    }


}
