/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.cfg.tx.jdbc;

import cn.easyplatform.cfg.TransactionContext;
import cn.easyplatform.dao.DaoException;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JdbcTransactionContext implements TransactionContext {

	protected CommandContext commandContext;

	/**
	 * @param commandContext
	 */
	public JdbcTransactionContext(CommandContext commandContext) {
		this.commandContext = commandContext;
		try {
			JdbcTransactions.begin();
		} catch (Exception ex) {
			throw new DaoException("dao.access.transaction.begin", ex,
					commandContext.getWorkflowContext().getName());
		}
	}

	@Override
	public void commit() {
		try {
			JdbcTransactions.commit();
		} catch (Exception ex) {
			throw new DaoException("dao.access.transaction.commit", ex,
					commandContext.getWorkflowContext().getName());
		}
	}

	@Override
	public void rollback() {
		try {
			JdbcTransactions.rollback();
		} catch (Exception ex) {
			throw new DaoException("dao.access.transaction.rollback", ex,
					commandContext.getWorkflowContext().getName());
		}
	}

	@Override
	public void close() {
		JdbcTransactions.close();
	}

}
