/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.services;

import cn.easyplatform.dos.EnvDo;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.dos.UserDo;
import cn.easyplatform.entities.beans.LogicBean;
import cn.easyplatform.entities.beans.ResourceBean;
import org.quartz.JobDetail;
import org.quartz.Trigger;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface IScheduleService {

    void start(IProjectService service, ResourceBean bean);

    void stop(String pid, String id);

    void stopGroup(String pid);

    int getState(String pid, String id);

    void pauseGroup(String pid);

    void pause(String pid, String id);

    void resumeGroup(String pid);

    void resume(String pid, String id);

    boolean modify(String pid, String id, Object time);

    Map<String, Object> getRuntimeInfo(String pid, String id);

    Collection<String> getJobKeys(String pid);

    //动态任务
    void scheduleDynamicJob(String pid, long jobId, String entity, String userId, Date time);
}
