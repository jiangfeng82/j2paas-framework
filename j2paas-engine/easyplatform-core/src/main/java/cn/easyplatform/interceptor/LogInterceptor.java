/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.interceptor;

import cn.easyplatform.type.IResponseMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class LogInterceptor extends AbstractCommandInterceptor {

    private static Logger log = LoggerFactory.getLogger(LogInterceptor.class);


    public IResponseMessage<?> execute(Command command) {
        if (!log.isDebugEnabled())
            return next.execute(command);
        log.debug(
                "--- starting {} --------------------------------------------------------",
                command.getName() == null ? command.getClass().getSimpleName() : command.getName());
        try {
            return next.execute(command);
        } finally {
            log.debug(
                    "--- {} finished --------------------------------------------------------",
                    command.getName() == null ? command.getClass().getSimpleName() : command.getName());
        }
    }
}
